package com.duongtuananh.buildingmanagement.entity.view;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Entity(name = "customerview")
public class CustomerView implements Serializable {
    @Id
    private Integer ID;
    private String code;
    private Integer id_Cus;
    private String Name_Emp;
    private String Gender;
    private String ID_Card;
    private Date DofB;
    private String Address;
    private String Phone_Number;
    private String email;
    private String name_Bank;
    private String bank_Account_Number;
    private String Name_Cpn;

    public Integer getID() {
        return ID;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName_Emp() {
        return Name_Emp;
    }

    public void setName_Emp(String name_Emp) {
        Name_Emp = name_Emp;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public String getID_Card() {
        return ID_Card;
    }

    public void setID_Card(String ID_Card) {
        this.ID_Card = ID_Card;
    }

    public Date getDofB() {
        return DofB;
    }

    public void setDofB(Date dofB) {
        DofB = dofB;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getPhone_Number() {
        return Phone_Number;
    }

    public void setPhone_Number(String phone_Number) {
        Phone_Number = phone_Number;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName_Bank() {
        return name_Bank;
    }

    public void setName_Bank(String name_Bank) {
        this.name_Bank = name_Bank;
    }

    public String getBank_Account_Number() {
        return bank_Account_Number;
    }

    public void setBank_Account_Number(String bank_Account_Number) {
        this.bank_Account_Number = bank_Account_Number;
    }

    public String getName_Cpn() {
        return Name_Cpn;
    }

    public void setName_Cpn(String name_Cpn) {
        Name_Cpn = name_Cpn;
    }

    public Integer getId_Cus() {
        return id_Cus;
    }

    public void setId_Cus(Integer id_Cus) {
        this.id_Cus = id_Cus;
    }
}
