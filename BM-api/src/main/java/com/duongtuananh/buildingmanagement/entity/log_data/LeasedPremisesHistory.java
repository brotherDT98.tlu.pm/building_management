package com.duongtuananh.buildingmanagement.entity.log_data;

import javax.persistence.*;

@Entity
@Table(name = "leased_premises_history")
public class LeasedPremisesHistory extends BaseEntityHistory {
    private Integer id;
    private String code;
    private String name;
    private Float acreage;
    private Integer status;
    private String floors;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getAcreage() {
        return acreage;
    }

    public void setAcreage(Float acreage) {
        this.acreage = acreage;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getFloors() {
        return floors;
    }

    public void setFloors(String floors) {
        this.floors = floors;
    }

}
