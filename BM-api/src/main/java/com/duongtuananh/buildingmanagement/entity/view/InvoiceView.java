package com.duongtuananh.buildingmanagement.entity.view;

import com.duongtuananh.buildingmanagement.entity.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

@Entity(name = "invoice")
public class InvoiceView implements Serializable {
    @Id
    private int id;
    private int id_Cpn;
    private String cpn_Code;
    private String name_Cpn;
    private int num_Of_Emp;
    private float acreage;
    private String service_Code;
    private String name_Service;
    private LocalDateTime createdAt;
    private Date end_Date;
    private float unit_Price;
    private  float thuctedichvu;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getAcreage() {
        return acreage;
    }

    public void setAcreage(float acreage) {
        this.acreage = acreage;
    }

    public int getId_Cpn() {
        return id_Cpn;
    }

    public void setId_Cpn(int id_Cpn) {
        this.id_Cpn = id_Cpn;
    }

    public String getCpn_Code() {
        return cpn_Code;
    }

    public void setCpn_Code(String cpn_Code) {
        this.cpn_Code = cpn_Code;
    }

    public String getName_Cpn() {
        return name_Cpn;
    }

    public void setName_Cpn(String name_Cpn) {
        this.name_Cpn = name_Cpn;
    }

    public int getNum_Of_Emp() {
        return num_Of_Emp;
    }

    public void setNum_Of_Emp(int num_Of_Emp) {
        this.num_Of_Emp = num_Of_Emp;
    }

    public String getService_Code() {
        return service_Code;
    }

    public void setService_Code(String service_Code) {
        this.service_Code = service_Code;
    }

    public String getName_Service() {
        return name_Service;
    }

    public void setName_Service(String name_Service) {
        this.name_Service = name_Service;
    }

    public Date getEnd_Date() {
        return end_Date;
    }

    public void setEnd_Date(Date end_Date) {
        this.end_Date = end_Date;
    }

    public float getUnit_Price() {
        return unit_Price;
    }

    public void setUnit_Price(float unit_Price) {
        this.unit_Price = unit_Price;
    }

    public float getThuctedichvu() {
        return thuctedichvu;
    }

    public void setThuctedichvu(float thuctedichvu) {
        this.thuctedichvu = thuctedichvu;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }
}
