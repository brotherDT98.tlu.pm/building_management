package com.duongtuananh.buildingmanagement.entity.auth;

public enum ERole {
	ROLE_USER,
    ROLE_MODERATOR,
    ROLE_ADMIN
}
