package com.duongtuananh.buildingmanagement.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Entity(name = "Contract")
public class Contract extends BaseEntity<String> implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String code;
    private Integer id_LD;
    private Integer id_Cpn;
    private Integer id_Cus;
    private Integer id_Emp;
    private Date start_Date;
    private Date end_Date;
    private Integer status;
    private Float deposit;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getId_LD() {
        return id_LD;
    }

    public void setId_LD(Integer id_LD) {
        this.id_LD = id_LD;
    }

    public Integer getId_Cpn() {
        return id_Cpn;
    }

    public void setId_Cpn(Integer id_Cpn) {
        this.id_Cpn = id_Cpn;
    }

    public Integer getId_Cus() {
        return id_Cus;
    }

    public void setId_Cus(Integer id_Cus) {
        this.id_Cus = id_Cus;
    }

    public Integer getId_Emp() {
        return id_Emp;
    }

    public void setId_Emp(Integer id_Emp) {
        this.id_Emp = id_Emp;
    }

    public Date getStart_Date() {
        return start_Date;
    }

    public void setStart_Date(Date start_Date) {
        this.start_Date = start_Date;
    }

    public Date getEnd_Date() {
        return end_Date;
    }

    public void setEnd_Date(Date end_Date) {
        this.end_Date = end_Date;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Float getDeposit() {
        return deposit;
    }

    public void setDeposit(Float deposit) {
        this.deposit = deposit;
    }
}
