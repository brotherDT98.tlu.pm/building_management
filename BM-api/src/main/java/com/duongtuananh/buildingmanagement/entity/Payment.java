package com.duongtuananh.buildingmanagement.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Entity(name = "Payment")
public class Payment extends BaseEntity<String> implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String code;
    private Date day_Create;
    private String reason;
    private Float amomunt_Of_Money;
    private String type_Pay;
    private Integer id_Emp;
    private String receiver;
    private String phone_Receiver;
    private String address;
    private String note;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Date getDay_Create() {
        return day_Create;
    }

    public void setDay_Create(Date day_Create) {
        this.day_Create = day_Create;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Float getAmomunt_Of_Money() {
        return amomunt_Of_Money;
    }

    public void setAmomunt_Of_Money(Float amomunt_Of_Money) {
        this.amomunt_Of_Money = amomunt_Of_Money;
    }

    public String getType_Pay() {
        return type_Pay;
    }

    public void setType_Pay(String type_Pay) {
        this.type_Pay = type_Pay;
    }

    public Integer getId_Emp() {
        return id_Emp;
    }

    public void setId_Emp(Integer id_Emp) {
        this.id_Emp = id_Emp;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getPhone_Receiver() {
        return phone_Receiver;
    }

    public void setPhone_Receiver(String phone_Receiver) {
        this.phone_Receiver = phone_Receiver;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
