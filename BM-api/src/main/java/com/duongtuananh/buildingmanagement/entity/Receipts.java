package com.duongtuananh.buildingmanagement.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;
@Entity(name = "Receipts")
public class Receipts extends BaseEntity<String> implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String code;
    private Float amomunt_Of_Money;
    private Integer id_Cus;
    private Integer id_Emp;
    private Integer id_Cpn;
    private String type_Pay;
    private String note;
    private int status;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Float getAmomunt_Of_Money() {
        return amomunt_Of_Money;
    }

    public void setAmomunt_Of_Money(Float amomunt_Of_Money) {
        this.amomunt_Of_Money = amomunt_Of_Money;
    }

    public Integer getId_Cus() {
        return id_Cus;
    }

    public void setId_Cus(Integer id_Cus) {
        this.id_Cus = id_Cus;
    }

    public Integer getId_Emp() {
        return id_Emp;
    }

    public void setId_Emp(Integer id_Emp) {
        this.id_Emp = id_Emp;
    }

    public String getType_Pay() {
        return type_Pay;
    }

    public void setType_Pay(String type_Pay) {
        this.type_Pay = type_Pay;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Integer getId_Cpn() {
        return id_Cpn;
    }

    public void setId_Cpn(Integer id_Cpn) {
        this.id_Cpn = id_Cpn;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
