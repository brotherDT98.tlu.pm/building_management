package com.duongtuananh.buildingmanagement.entity.log_data;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "company_history")
public class CompanyHistory extends BaseEntityHistory implements Serializable {
    private Integer ID_Cpn;
    private String Cpn_Code;
    private String Name_Cpn;
    private String Tax_Code;
    private String Capital;
    private Integer Num_Of_Emp;
    private String Address;
    private String Phone_Number;

    public Integer getID_Cpn() {
        return ID_Cpn;
    }

    public void setID_Cpn(Integer ID_Cpn) {
        this.ID_Cpn = ID_Cpn;
    }

    public String getCpn_Code() {
        return Cpn_Code;
    }

    public void setCpn_Code(String cpn_Code) {
        Cpn_Code = cpn_Code;
    }

    public String getName_Cpn() {
        return Name_Cpn;
    }

    public void setName_Cpn(String name_Cpn) {
        Name_Cpn = name_Cpn;
    }

    public String getTax_Code() {
        return Tax_Code;
    }

    public void setTax_Code(String tax_Code) {
        Tax_Code = tax_Code;
    }

    public String getCapital() {
        return Capital;
    }

    public void setCapital(String capital) {
        Capital = capital;
    }

    public Integer getNum_Of_Emp() {
        return Num_Of_Emp;
    }

    public void setNum_Of_Emp(Integer num_Of_Emp) {
        Num_Of_Emp = num_Of_Emp;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getPhone_Number() {
        return Phone_Number;
    }

    public void setPhone_Number(String phone_Number) {
        Phone_Number = phone_Number;
    }
}
