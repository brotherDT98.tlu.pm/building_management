package com.duongtuananh.buildingmanagement.entity.view;

import com.duongtuananh.buildingmanagement.entity.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Entity(name = "invoicetotal")
public class InvoiceTotalView extends BaseEntity<String> implements Serializable {
    @Id
    private Integer id;
    private String code;
    private Integer id_Emp;
    private Integer id_Cus;
    private Integer id_Cpn;
    private String name_emp;
    private String name_cus;
    private String name_cpn;
    private Float amomunt_Of_Money;
    private String type_pay;
    private String note;
    private int status;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getId_Emp() {
        return id_Emp;
    }

    public void setId_Emp(Integer id_Emp) {
        this.id_Emp = id_Emp;
    }

    public Integer getId_Cus() {
        return id_Cus;
    }

    public void setId_Cus(Integer id_Cus) {
        this.id_Cus = id_Cus;
    }

    public String getName_emp() {
        return name_emp;
    }

    public void setName_emp(String name_emp) {
        this.name_emp = name_emp;
    }

    public String getName_cus() {
        return name_cus;
    }

    public void setName_cus(String name_cus) {
        this.name_cus = name_cus;
    }

    public Float getAmomunt_Of_Money() {
        return amomunt_Of_Money;
    }

    public void setAmomunt_Of_Money(Float amomunt_Of_Money) {
        this.amomunt_Of_Money = amomunt_Of_Money;
    }

    public String getType_pay() {
        return type_pay;
    }

    public void setType_pay(String type_pay) {
        this.type_pay = type_pay;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Integer getId_Cpn() {
        return id_Cpn;
    }

    public void setId_Cpn(Integer id_Cpn) {
        this.id_Cpn = id_Cpn;
    }

    public String getName_cpn() {
        return name_cpn;
    }

    public void setName_cpn(String name_cpn) {
        this.name_cpn = name_cpn;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
