package com.duongtuananh.buildingmanagement.entity.log_data;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "register_service_history")
public class RegisterServiceHistory extends BaseEntityHistory implements Serializable {
    private Integer ID_Register;
    private Integer ID_Cpn;
    private Integer ID_Service;
    private Date End_Date;

    public Integer getID_Register() {
        return ID_Register;
    }

    public void setID_Register(Integer ID_Register) {
        this.ID_Register = ID_Register;
    }

    public Integer getID_Cpn() {
        return ID_Cpn;
    }

    public void setID_Cpn(Integer ID_Cpn) {
        this.ID_Cpn = ID_Cpn;
    }

    public Integer getID_Service() {
        return ID_Service;
    }

    public void setID_Service(Integer ID_Service) {
        this.ID_Service = ID_Service;
    }


    public Date getEnd_Date() {
        return End_Date;
    }

    public void setEnd_Date(Date end_Date) {
        End_Date = end_Date;
    }
}
