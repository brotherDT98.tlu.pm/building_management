package com.duongtuananh.buildingmanagement.entity.log_data;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "customer_history")
public class CustomerHistory extends BaseEntityHistory implements Serializable {
    private Integer id;
    private String code;
    private Integer id_Cus;
    private String email;
    private String name_Bank;
    private String bank_Account_Number;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getId_Cus() {
        return id_Cus;
    }

    public void setId_Cus(Integer id_Cus) {
        this.id_Cus = id_Cus;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName_Bank() {
        return name_Bank;
    }

    public void setName_Bank(String name_Bank) {
        this.name_Bank = name_Bank;
    }

    public String getBank_Account_Number() {
        return bank_Account_Number;
    }

    public void setBank_Account_Number(String bank_Account_Number) {
        this.bank_Account_Number = bank_Account_Number;
    }
}
