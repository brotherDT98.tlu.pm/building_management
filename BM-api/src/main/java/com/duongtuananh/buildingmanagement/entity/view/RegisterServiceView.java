package com.duongtuananh.buildingmanagement.entity.view;

import com.duongtuananh.buildingmanagement.entity.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Entity(name = "registerserviceview")
public class RegisterServiceView extends BaseEntity<String> implements Serializable {
    @Id
    private Integer ID_Register;
    private String Cpn_Code;
    private String Name_Cpn;
    private String Service_Code;
    private String Name_Service;
    private Date End_Date;

    public Integer getID_Register() {
        return ID_Register;
    }

    public void setID_Register(Integer ID_Register) {
        this.ID_Register = ID_Register;
    }

    public String getCpn_Code() {
        return Cpn_Code;
    }

    public void setCpn_Code(String cpn_Code) {
        Cpn_Code = cpn_Code;
    }

    public String getName_Cpn() {
        return Name_Cpn;
    }

    public void setName_Cpn(String name_Cpn) {
        Name_Cpn = name_Cpn;
    }

    public String getService_Code() {
        return Service_Code;
    }

    public void setService_Code(String service_Code) {
        Service_Code = service_Code;
    }

    public String getName_Service() {
        return Name_Service;
    }

    public void setName_Service(String name_Service) {
        Name_Service = name_Service;
    }

    public Date getEnd_Date() {
        return End_Date;
    }

    public void setEnd_Date(Date end_Date) {
        End_Date = end_Date;
    }

}
