package com.duongtuananh.buildingmanagement.security.service.Impl;

import com.duongtuananh.buildingmanagement.entity.Customer;
import com.duongtuananh.buildingmanagement.entity.log_data.CustomerHistory;
import com.duongtuananh.buildingmanagement.entity.view.CustomerView;
import com.duongtuananh.buildingmanagement.repository.CustomerRepository;
import com.duongtuananh.buildingmanagement.repository.log_data.CustomerHistoryRepo;
import com.duongtuananh.buildingmanagement.repository.view.CustomerViewRepo;
import com.duongtuananh.buildingmanagement.security.service.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class CustomerServiceImpl implements CustomerService {
    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private CustomerHistoryRepo customerHistoryRepo;

    @Autowired
    private CustomerViewRepo customerViewRepo;

    @Override
    public List<Customer> getAllCustomer() {
        return customerRepository.findAll();
    }

    @Override
    public void saveCustomer(Customer customer) {
        customerRepository.save(customer);
    }

    @Override
    public void deleteCustomer(Integer ID) {
        customerRepository.deleteById(ID);
    }

    @Override
    public Optional<Customer> getCusByID(Integer ID) {
        return customerRepository.findById(ID);
    }

    @Override
    public List<CustomerView> getAllCustomerDetails() {
        return customerViewRepo.findAll();
    }

    @Override
    public Optional<CustomerView> getCusDetailsByID_Cus(Integer id_Cus) {
        return customerViewRepo.getCusByID_Cus(id_Cus);
    }

    @Override
    public List<CustomerView> getCustomersBySearch(String search) {
        return customerViewRepo.getCustomersBySearch(search);
    }

    @Override
    public List<CustomerHistory> getAllCustomerHistory() {
        return customerHistoryRepo.findAll();
    }

    @Override
    public List<CustomerHistory> getAllCustomersHistoryBySearch(String search) {
        return customerHistoryRepo.getCustomersHistoriesBySearch(search);
    }

    @Override
    public List<CustomerHistory> getAllCustomersHistoryByDate(Date fromDate, Date toDate, String value) {
        return customerHistoryRepo.getCustomersHistoriesByDate(fromDate,toDate,value);
    }

    @Override
    public void insert(CustomerHistory customerHistory) {
        customerHistoryRepo.save(customerHistory);
    }

    @Override
    public List<CustomerView> getCusByCompany(String nameCompany) {
        return customerViewRepo.getCusByCompany(nameCompany);
    }
}
