package com.duongtuananh.buildingmanagement.security.service.Impl;

import com.duongtuananh.buildingmanagement.entity.Emp_Building;
import com.duongtuananh.buildingmanagement.entity.log_data.EmployyeBuildingHistory;
import com.duongtuananh.buildingmanagement.repository.Emp_BuildingRepository;
import com.duongtuananh.buildingmanagement.repository.log_data.EmpBuildingHistoryRepo;
import com.duongtuananh.buildingmanagement.security.service.service.Emp_BuildingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class Emp_BuildingServiceImpl implements Emp_BuildingService {
    @Autowired
    private Emp_BuildingRepository emp_buildingRepository;

    @Autowired
    private EmpBuildingHistoryRepo empBuildingHistoryRepo;

    @Override
    public List<Emp_Building> getAllEmpBuilding() {
        return emp_buildingRepository.findAll();
    }

    @Override
    public void saveEmpBuilding(Emp_Building emp_building) {
        emp_buildingRepository.save(emp_building);
    }

    @Override
    public void deleteEmpBuilding(Integer ID) {
        emp_buildingRepository.deleteById(ID);
    }

    @Override
    public Optional<Emp_Building> getEmpBuildingByID(Integer ID) {
        return emp_buildingRepository.findById(ID);
    }

    @Override
    public List<Emp_Building> getEmployeesBuildingBySearch(String search) {
        return emp_buildingRepository.getEmployeesBuildingBySearch(search);
    }

    @Override
    public List<EmployyeBuildingHistory> getAllEmpBuildingHistory() {
        return empBuildingHistoryRepo.findAll();
    }

    @Override
    public List<EmployyeBuildingHistory> getAllEmpsBuildingHistoryBySearch(String search) {
        return empBuildingHistoryRepo.getEmployeesBuildingHistoriesBySearch(search);
    }

    @Override
    public List<EmployyeBuildingHistory> getAllEmpsBuildingHistoryByDate(Date fromDate, Date toDate, String value) {
        return empBuildingHistoryRepo.getEmployeesCompanyHistoriesByDate(fromDate,toDate,value);
    }

    @Override
    public void insert(EmployyeBuildingHistory employyeBuildingHistory) {
        empBuildingHistoryRepo.save(employyeBuildingHistory);
    }
}
