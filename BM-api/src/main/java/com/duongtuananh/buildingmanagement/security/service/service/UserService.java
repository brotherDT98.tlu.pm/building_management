package com.duongtuananh.buildingmanagement.security.service.service;

import com.duongtuananh.buildingmanagement.entity.auth.User;

import java.util.List;
import java.util.Optional;

public interface UserService {
    List<User> getAllUser();
    void saveUser(User user);
    void deleteUser(Long ID);
    Optional<User> getUserById(Long id);
    List<User> getUsersBySearch(String search);
    void updateUserRole(Long userId, Integer roleId);

    User findUserByEmail(String email);
    void changeUserPassword(User user, String password);

    User getUserByPasswordResetToken(String token);
}
