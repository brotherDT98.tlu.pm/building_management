package com.duongtuananh.buildingmanagement.security.service.service;

import com.duongtuananh.buildingmanagement.entity.RegisterService;
import com.duongtuananh.buildingmanagement.entity.log_data.ReceiptsHistory;
import com.duongtuananh.buildingmanagement.entity.log_data.RegisterServiceHistory;
import com.duongtuananh.buildingmanagement.entity.view.RegisterServiceView;
import com.duongtuananh.buildingmanagement.repository.ReceiptsRepository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface RegisterService_Service {
    List<RegisterService> getAllRegisterService();
    void saveRegisterService(RegisterService registerService);
    void deleteRegisterService(Integer ID);

    List<RegisterServiceView> getAllRegisDetails();
    Optional<RegisterService> getRegisterServiceByID(Integer ID);
    List<RegisterServiceView> getListRegisterServiceByCodeCompany(String code);

    List<RegisterServiceView> getRegisterServicesBySearch(String search);
    List<RegisterServiceHistory> getAllRegisterServiceHistory();
    List<RegisterServiceHistory> getAllRegisterServiceHistoryBySearch(String search);
    List<RegisterServiceHistory> getAllRegisterServiceHistoryByDate(Date fromDate, Date toDate, String value);
    void insert(RegisterServiceHistory registerServiceHistory);
}
