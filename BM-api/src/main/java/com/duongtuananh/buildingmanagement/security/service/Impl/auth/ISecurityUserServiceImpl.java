package com.duongtuananh.buildingmanagement.security.service.Impl.auth;

import com.duongtuananh.buildingmanagement.entity.auth.PasswordResetToken;
import com.duongtuananh.buildingmanagement.entity.auth.User;
import com.duongtuananh.buildingmanagement.repository.auth.PasswordResetTokenRepository;
import com.duongtuananh.buildingmanagement.security.service.service.ISecurityUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ISecurityUserServiceImpl implements ISecurityUserService {
    @Autowired
    private PasswordResetTokenRepository passwordTokenRepository;

    @Override
    public String validatePasswordResetToken( String token) {
        final PasswordResetToken passToken = passwordTokenRepository.findByToken(token);
        if ((passToken == null)) {
            return "invalidToken";
        }
        return "null";

//        final Calendar cal = Calendar.getInstance();
//        if ((passToken.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
//            return "expired";
//        }
///// VALID USER LOGIN LUON =))
//        final User user = passToken.getUser();
//        final Authentication auth = new UsernamePasswordAuthenticationToken(user, null, Arrays.asList(new SimpleGrantedAuthority("CHANGE_PASSWORD_PRIVILEGE")));
//        SecurityContextHolder.getContext().setAuthentication(auth);
//        return "null";
    }

    @Override
    public void createPasswordResetTokenForUser(User user, String token) {
        PasswordResetToken myToken = new PasswordResetToken(token, user);
        passwordTokenRepository.save(myToken);
    }

    @Override
    public PasswordResetToken findByUser(User user) {
        return passwordTokenRepository.findByUser(user);
    }


}
