package com.duongtuananh.buildingmanagement.security.service.service;

import com.duongtuananh.buildingmanagement.entity.Customer;
import com.duongtuananh.buildingmanagement.entity.log_data.CustomerHistory;
import com.duongtuananh.buildingmanagement.entity.view.CustomerView;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface CustomerService {
    List<Customer> getAllCustomer();
    void saveCustomer(Customer customer);
    void deleteCustomer(Integer ID);
    Optional<Customer> getCusByID(Integer ID);

    List<CustomerView> getAllCustomerDetails();
    Optional<CustomerView> getCusDetailsByID_Cus(Integer id_Cus);
    List<CustomerView> getCustomersBySearch( String search);
    List<CustomerHistory> getAllCustomerHistory();
    List<CustomerHistory> getAllCustomersHistoryBySearch(String search);
    List<CustomerHistory> getAllCustomersHistoryByDate(Date fromDate, Date toDate, String value);
    void insert(CustomerHistory customerHistory);
    List<CustomerView> getCusByCompany(String nameCompany);
}
