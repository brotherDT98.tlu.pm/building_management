package com.duongtuananh.buildingmanagement.security.service.service;

import com.duongtuananh.buildingmanagement.entity.auth.PasswordResetToken;
import com.duongtuananh.buildingmanagement.entity.auth.User;

import java.util.Optional;

public interface ISecurityUserService {
    String validatePasswordResetToken( String token);
    void createPasswordResetTokenForUser(User user, String token);
    PasswordResetToken findByUser(User user);
}
