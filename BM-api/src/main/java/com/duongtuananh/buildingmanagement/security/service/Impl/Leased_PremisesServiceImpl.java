package com.duongtuananh.buildingmanagement.security.service.Impl;

import com.duongtuananh.buildingmanagement.entity.Leased_Premises;
import com.duongtuananh.buildingmanagement.entity.log_data.LeasedPremisesHistory;
import com.duongtuananh.buildingmanagement.repository.Leased_PremisesRepository;
import com.duongtuananh.buildingmanagement.repository.log_data.LeasedPremisesHistoryRepo;
import com.duongtuananh.buildingmanagement.security.service.service.Leased_PremisesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class Leased_PremisesServiceImpl implements Leased_PremisesService {
    @Autowired
    private Leased_PremisesRepository leasedPremisesRepository;

    @Autowired
    private LeasedPremisesHistoryRepo leasedPremisesHistoryRepo;

    @Override
    public List<Leased_Premises> getAllLP() {
        return leasedPremisesRepository.findAll();
    }

    @Override
    public void saveLP(Leased_Premises leasedPremises) {
        leasedPremisesRepository.save(leasedPremises);
    }

    @Override
    public void deleteLP(Integer ID) {
        leasedPremisesRepository.deleteById(ID);
    }

    @Override
    public Optional<Leased_Premises> getLPByID(Integer ID) {
        return leasedPremisesRepository.findById(ID);
    }

    @Override
    public List<Leased_Premises> getLeasedPremisesBySearch(String search) {
        return leasedPremisesRepository.getLeasedPremisesBySearch(search);
    }

    @Override
    public List<LeasedPremisesHistory> getAllLPHistory() {
        return leasedPremisesHistoryRepo.findAll();
    }

    @Override
    public List<LeasedPremisesHistory> getLeasedPremisesHistoriesBySearch(String search) {
        return leasedPremisesHistoryRepo.getLeasedPremisesHistoriesBySearch(search);
    }

    @Override
    public List<LeasedPremisesHistory> getLeasedPremisesHistoriesByDate(Date fromDate, Date toDate, String value) {
        return leasedPremisesHistoryRepo.getLeasedPremisesHistoriesByDate(fromDate,toDate,value);
    }

    @Override
    public void insert(LeasedPremisesHistory leasedPremisesHistory) {
         leasedPremisesHistoryRepo.save(leasedPremisesHistory);
    }

    @Override
    public List<Leased_Premises> getLPByStatus() {
        return leasedPremisesRepository.getLPByStatus();
    }
}
