package com.duongtuananh.buildingmanagement.security.service.Impl;

import com.duongtuananh.buildingmanagement.entity.Company;
import com.duongtuananh.buildingmanagement.entity.log_data.CompanyHistory;
import com.duongtuananh.buildingmanagement.repository.CompanyRepository;
import com.duongtuananh.buildingmanagement.repository.log_data.CompanyHistoryRepo;
import com.duongtuananh.buildingmanagement.security.service.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class CompanyServiceImpl implements CompanyService {
    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private CompanyHistoryRepo companyHistoryRepo;

    @Override
    public List<Company> getAllCompany() {
        return companyRepository.findAll();
    }

    @Override
    public void saveCompany(Company company) {
        companyRepository.save(company);
    }

    @Override
    public void deleteCompany(Integer ID) {
        companyRepository.deleteById(ID);
    }

    @Override
    public Optional<Company> getCpnByID(Integer id) {
        return companyRepository.findById(id);
    }

    @Override
    public List<Company> getCompaniesBySearch(String search) {
        return companyRepository.getCompaniesBySearch(search);
    }

    @Override
    public List<CompanyHistory> getAllCompanyHistory() {
        return companyHistoryRepo.findAll();
    }

    @Override
    public List<CompanyHistory> getCompanyHistoriesBySearch(String search) {
        return companyHistoryRepo.getCompanyHistoriesBySearch(search);
    }

    @Override
    public List<CompanyHistory> getCompanyHistoriesByDate(Date fromDate, Date toDate, String value) {
        return companyHistoryRepo.getCompanyHistoriesByDate(fromDate,toDate,value);
    }

    @Override
    public void insertCompanyHistory(CompanyHistory companyHistory) {
        companyHistoryRepo.save(companyHistory);
    }

}
