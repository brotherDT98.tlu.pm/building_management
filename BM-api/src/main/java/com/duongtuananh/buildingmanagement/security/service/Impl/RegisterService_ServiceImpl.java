package com.duongtuananh.buildingmanagement.security.service.Impl;

import com.duongtuananh.buildingmanagement.entity.RegisterService;
import com.duongtuananh.buildingmanagement.entity.log_data.RegisterServiceHistory;
import com.duongtuananh.buildingmanagement.entity.view.RegisterServiceView;
import com.duongtuananh.buildingmanagement.repository.ReceiptsRepository;
import com.duongtuananh.buildingmanagement.repository.RegisterServiceRepository;
import com.duongtuananh.buildingmanagement.repository.log_data.RegisterServiceHistoryRepo;
import com.duongtuananh.buildingmanagement.repository.view.RegisServiceViewRepo;
import com.duongtuananh.buildingmanagement.security.service.service.RegisterService_Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class RegisterService_ServiceImpl implements RegisterService_Service {
    @Autowired
    private RegisterServiceRepository registerServiceRepository;

    @Autowired
    private RegisServiceViewRepo regisServiceViewRepo;

    @Autowired
    private RegisterServiceHistoryRepo registerServiceHistoryRepo;

    @Override
    public List<RegisterService> getAllRegisterService() {
        return registerServiceRepository.findAll();
    }

    @Override
    public void saveRegisterService(RegisterService registerService) {
        registerServiceRepository.save(registerService);
    }

    @Override
    public void deleteRegisterService(Integer ID) {
        registerServiceRepository.deleteById(ID);
    }

    @Override
    public List<RegisterServiceView> getAllRegisDetails() {
        return regisServiceViewRepo.findAll();
    }

    @Override
    public Optional<RegisterService> getRegisterServiceByID(Integer ID) {
        return registerServiceRepository.findById(ID);
    }

    @Override
    public List<RegisterServiceView> getListRegisterServiceByCodeCompany(String code) {
        return regisServiceViewRepo.getListServiceByCompany(code);
    }

    @Override
    public List<RegisterServiceView> getRegisterServicesBySearch(String search) {
        return regisServiceViewRepo.getRegisterServicesBySearch(search);
    }

    @Override
    public List<RegisterServiceHistory> getAllRegisterServiceHistory() {
        return registerServiceHistoryRepo.findAll();
    }

    @Override
    public List<RegisterServiceHistory> getAllRegisterServiceHistoryBySearch(String search) {
        return registerServiceHistoryRepo.getRegisterServiceHistoriesBySearch(search);
    }

    @Override
    public List<RegisterServiceHistory> getAllRegisterServiceHistoryByDate(Date fromDate, Date toDate, String value) {
        return registerServiceHistoryRepo.getRegisterServiceHistoriesByDate(fromDate,toDate,value);
    }

    @Override
    public void insert(RegisterServiceHistory receiptsRepository) {
        registerServiceHistoryRepo.save(receiptsRepository);
    }
}
