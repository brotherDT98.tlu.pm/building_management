package com.duongtuananh.buildingmanagement.security.service.service;

import com.duongtuananh.buildingmanagement.entity.Contract;
import com.duongtuananh.buildingmanagement.entity.log_data.ContractHistory;
import com.duongtuananh.buildingmanagement.entity.log_data.LeasedPremisesHistory;
import com.duongtuananh.buildingmanagement.entity.view.ContractView;
import com.duongtuananh.buildingmanagement.entity.view.InvoiceView;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface ContractService {
    List<Contract> getAllContract();
    void saveContract(Contract contract);
    void deleteContract(Integer ID);
    Optional<Contract> getContractByID(Integer ID);
    void updateAllContract();
    List<ContractView> getAllDetailsContract();
    Optional<ContractView> getContractDetailByID(Integer ID);
    List<InvoiceView> getInvoices(Integer id);
    List<ContractView> getContractsBySearch(String search);
    List<ContractView> getContractByCompany(Integer idCpn);
    List<ContractHistory> getAllContractHistory();
    List<ContractHistory> getContractsHistoryBySearch(String search);
    List<ContractHistory> getContractsHistoryByDate(Date fromDate, Date toDate, String value);
    void insert(ContractHistory contractHistory);
}
