package com.duongtuananh.buildingmanagement.security.service.service;

import com.duongtuananh.buildingmanagement.entity.Receipts;
import com.duongtuananh.buildingmanagement.entity.log_data.ReceiptsHistory;
import com.duongtuananh.buildingmanagement.entity.view.InvoiceTotalView;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface ReceiptsService {
    List<Receipts> getAllReceipts();
    void saveReceipts(Receipts receipts);
    void deleteReceipts(Integer ID);
    Optional<Receipts> getReceiptsByID(Integer ID);
    List<InvoiceTotalView> getAllInvoiceTotal();
    Optional<InvoiceTotalView> getInvoiceTotal(Integer id);
    List<InvoiceTotalView> getInvoicesTotalBySearch(String search);
    List<InvoiceTotalView> getInvoicesTotalByDate(Date fromDate, Date toDate);
    List<ReceiptsHistory> getAllReceiptsHistory();
    List<ReceiptsHistory> getAllReceiptsHistoryBySearch(String search);
    List<ReceiptsHistory> getAllReceiptsHistoryByDate(Date fromDate, Date toDate,String value);
    void insert(ReceiptsHistory receiptsHistory);
}
