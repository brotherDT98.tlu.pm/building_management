package com.duongtuananh.buildingmanagement.security.service.Impl;

import com.duongtuananh.buildingmanagement.entity.log_data.ServiceHistory;
import com.duongtuananh.buildingmanagement.repository.ServiceRepository;
import com.duongtuananh.buildingmanagement.repository.log_data.ServiceHistoryRepo;
import com.duongtuananh.buildingmanagement.security.service.service.Service_Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class Service_ServiceImpl implements Service_Service {
    @Autowired
    private ServiceRepository serviceRepository;

    @Autowired
    private ServiceHistoryRepo serviceHistoryRepo;

    @Override
    public List<com.duongtuananh.buildingmanagement.entity.Service> getAllService() {
        return serviceRepository.findAll();
    }

    @Override
    public void saveService(com.duongtuananh.buildingmanagement.entity.Service service) {
        serviceRepository.save(service);
    }

    @Override
    public void deleteService(Integer ID) {
        serviceRepository.deleteById(ID);
    }

    @Override
    public Optional<com.duongtuananh.buildingmanagement.entity.Service> getServiceById(Integer Id) {
        return serviceRepository.findById(Id);
    }

    @Override
    public List<com.duongtuananh.buildingmanagement.entity.Service> getServicesBySearch(String search) {
        return serviceRepository.getServicesBySearch(search);
    }

    @Override
    public List<ServiceHistory> getAllServiceHistory() {
        return serviceHistoryRepo.findAll();
    }

    @Override
    public List<ServiceHistory> getAllServicesHistoryBySearch(String search) {
        return serviceHistoryRepo.getServicesHistoriesBySearch(search);
    }

    @Override
    public List<ServiceHistory> getAllServicesHistoryByDate(Date fromDate, Date toDate, String value) {
        return serviceHistoryRepo.getServicesHistoriesByDate(fromDate,toDate,value);
    }

    @Override
    public void insert(ServiceHistory serviceHistory) {
        serviceHistoryRepo.save(serviceHistory);
    }


}
