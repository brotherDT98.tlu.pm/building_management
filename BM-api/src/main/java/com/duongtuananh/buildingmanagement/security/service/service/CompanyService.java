package com.duongtuananh.buildingmanagement.security.service.service;

import com.duongtuananh.buildingmanagement.entity.Company;
import com.duongtuananh.buildingmanagement.entity.log_data.CompanyHistory;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface CompanyService {
    List<Company> getAllCompany();
    void saveCompany(Company company);
    void deleteCompany(Integer ID);
    Optional<Company> getCpnByID(Integer id);
    List<Company> getCompaniesBySearch(String search);
    List<CompanyHistory> getAllCompanyHistory();
    List<CompanyHistory> getCompanyHistoriesBySearch(String search);
    List<CompanyHistory> getCompanyHistoriesByDate(Date fromDate , Date toDate, String value);
    void insertCompanyHistory(CompanyHistory companyHistory);
}
