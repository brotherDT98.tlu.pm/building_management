package com.duongtuananh.buildingmanagement.security.service.Impl;

import com.duongtuananh.buildingmanagement.entity.auth.User;
import com.duongtuananh.buildingmanagement.repository.auth.PasswordResetTokenRepository;
import com.duongtuananh.buildingmanagement.repository.auth.UserRepository;
import com.duongtuananh.buildingmanagement.security.service.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    private PasswordResetTokenRepository passwordTokenRepository;


    @Override
    public List<User> getAllUser() {
        return userRepository.findAll();
    }

    @Override
    public void saveUser(User user) {
        userRepository.save(user);
    }

    @Override
    public void deleteUser(Long ID) {
        userRepository.deleteById(ID);
    }

    @Override
    public Optional<User> getUserById(Long id) {
        return userRepository.findById(id);
    }

    @Override
    public List<User> getUsersBySearch(String search) {
        return userRepository.getUsersBySearch(search);
    }

    @Override
    public void updateUserRole(Long userId, Integer roleId) {
        userRepository.updateRoles(userId,roleId);
    }

    @Override
    public User findUserByEmail(String email) {
        return userRepository.findUserByEmail(email);
    }

    @Override
    public void changeUserPassword(User user, String password) {
        user.setPassword(encoder.encode(password));
        userRepository.save(user);
    }

    @Override
    public User getUserByPasswordResetToken(String token) {
        return passwordTokenRepository.findByToken(token).getUser();
    }
}
