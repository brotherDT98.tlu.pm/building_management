package com.duongtuananh.buildingmanagement.security.service.Impl;

import com.duongtuananh.buildingmanagement.entity.Payment;
import com.duongtuananh.buildingmanagement.repository.PaymentRepository;
import com.duongtuananh.buildingmanagement.security.service.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PaymentServiceImpl implements PaymentService {
    @Autowired
    private PaymentRepository paymentRepository;

    @Override
    public List<Payment> getAllPayment() {
        return paymentRepository.findAll();
    }

    @Override
    public void savePayent(Payment payment) {
        paymentRepository.save(payment);
    }

    @Override
    public void deletePayment(Integer ID) {
        paymentRepository.deleteById(ID);
    }

    @Override
    public Optional<Payment> getPaymentByID(Integer ID) {
        return paymentRepository.findById(ID);
    }
}
