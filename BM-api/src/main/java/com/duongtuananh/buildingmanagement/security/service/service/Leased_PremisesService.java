package com.duongtuananh.buildingmanagement.security.service.service;
import com.duongtuananh.buildingmanagement.entity.Leased_Premises;
import com.duongtuananh.buildingmanagement.entity.log_data.LeasedPremisesHistory;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface Leased_PremisesService {
    List<Leased_Premises> getAllLP();
    void saveLP(Leased_Premises leasedPremises);
    void deleteLP(Integer ID);
    Optional<Leased_Premises> getLPByID(Integer ID);
    List<Leased_Premises> getLeasedPremisesBySearch(String search);
    List<LeasedPremisesHistory> getAllLPHistory();
    List<LeasedPremisesHistory> getLeasedPremisesHistoriesBySearch(String search);
    List<LeasedPremisesHistory> getLeasedPremisesHistoriesByDate(Date fromDate, Date toDate, String value);
    void insert(LeasedPremisesHistory leasedPremisesHistory);
    List<Leased_Premises> getLPByStatus();
}
