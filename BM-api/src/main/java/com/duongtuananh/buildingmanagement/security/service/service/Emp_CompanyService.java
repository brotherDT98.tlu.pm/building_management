package com.duongtuananh.buildingmanagement.security.service.service;

import com.duongtuananh.buildingmanagement.entity.Emp_Company;
import com.duongtuananh.buildingmanagement.entity.log_data.EmployeeCompanyHistory;
import com.duongtuananh.buildingmanagement.entity.log_data.EmployyeBuildingHistory;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface Emp_CompanyService {
    List<Emp_Company> getAllEmpCompany();
    void saveEmpCompany(Emp_Company emp_company);
    void deleteEmpCompany(Integer ID);
    Optional<Emp_Company> getEmpCompanyById(Integer ID);
    List<Emp_Company> getEmployeesCompanyBySearch(String search);
    List<Emp_Company> getEmployeeByCompany(Integer idCompany);
    List<EmployeeCompanyHistory> getAllEmpCompanyHistory();
    List<EmployeeCompanyHistory> getAllEmpsCompanyHistoryBySearch(String search);
    List<EmployeeCompanyHistory> getAllEmpsCompanyHistoryByDate(Date fromDate, Date toDate, String value);
    void insert(EmployeeCompanyHistory employeeCompanyHistory);
}
