package com.duongtuananh.buildingmanagement.security.service.service;

import com.duongtuananh.buildingmanagement.entity.Service;
import com.duongtuananh.buildingmanagement.entity.log_data.ServiceHistory;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface Service_Service {
    List<Service> getAllService();
    void saveService(Service service);
    void deleteService(Integer ID);
    Optional<Service> getServiceById(Integer Id);
    List<Service> getServicesBySearch(String search);
    List<ServiceHistory> getAllServiceHistory();
    List<ServiceHistory> getAllServicesHistoryBySearch(String search);
    List<ServiceHistory> getAllServicesHistoryByDate(Date fromDate, Date toDate, String value);
    void insert(ServiceHistory serviceHistory);
}
