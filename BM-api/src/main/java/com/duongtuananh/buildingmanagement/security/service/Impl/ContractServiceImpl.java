package com.duongtuananh.buildingmanagement.security.service.Impl;

import com.duongtuananh.buildingmanagement.entity.Contract;
import com.duongtuananh.buildingmanagement.entity.log_data.ContractHistory;
import com.duongtuananh.buildingmanagement.entity.log_data.LeasedPremisesHistory;
import com.duongtuananh.buildingmanagement.entity.view.ContractView;
import com.duongtuananh.buildingmanagement.entity.view.InvoiceView;
import com.duongtuananh.buildingmanagement.repository.ContractRepository;
import com.duongtuananh.buildingmanagement.repository.log_data.ContractHistoryRepo;
import com.duongtuananh.buildingmanagement.repository.view.ContractViewRepo;
import com.duongtuananh.buildingmanagement.repository.view.InvoiceViewRepo;
import com.duongtuananh.buildingmanagement.security.service.service.ContractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class ContractServiceImpl implements ContractService {
    @Autowired
    private ContractRepository contractRepository;

    @Autowired
    private ContractHistoryRepo contractHistoryRepo;

    @Autowired
    private ContractViewRepo contractViewRepo;

    @Autowired
    private InvoiceViewRepo invoiceViewRepo;

    @Override
    public List<Contract> getAllContract() {
        return contractRepository.findAll();
    }

    @Override
    public void saveContract(Contract contract) {
        contractRepository.save(contract);
    }

    @Override
    public void deleteContract(Integer ID) {
        contractRepository.deleteById(ID);
    }

    @Override
    public Optional<Contract> getContractByID(Integer ID) {
        return contractRepository.findById(ID);
    }

    @Override
    public void updateAllContract() {
        contractRepository.updateAllContract();
    }

    @Override
    public List<ContractView> getAllDetailsContract() {
        return contractViewRepo.findAll();
    }

    @Override
    public Optional<ContractView> getContractDetailByID(Integer ID) {
        return contractViewRepo.findById(ID);
    }

    @Override
    public List<InvoiceView> getInvoices(Integer id) {
        return invoiceViewRepo.getInvoicesByCompany(id);
    }

    @Override
    public List<ContractView> getContractsBySearch(String search) {
        return contractViewRepo.getContractsBySearch(search);
    }

    @Override
    public List<ContractView> getContractByCompany(Integer idCpn) {
        return contractViewRepo.getContractViewByCompany(idCpn);
    }

    @Override
    public List<ContractHistory> getAllContractHistory() {
        return contractHistoryRepo.findAll();
    }

    @Override
    public List<ContractHistory> getContractsHistoryBySearch(String search) {
        return contractHistoryRepo.getContractsHistoriesBySearch(search);
    }

    @Override
    public List<ContractHistory> getContractsHistoryByDate(Date fromDate, Date toDate, String value) {
        return contractHistoryRepo.getContractsHistoriesByDate(fromDate,toDate,value);
    }

    @Override
    public void insert(ContractHistory contractHistory) {
        contractHistoryRepo.save(contractHistory);
    }
}
