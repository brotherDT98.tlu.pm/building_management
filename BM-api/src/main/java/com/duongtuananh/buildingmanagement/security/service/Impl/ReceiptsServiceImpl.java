package com.duongtuananh.buildingmanagement.security.service.Impl;

import com.duongtuananh.buildingmanagement.entity.Receipts;
import com.duongtuananh.buildingmanagement.entity.log_data.ReceiptsHistory;
import com.duongtuananh.buildingmanagement.entity.view.InvoiceTotalView;
import com.duongtuananh.buildingmanagement.repository.ReceiptsRepository;
import com.duongtuananh.buildingmanagement.repository.log_data.ReceiptsHistoryRepo;
import com.duongtuananh.buildingmanagement.repository.view.InvoiceTotalViewRepo;
import com.duongtuananh.buildingmanagement.security.service.service.ReceiptsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class ReceiptsServiceImpl implements ReceiptsService {
    @Autowired
    private ReceiptsRepository receiptsRepository;

    @Autowired
    private InvoiceTotalViewRepo invoiceTotalViewRepo;

    @Autowired
    private ReceiptsHistoryRepo receiptsHistoryRepo;

    @Override
    public List<Receipts> getAllReceipts() {
        return receiptsRepository.findAll();
    }

    @Override
    public void saveReceipts(Receipts receipts) {
        receiptsRepository.save(receipts);
    }

    @Override
    public void deleteReceipts(Integer ID) {
        receiptsRepository.deleteById(ID);
    }

    @Override
    public Optional<Receipts> getReceiptsByID(Integer ID) {
        return receiptsRepository.findById(ID);
    }

    @Override
    public List<InvoiceTotalView> getAllInvoiceTotal() {
        return invoiceTotalViewRepo.findAll();
    }

    @Override
    public Optional<InvoiceTotalView> getInvoiceTotal(Integer id) {
        return invoiceTotalViewRepo.getIvoiceTotal(id);
    }

    @Override
    public List<InvoiceTotalView> getInvoicesTotalBySearch(String search) {
        return invoiceTotalViewRepo.getInvoicesTotalBySearch(search);
    }

    @Override
    public List<InvoiceTotalView> getInvoicesTotalByDate(Date fromDate, Date toDate) {
        return invoiceTotalViewRepo.getInvoicesTotalByDate(fromDate, toDate);
    }

    @Override
    public List<ReceiptsHistory> getAllReceiptsHistory() {
        return receiptsHistoryRepo.findAll();
    }

    @Override
    public List<ReceiptsHistory> getAllReceiptsHistoryBySearch(String search) {
        return receiptsHistoryRepo.getReceiptsHistoriesBySearch(search);
    }

    @Override
    public List<ReceiptsHistory> getAllReceiptsHistoryByDate(Date fromDate, Date toDate, String value) {
        return receiptsHistoryRepo.getReceiptsHistoriesByDate(fromDate,toDate,value);
    }

    @Override
    public void insert(ReceiptsHistory receiptsHistory) {
        receiptsHistoryRepo.save(receiptsHistory);
    }
}
