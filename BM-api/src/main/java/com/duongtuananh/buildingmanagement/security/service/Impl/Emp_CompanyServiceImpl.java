package com.duongtuananh.buildingmanagement.security.service.Impl;

import com.duongtuananh.buildingmanagement.entity.Emp_Company;
import com.duongtuananh.buildingmanagement.entity.log_data.EmployeeCompanyHistory;
import com.duongtuananh.buildingmanagement.repository.Emp_CompanyRepository;
import com.duongtuananh.buildingmanagement.repository.log_data.EmpCompanyHistoryRepo;
import com.duongtuananh.buildingmanagement.security.service.service.Emp_CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class Emp_CompanyServiceImpl implements Emp_CompanyService {
    @Autowired
    private Emp_CompanyRepository emp_companyRepository;

    @Autowired
    private EmpCompanyHistoryRepo empCompanyHistoryRepo;

    @Override
    public List<Emp_Company> getAllEmpCompany() {
        return emp_companyRepository.findAll();
    }

    @Override
    public void saveEmpCompany(Emp_Company emp_company) {
        emp_companyRepository.save(emp_company);
    }

    @Override
    public void deleteEmpCompany(Integer ID) {
        emp_companyRepository.deleteById(ID);
    }

    @Override
    public Optional<Emp_Company> getEmpCompanyById(Integer ID) {
        return emp_companyRepository.findById(ID);
    }

    @Override
    public List<Emp_Company> getEmployeesCompanyBySearch(String search) {
        return emp_companyRepository.getEmployeesCompanyBySearch(search);
    }

    @Override
    public List<Emp_Company> getEmployeeByCompany(Integer idCompany) {
        return emp_companyRepository.getEmployeeByCompany(idCompany);
    }

    @Override
    public List<EmployeeCompanyHistory> getAllEmpCompanyHistory() {
        return empCompanyHistoryRepo.findAll();
    }

    @Override
    public List<EmployeeCompanyHistory> getAllEmpsCompanyHistoryBySearch(String search) {
        return empCompanyHistoryRepo.getEmployeesCompanyHistoriesBySearch(search);
    }

    @Override
    public List<EmployeeCompanyHistory> getAllEmpsCompanyHistoryByDate(Date fromDate, Date toDate, String value) {
        return empCompanyHistoryRepo.getEmployeesCompanyHistoriesByDate(fromDate,toDate,value);
    }

    @Override
    public void insert(EmployeeCompanyHistory employeeCompanyHistory) {
        empCompanyHistoryRepo.save(employeeCompanyHistory);
    }
}
