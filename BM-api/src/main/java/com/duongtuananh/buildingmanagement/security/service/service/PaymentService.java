package com.duongtuananh.buildingmanagement.security.service.service;

import com.duongtuananh.buildingmanagement.entity.Payment;

import java.util.List;
import java.util.Optional;

public interface PaymentService {
    List<Payment> getAllPayment();
    void savePayent(Payment payment);
    void deletePayment(Integer ID);
    Optional<Payment> getPaymentByID(Integer ID);
}
