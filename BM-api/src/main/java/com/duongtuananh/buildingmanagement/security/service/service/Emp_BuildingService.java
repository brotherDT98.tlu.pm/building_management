package com.duongtuananh.buildingmanagement.security.service.service;

import com.duongtuananh.buildingmanagement.entity.Emp_Building;
import com.duongtuananh.buildingmanagement.entity.log_data.EmployyeBuildingHistory;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface Emp_BuildingService {
    List<Emp_Building> getAllEmpBuilding();
    void saveEmpBuilding(Emp_Building emp_building);
    void deleteEmpBuilding(Integer ID);
    Optional<Emp_Building> getEmpBuildingByID(Integer ID);
    List<Emp_Building> getEmployeesBuildingBySearch(String search);
    List<EmployyeBuildingHistory> getAllEmpBuildingHistory();
    List<EmployyeBuildingHistory> getAllEmpsBuildingHistoryBySearch(String search);
    List<EmployyeBuildingHistory> getAllEmpsBuildingHistoryByDate(Date fromDate, Date toDate, String value);
    void insert(EmployyeBuildingHistory employyeBuildingHistory);
}
