package com.duongtuananh.buildingmanagement.api;
import com.duongtuananh.buildingmanagement.entity.Contract;
import com.duongtuananh.buildingmanagement.entity.log_data.ContractHistory;
import com.duongtuananh.buildingmanagement.entity.view.ContractView;
import com.duongtuananh.buildingmanagement.entity.view.InvoiceView;
import com.duongtuananh.buildingmanagement.security.service.service.ContractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/contract")
public class ContractAPI {
    @Autowired
    private ContractService contractService;

    ContractAPI (ContractService contractService) {
        super();
        this.contractService = contractService;
    }
    @GetMapping
    public List<Contract> getAllContract() {
        return contractService.getAllContract();
    }

    @GetMapping(value = "/AllDetails")
    public List<ContractView> getAllContractDetail() {
        return contractService.getAllDetailsContract();
    }

    @PostMapping
    public String insertContract(@RequestBody Contract contract) {
        contractService.saveContract(contract);
        return "Inserted !";
    }

    @PutMapping(value = "/{ID}")
    public String updateContract(@RequestBody Contract contract,@PathVariable("ID") Integer ID) {
        contract.setId(ID);
        contractService.saveContract(contract);
        return "Updated !";
    }

    @DeleteMapping(value = "/{ID}")
    public String deleteContract(@PathVariable("ID") Integer ID) {
        contractService.deleteContract(ID);
        return "Deleted !";
    }

    @GetMapping(value = "/{ID}")
    public Optional<Contract> getContractByID(@PathVariable("ID") Integer ID) {
        return contractService.getContractByID(ID);
    }

    @GetMapping(value = "detail/{ID}")
    public Optional<ContractView> getContractDetailByID(@PathVariable("ID") Integer ID) {
        return contractService.getContractDetailByID(ID);
    }

    @GetMapping(value = "/invoice")
    public List<InvoiceView> getInvoices(@RequestParam Integer id) {
        return contractService.getInvoices(id);
    }

    @GetMapping(value = "/search")
    public List<ContractView> getContractsBySearch(@RequestParam String search) {
        return contractService.getContractsBySearch(search);
    }

    @GetMapping(value = "/byCompany")
    public List<ContractView> getContractByCompany(@RequestParam Integer id_company) {
        return contractService.getContractByCompany(id_company);
    }

    @GetMapping(value = "/history")
    public List<ContractHistory> getContractsHistory() {
        return contractService.getAllContractHistory();
    }

    @GetMapping(value = "/history/search")
    public List<ContractHistory> getContractsHistoryBySearch(@RequestParam String search) {
        return contractService.getContractsHistoryBySearch(search);
    }

    @GetMapping(value = "/history/byDate")
    public List<ContractHistory> getContractsHistoryByDate(@RequestParam Date from_date, @RequestParam Date to_date, @RequestParam(required = false) String value) {
        return contractService.getContractsHistoryByDate(from_date,to_date,value);
    }

    @PostMapping(value = "/history")
    public void insert(@RequestBody ContractHistory contractHistory) {
        contractService.insert(contractHistory);
    }
}
