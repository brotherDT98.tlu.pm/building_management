package com.duongtuananh.buildingmanagement.api;

import com.duongtuananh.buildingmanagement.entity.auth.ERole;
import com.duongtuananh.buildingmanagement.entity.auth.PasswordResetToken;
import com.duongtuananh.buildingmanagement.entity.auth.Role;
import com.duongtuananh.buildingmanagement.entity.auth.User;
import com.duongtuananh.buildingmanagement.payload.request.LoginRequest;
import com.duongtuananh.buildingmanagement.payload.request.SignupRequest;
import com.duongtuananh.buildingmanagement.payload.response.JwtResponse;
import com.duongtuananh.buildingmanagement.payload.response.MessageResponse;
import com.duongtuananh.buildingmanagement.repository.auth.RoleRepository;
import com.duongtuananh.buildingmanagement.repository.auth.UserRepository;
import com.duongtuananh.buildingmanagement.security.jwt.JwtUtils;
import com.duongtuananh.buildingmanagement.security.service.Impl.auth.UserDetailsImpl;
import com.duongtuananh.buildingmanagement.security.service.service.ISecurityUserService;
import com.duongtuananh.buildingmanagement.security.service.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {
	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	UserRepository userRepository;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	PasswordEncoder encoder;

	@Autowired
	JwtUtils jwtUtils;

	@Autowired
	ISecurityUserService iSecurityUserService;

	@Autowired
	UserService userService;

	@PostMapping("/signin")
	public ResponseEntity<?> authenticateUser(@RequestBody LoginRequest loginRequest) {

		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

		SecurityContextHolder.getContext().setAuthentication(authentication);
		String jwt = jwtUtils.generateJwtToken(authentication);

		UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
		List<String> roles = userDetails.getAuthorities().stream()
				.map(item -> item.getAuthority())
				.collect(Collectors.toList());

		return ResponseEntity.ok(new JwtResponse(jwt,
				userDetails.getId(),
				userDetails.getUsername(),
				userDetails.getEmail(),
				roles));
	}

	@PostMapping("/signup")
	public ResponseEntity<?> registerUser(@RequestBody SignupRequest signUpRequest) {
		if (userRepository.existsByUsername(signUpRequest.getUsername())) {
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Error: Username is already taken!"));
		}

		if (userRepository.existsByEmail(signUpRequest.getEmail())) {
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Error: Email is already in use!"));
		}

		// Create new user's account
		User user = new User(signUpRequest.getUsername(),
				signUpRequest.getEmail(),
				encoder.encode(signUpRequest.getPassword()));

		Set<String> strRoles = signUpRequest.getRole();
		Set<Role> roles = new HashSet<>();

		if (strRoles == null) {
			Role userRole = roleRepository.findByName(ERole.ROLE_USER)
					.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
			roles.add(userRole);
		} else {
			strRoles.forEach(role -> {
				switch (role) {
					case "admin":
						Role adminRole = roleRepository.findByName(ERole.ROLE_ADMIN)
								.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
						roles.add(adminRole);

						break;
					case "mod":
						Role modRole = roleRepository.findByName(ERole.ROLE_MODERATOR)
								.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
						roles.add(modRole);

						break;
					default:
						Role userRole = roleRepository.findByName(ERole.ROLE_USER)
								.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
						roles.add(userRole);
				}
			});
		}

		user.setRoles(roles);
		userRepository.save(user);

		return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
	}

	@PostMapping("/user/resetPassword")
	public ResponseEntity<?> resetPassword(HttpServletRequest request,
										@RequestParam("email") String email) {
		User user = userService.findUserByEmail(email);
		if (user == null) {
			System.out.println("Not found user !");
		}
		String token = UUID.randomUUID().toString();
		iSecurityUserService.createPasswordResetTokenForUser(user, token);
		return ResponseEntity.ok(new MessageResponse("Create successfully !"));
	}

	@PostMapping("/user/savePassword")
	public ResponseEntity<?> savePassword(@RequestParam("token") String  token,@RequestParam("new_pass") String newPass) {

//		String result = iSecurityUserService.validatePasswordResetToken(token);

//		if(result != null) {
//			return ResponseEntity.ok(new MessageResponse("auth.message !"));
//		}

		Optional<User> user = Optional.ofNullable(userService.getUserByPasswordResetToken(token));
		if(user.isPresent()) {
			userService.changeUserPassword(user.get(), newPass);
			return ResponseEntity.ok(new MessageResponse("Update successfully !"));
		} else {
			return ResponseEntity.ok(new MessageResponse("Invalid !"));
		}
	}
	@GetMapping("/user/getTokenResetPass")
	public String getTokenResetPass(@RequestParam("email") String email) {
		User user = userService.findUserByEmail(email);
//		Long userId = user.getId();
		PasswordResetToken passwordResetToken = iSecurityUserService.findByUser(user);

		return passwordResetToken.getToken();
	}
}
