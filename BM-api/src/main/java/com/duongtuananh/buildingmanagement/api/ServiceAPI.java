package com.duongtuananh.buildingmanagement.api;
import com.duongtuananh.buildingmanagement.entity.Service;
import com.duongtuananh.buildingmanagement.entity.log_data.RegisterServiceHistory;
import com.duongtuananh.buildingmanagement.entity.log_data.ServiceHistory;
import com.duongtuananh.buildingmanagement.security.service.service.Service_Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/service",produces = "application/json;charset=UTF-8")
public class ServiceAPI {
    @Autowired
    private Service_Service service_service;

    @GetMapping
    public List<Service> getAllService() {
        return service_service.getAllService();
    }

    @GetMapping(value = "/{ID}")
    public Optional<Service> getServiceById(@PathVariable Integer ID) {
        return service_service.getServiceById(ID);
    }

    @PostMapping
    public Service insertService(@RequestBody Service service) {
        service_service.saveService(service);
        return service;
    }

    @PutMapping(value = "/{ID}")
    public String updateService(@RequestBody Service service,@PathVariable("{ID}") Integer ID) {
        service.setID_Service(ID);
        service_service.saveService(service);
        return "Updated !";
    }

    @DeleteMapping(value = "/{ID}")
    public String deleteService(@PathVariable("ID") Integer ID) {
        service_service.deleteService(ID);
        return "Deleted !";
    }

    @GetMapping(value = "/search")
    public List<Service> getServicesBySearch(@RequestParam String search) {
        return service_service.getServicesBySearch(search);
    }

    @GetMapping(value = "/history")
    public List<ServiceHistory> getServiceHistory() {
        return service_service.getAllServiceHistory();
    }

    @GetMapping(value = "/history/search")
    public List<ServiceHistory> getServiceHistoryBySearch(@RequestParam String search) {
        return service_service.getAllServicesHistoryBySearch(search);
    }

    @GetMapping(value = "/history/byDate")
    public List<ServiceHistory> getServiceHistoryByDate(@RequestParam Date from_date, @RequestParam Date to_date, @RequestParam(required = false) String value) {
        return service_service.getAllServicesHistoryByDate(from_date,to_date,value);
    }

    @PostMapping(value = "/history")
    public void insert(@RequestBody ServiceHistory serviceHistory) {
        service_service.insert(serviceHistory);
    }
}
