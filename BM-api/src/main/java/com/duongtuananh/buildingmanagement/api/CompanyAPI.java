package com.duongtuananh.buildingmanagement.api;
import com.duongtuananh.buildingmanagement.entity.Company;
import com.duongtuananh.buildingmanagement.entity.log_data.CompanyHistory;
import com.duongtuananh.buildingmanagement.security.service.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/company")
public class CompanyAPI {
    @Autowired
    private CompanyService companyService;
    @GetMapping
    public List<Company> getAllCompany() {
        return companyService.getAllCompany();
    }

    @GetMapping(value = "/{ID}")
    public Optional<Company> getCpnByID (@PathVariable("ID") Integer ID) {
        return companyService.getCpnByID(ID);
    }

    @PostMapping
    public String insertCompany(@RequestBody Company company) {
        companyService.saveCompany(company);
        return "Inserted !";
    }

    @PutMapping(value = "/{ID}")
    public String updateCompany(@RequestBody Company company,@PathVariable("ID") Integer ID) {
        company.setID_Cpn(ID);
        companyService.saveCompany(company);
        return "Updated !";
    }

    @DeleteMapping(value = "/{ID}")
    public String deleteCompany(@PathVariable("ID") Integer ID) {
        companyService.deleteCompany(ID);
        return "Deleted !";
    }

    @GetMapping(value = "/search")
    public List<Company> getCompaniesBySearch(@RequestParam String search) {
        return companyService.getCompaniesBySearch(search);
    }

    @GetMapping(value = "/history")
    public List<CompanyHistory> getCompaniesHistory() {
        return companyService.getAllCompanyHistory();
    }

    @GetMapping(value = "/history/search")
    public List<CompanyHistory> getCompaniesHistoryBySearch(@RequestParam String search) {
        return companyService.getCompanyHistoriesBySearch(search);
    }

    @GetMapping(value = "/history/byDate")
    public List<CompanyHistory> getCompaniesHistoryByDate(@RequestParam Date from_date, @RequestParam Date to_date, @RequestParam(required = false) String value) {
        return companyService.getCompanyHistoriesByDate(from_date,to_date,value);
    }
    @PostMapping(value = "/history")
    public void insertAfterDeleteCompanyHistory(@RequestBody CompanyHistory  companyHistory){
         companyService.insertCompanyHistory(companyHistory);
    }
}
