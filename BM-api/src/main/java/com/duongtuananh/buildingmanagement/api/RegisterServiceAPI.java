package com.duongtuananh.buildingmanagement.api;
import com.duongtuananh.buildingmanagement.entity.RegisterService;
import com.duongtuananh.buildingmanagement.entity.log_data.EmployeeCompanyHistory;
import com.duongtuananh.buildingmanagement.entity.log_data.RegisterServiceHistory;
import com.duongtuananh.buildingmanagement.entity.view.RegisterServiceView;
import com.duongtuananh.buildingmanagement.security.service.service.RegisterService_Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
    @RequestMapping(value = "/registerservice")
public class RegisterServiceAPI {
    @Autowired
    private RegisterService_Service registerService_service;

    @GetMapping
    public List<RegisterService> getAllRegisterService() {
        return registerService_service.getAllRegisterService();
    }

    @PostMapping
    public String insertRegisterService(@RequestBody RegisterService registerService) {
        registerService_service.saveRegisterService(registerService);
        return "Inserted !";
    }

    @PutMapping(value = "/{ID}")
    public String updateRegisterService(@RequestBody RegisterService registerService,@PathVariable("ID") Integer ID) {
        registerService.setID_Register(ID);
        registerService_service.saveRegisterService(registerService);
        return "Updated !";
    }

    @DeleteMapping(value = "/{ID}")
    public String deleteRegisterService(@PathVariable("ID") Integer ID) {
        registerService_service.deleteRegisterService(ID);
        return "Deleted !";
    }

    @GetMapping(value = "/details")
    public List<RegisterServiceView> getAllDetails() {
        return registerService_service.getAllRegisDetails();
    }

    @GetMapping(value = "/{ID}")
    public Optional<RegisterService> getResgisterServiceByID(@PathVariable("ID") Integer ID) {
        return registerService_service.getRegisterServiceByID(ID);
    }

    @GetMapping(value = "/byCompany")
    public List<RegisterServiceView> getListRegisterServiceCompany(@RequestParam String code) {
        return registerService_service.getListRegisterServiceByCodeCompany(code);
    }

    @GetMapping(value = "/search")
    public List<RegisterServiceView> getRegisterServicesBySearch(@RequestParam String search) {
        return registerService_service.getRegisterServicesBySearch(search);
    }

    @GetMapping(value = "/history")
    public List<RegisterServiceHistory> getRegisterServiceHistory() {
        return registerService_service.getAllRegisterServiceHistory();
    }

    @GetMapping(value = "/history/search")
    public List<RegisterServiceHistory> getRegisterServiceHistoryBySearch(@RequestParam String search) {
        return registerService_service.getAllRegisterServiceHistoryBySearch(search);
    }

    @GetMapping(value = "/history/byDate")
    public List<RegisterServiceHistory> getRegisterServiceHistoryByDate(@RequestParam Date from_date, @RequestParam Date to_date, @RequestParam(required = false) String value) {
        return registerService_service.getAllRegisterServiceHistoryByDate(from_date,to_date,value);
    }

    @PostMapping(value = "/history")
    public void insert(@RequestBody RegisterServiceHistory registerServiceHistory) {
        registerService_service.insert(registerServiceHistory);
    }
}
