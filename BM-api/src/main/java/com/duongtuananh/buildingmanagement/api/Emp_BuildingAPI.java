package com.duongtuananh.buildingmanagement.api;
import com.duongtuananh.buildingmanagement.entity.Emp_Building;
import com.duongtuananh.buildingmanagement.entity.log_data.ContractHistory;
import com.duongtuananh.buildingmanagement.entity.log_data.EmployeeCompanyHistory;
import com.duongtuananh.buildingmanagement.entity.log_data.EmployyeBuildingHistory;
import com.duongtuananh.buildingmanagement.security.service.service.Emp_BuildingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/emp-building")
public class Emp_BuildingAPI {
    @Autowired
    private Emp_BuildingService emp_buildingService;

    @GetMapping
    public List<Emp_Building> getAllEmployee() {
        return emp_buildingService.getAllEmpBuilding();
    }

    @PostMapping
    public String insertEmployee(@RequestBody Emp_Building emp_building) {
        emp_buildingService.saveEmpBuilding(emp_building);
        System.out.println(emp_building.toString());
        return "Inserted !";
    }

    @PutMapping(value = "/{ID}")
    public String updateEmployee(@RequestBody Emp_Building emp_building,@PathVariable("ID") Integer ID) {
        emp_building.setID_Emp(ID);
        emp_buildingService.saveEmpBuilding(emp_building);
        return "Updated !";
    }

    @DeleteMapping(value = "/{ID}")
    public String deleteEmployee(@PathVariable("ID") Integer ID) {
        emp_buildingService.deleteEmpBuilding(ID);
        return "Deleted !";
    }

    @GetMapping(value = "/{ID}")
    public Optional<Emp_Building> getEmpBuildingByID(@PathVariable("ID") Integer ID) {
        return emp_buildingService.getEmpBuildingByID(ID);
    }

    @GetMapping(value = "/search")
    public List<Emp_Building> getEmployeesBuildingBySearch(@RequestParam String search) {
        return emp_buildingService.getEmployeesBuildingBySearch(search);
    }

    @GetMapping(value = "/history")
    public List<EmployyeBuildingHistory> getEmployyeBuildingHistory() {
        return emp_buildingService.getAllEmpBuildingHistory();
    }

    @GetMapping(value = "history/search")
    public List<EmployyeBuildingHistory> getEmployyeBuildingHistoryBySearch(@RequestParam String search) {
        return emp_buildingService.getAllEmpsBuildingHistoryBySearch(search);
    }

    @GetMapping(value = "/history/byDate")
    public List<EmployyeBuildingHistory> getEmployyeBuildingHistoryByDate(@RequestParam Date from_date, Date to_date, String value) {
        return emp_buildingService.getAllEmpsBuildingHistoryByDate(from_date,to_date,value);
    }

    @PostMapping(value = "/history")
    public void insert(@RequestBody EmployyeBuildingHistory employyeBuildingHistory) {
        emp_buildingService.insert(employyeBuildingHistory);
    }
}
