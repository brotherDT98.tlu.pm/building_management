package com.duongtuananh.buildingmanagement.api;
import com.duongtuananh.buildingmanagement.entity.Customer;
import com.duongtuananh.buildingmanagement.entity.log_data.ContractHistory;
import com.duongtuananh.buildingmanagement.entity.log_data.CustomerHistory;
import com.duongtuananh.buildingmanagement.entity.view.CustomerView;
import com.duongtuananh.buildingmanagement.security.service.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/customer")
public class CustomerAPI {
    @Autowired
    private CustomerService customerService;

    @GetMapping
    public List<Customer> getAllCustomer() {
        return customerService.getAllCustomer();
    }

    @PostMapping
    public String insertCustomer(@RequestBody Customer customer) {
        customerService.saveCustomer(customer);
        return "Inserted !";
    }

    @PutMapping(value = "/{ID}")
    public String updateCustomer(@RequestBody Customer customer,@PathVariable("ID") Integer ID) {
        customer.setId(ID);
        customerService.saveCustomer(customer);
        return "Updated !";
    }

    @DeleteMapping(value = "/{ID}")
    public String deleteCustomer(@PathVariable("ID") Integer ID) {
        customerService.deleteCustomer(ID);
        return "Deleted !";
    }

    @GetMapping(value = "/{ID}")
    public Optional<Customer> getCustomerByID(@PathVariable("ID") Integer ID) {
        return customerService.getCusByID(ID);
    }

    @GetMapping(value = "/details")
    public List<CustomerView> getAllCusDetails() {
        return customerService.getAllCustomerDetails();
    }
        @GetMapping(value = "/details/{id_Cus}")
    public Optional<CustomerView> getCusById_Cus(@PathVariable("id_Cus") Integer id_Cus) {
        return customerService.getCusDetailsByID_Cus(id_Cus);
    }

    @GetMapping(value = "/search")
    public List<CustomerView> getCustomersBySearch(@RequestParam String search) {
        return customerService.getCustomersBySearch(search);
    }

    @GetMapping(value = "/history")
    public List<CustomerHistory> getCustomersHistory() {
        return customerService.getAllCustomerHistory();
    }

    @GetMapping(value = "history/search")
    public List<CustomerHistory> getCustomersHistoryBySearch(@RequestParam String search) {
        return customerService.getAllCustomersHistoryBySearch(search);
    }

    @GetMapping(value = "/history/byDate")
    public List<CustomerHistory> getCustomersHistoryByDate(@RequestParam Date from_date,Date to_date, String value) {
        return customerService.getAllCustomersHistoryByDate(from_date,to_date,value);
    }

    @PostMapping(value = "/history")
    public void insert(@RequestBody CustomerHistory customerHistory) {
        customerService.insert(customerHistory);
    }

    @GetMapping(value = "/byCompany")
    public List<CustomerView> getCusByCompany(@RequestParam String name){
        return customerService.getCusByCompany(name);
    }
}
