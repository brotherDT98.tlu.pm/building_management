package com.duongtuananh.buildingmanagement.api;

import com.duongtuananh.buildingmanagement.entity.Company;
import com.duongtuananh.buildingmanagement.entity.auth.ERole;
import com.duongtuananh.buildingmanagement.entity.auth.Role;
import com.duongtuananh.buildingmanagement.entity.auth.User;
import com.duongtuananh.buildingmanagement.payload.request.SignupRequest;
import com.duongtuananh.buildingmanagement.repository.auth.RoleRepository;
import com.duongtuananh.buildingmanagement.security.jwt.JwtUtils;
import com.duongtuananh.buildingmanagement.security.service.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@RestController
@RequestMapping(value = "/allUsers")
public class UserAPI {

    @Autowired
    private UserService userService;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    JwtUtils jwtUtils;

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping
    public List<User> getAllUsers () {
        return userService.getAllUser();
    }

    @GetMapping(value = "/update")
    public String updateUser(@RequestParam Long userId,@RequestParam Integer roleId) {
//        user.setId(ID);
//        user.setUsername(username);
//        User newUser = new User(user.getId(),user.getUsername(),
//                user.getEmail(),
//                encoder.encode(user.getPassword()), user.getRoles());

        userService.updateUserRole(userId,roleId);
        return "Updated !";
    }


    @DeleteMapping(value = "/{ID}")
    public String deleteUser(@PathVariable("ID") Long ID) {
        userService.deleteUser(ID);
        return "Deleted !";
    }

    @GetMapping(value = "/{ID}")
    public Optional<User> getUserById (@PathVariable("ID") Long ID) {
        return userService.getUserById(ID);
    }

    @GetMapping(value = "/search")
    public List<User> getUsersBySearch(@RequestParam String search) {
        return userService.getUsersBySearch(search);
    }

}
