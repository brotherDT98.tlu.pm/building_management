package com.duongtuananh.buildingmanagement.api;
import com.duongtuananh.buildingmanagement.entity.Emp_Company;
import com.duongtuananh.buildingmanagement.entity.log_data.ContractHistory;
import com.duongtuananh.buildingmanagement.entity.log_data.EmployeeCompanyHistory;
import com.duongtuananh.buildingmanagement.security.service.service.Emp_CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/employeecompany")
public class Emp_CompanyAPI {
    @Autowired
    private Emp_CompanyService emp_companyService;

    @GetMapping
    public List<Emp_Company> getAllEmployee() {
        return emp_companyService.getAllEmpCompany();
    }

    @GetMapping(value = "/{ID}")
    public Optional<Emp_Company> getEmpCompanyByID(@PathVariable("ID") Integer ID) {
        return emp_companyService.getEmpCompanyById(ID);
    }

    @PostMapping
    public String insertEmployee(@RequestBody Emp_Company emp_company) {
        emp_companyService.saveEmpCompany(emp_company);
        return "Inserted !";
    }

    @PutMapping(value = "/{ID}")
    public String updateEmployee(@RequestBody Emp_Company emp_company,@PathVariable("ID") Integer ID) {
        emp_company.setID_Emp(ID);
        emp_companyService.saveEmpCompany(emp_company);
        return "Updated !";
    }

    @DeleteMapping(value = "/{ID}")
    public String deleteEmployee(@PathVariable("ID") Integer ID) {
        emp_companyService.deleteEmpCompany(ID);
        return "Deleted !";
    }

    @GetMapping(value = "/search")
    public List<Emp_Company> getEmployeesCompanyBySearch(@RequestParam String search) {
        return emp_companyService.getEmployeesCompanyBySearch(search);
    }

    @GetMapping(value = "/byCompany")
    public List<Emp_Company> getEmployeeByCompany(@RequestParam Integer id_company) {
        return emp_companyService.getEmployeeByCompany(id_company);
    }

    @GetMapping(value = "/history")
    public List<EmployeeCompanyHistory> getEmployeeCompanyHistory () {
        return emp_companyService.getAllEmpCompanyHistory();
    }

    @GetMapping(value = "history/search")
    public List<EmployeeCompanyHistory> getEmployeeCompanyHistoryBySearch(@RequestParam String search) {
        return emp_companyService.getAllEmpsCompanyHistoryBySearch(search);
    }

    @GetMapping(value = "/history/byDate")
    public List<EmployeeCompanyHistory> getEmployeeCompanyHistoryByDate(@RequestParam Date from_date, Date to_date, String value) {
        return emp_companyService.getAllEmpsCompanyHistoryByDate(from_date,to_date,value);
    }

    @PostMapping(value = "/history")
    public void insert(@RequestBody EmployeeCompanyHistory employeeCompanyHistory) {
        emp_companyService.insert(employeeCompanyHistory);
    }
}
