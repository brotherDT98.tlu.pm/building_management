package com.duongtuananh.buildingmanagement.api;
import com.duongtuananh.buildingmanagement.entity.Receipts;
import com.duongtuananh.buildingmanagement.entity.log_data.EmployeeCompanyHistory;
import com.duongtuananh.buildingmanagement.entity.log_data.ReceiptsHistory;
import com.duongtuananh.buildingmanagement.entity.view.InvoiceTotalView;
import com.duongtuananh.buildingmanagement.security.service.service.ReceiptsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/receipts")
public class ReceiptsAPI {
    @Autowired
    private ReceiptsService receiptsService;

    @GetMapping
    public List<Receipts> getAllReceipts() {
        return receiptsService.getAllReceipts();
    }

    @PostMapping
    public String insertReceipts(@RequestBody Receipts receipts) {
        receiptsService.saveReceipts(receipts);
        return "Inserted !";
    }

    @PutMapping(value = "/{ID}")
    public String updateReceipts(@RequestBody Receipts receipts,@PathVariable("ID") Integer ID) {
        receipts.setId(ID);
        receiptsService.saveReceipts(receipts);
        return "Updated !";
    }

    @DeleteMapping(value = "/{ID}")
    public String deleteReceipts(@PathVariable("ID") Integer ID) {
        receiptsService.deleteReceipts(ID);
        return "Deleted !";
    }

    @GetMapping(value = "/{ID}")
    public Optional<Receipts> getReceiptsByID(@PathVariable("ID") Integer ID) {
        return receiptsService.getReceiptsByID(ID);
    }

    @GetMapping(value = "/all")
    public List<InvoiceTotalView> getAllInvoiceTotal() {
        return receiptsService.getAllInvoiceTotal();
    }

    @GetMapping(value = "/byID")
    public Optional<InvoiceTotalView> getInvoiceTotal(@RequestParam Integer id) {
        return receiptsService.getInvoiceTotal(id);
    }
    @GetMapping(value = "/search")
    public List<InvoiceTotalView> getInvoicesTotalBySearch(@RequestParam String search) {
        return receiptsService.getInvoicesTotalBySearch(search);
    }

    @GetMapping(value = "/byDate")
    public List<InvoiceTotalView> getInvoicesTotalByDate(@RequestParam Date fromDate,@RequestParam Date toDate) {
        return receiptsService.getInvoicesTotalByDate(fromDate,toDate);
    }

    @GetMapping(value = "/history")
    public List<ReceiptsHistory> getReceiptsHistory() {
        return receiptsService.getAllReceiptsHistory();
    }

    @GetMapping(value = "/history/search")
    public List<ReceiptsHistory> getReceiptsHistoryBySearch(@RequestParam String search) {
        return receiptsService.getAllReceiptsHistoryBySearch(search);
    }

    @GetMapping(value = "/history/byDate")
    public List<ReceiptsHistory> getReceiptsHistoryByDate(@RequestParam Date from_date, @RequestParam Date to_date, @RequestParam(required = false) String value) {
        return receiptsService.getAllReceiptsHistoryByDate(from_date,to_date,value);
    }

    @PostMapping(value = "/history")
    public void insert(@RequestBody ReceiptsHistory receiptsHistory) {
        receiptsService.insert(receiptsHistory);
    }
}
