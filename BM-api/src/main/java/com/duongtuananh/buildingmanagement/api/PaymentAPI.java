package com.duongtuananh.buildingmanagement.api;

import com.duongtuananh.buildingmanagement.entity.Payment;
import com.duongtuananh.buildingmanagement.security.service.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/payment")
public class PaymentAPI {
    @Autowired
    private PaymentService paymentService;

    @GetMapping
    public List<Payment> getAllPayment() {
        return paymentService.getAllPayment();
    }

    @PostMapping
    public String insertPayment(@RequestBody Payment payment) {
        paymentService.savePayent(payment);
        return "Inserted !";
    }

    @PutMapping(value = "/{ID}")
    public String updatePayment(@RequestBody Payment payment,@PathVariable("ID") Integer ID) {
        payment.setId(ID);
        paymentService.savePayent(payment);
        return "Updated !";
    }

    @DeleteMapping(value = "/{ID}")
    public String deletePayment(@PathVariable("ID") Integer ID) {
        paymentService.deletePayment(ID);
        return "Deleted !";
    }

    @GetMapping(value = "/{ID}")
    public Optional<Payment> getPaymentByID(@PathVariable("ID") Integer ID) {
        return paymentService.getPaymentByID(ID);
    }
}
