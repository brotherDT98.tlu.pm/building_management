package com.duongtuananh.buildingmanagement.api;

import com.duongtuananh.buildingmanagement.entity.Leased_Premises;
import com.duongtuananh.buildingmanagement.entity.log_data.EmployeeCompanyHistory;
import com.duongtuananh.buildingmanagement.entity.log_data.LeasedPremisesHistory;
import com.duongtuananh.buildingmanagement.payload.response.MessageResponse;
import com.duongtuananh.buildingmanagement.repository.Leased_PremisesRepository;
import com.duongtuananh.buildingmanagement.security.service.service.Leased_PremisesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/leased-premises")
public class Leased_PremisesAPI {
    @Autowired
    private Leased_PremisesService leasedPremisesService;

    @GetMapping
    public List<Leased_Premises> getAllLP() {
        return leasedPremisesService.getAllLP();
    }

    @Autowired
    private Leased_PremisesRepository leasedPremisesRepository;

    @PostMapping
    public ResponseEntity<?> insertLP(@RequestBody Leased_Premises leasedPremises) {
        if (leasedPremisesRepository.existsByCodeLP(leasedPremises.getCode()) == 1) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Mã mặt bằng đã tồn tại !"));
        }
        if (leasedPremisesRepository.existsByNameLP(leasedPremises.getName()) == 1) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Tên mặt bằng đã tồn tại !"));
        }
        leasedPremisesService.saveLP(leasedPremises);
        return ResponseEntity.ok(new MessageResponse("Create successfully !"));
    }

    @PutMapping(value = "/{ID}")
    public ResponseEntity<?> updateLP(@RequestBody Leased_Premises leasedPremises,@PathVariable("ID") Integer ID) {
//        if (leasedPremisesRepository.existsByCodeLP(leasedPremises.getCode()) == 1) {
//            return ResponseEntity
//                    .badRequest()
//                    .body(new MessageResponse("Error: Mã mặt bằng đã tồn tại !"));
//        }
//        if (leasedPremisesRepository.existsByNameLP(leasedPremises.getName()) == 1) {
//            return ResponseEntity
//                    .badRequest()
//                    .body(new MessageResponse("Error: Tên mặt bằng đã tồn tại !"));
//        }
        leasedPremises.setId(ID);
        leasedPremisesService.saveLP(leasedPremises);
        return ResponseEntity.ok(new MessageResponse("Update successfully !"));
    }

    @DeleteMapping(value = "/{ID}")
    public String deleteLP(@PathVariable("ID") Integer ID) {
        leasedPremisesService.deleteLP(ID);
        return "Deleted !";
    }

    @GetMapping(value = "/{ID}")
    public Optional<Leased_Premises> getLPByID(@PathVariable("ID") Integer ID) {
        return leasedPremisesService.getLPByID(ID);
    }

    @GetMapping(value = "/search")
    public List<Leased_Premises> getLeasedPremisesBySearch(String search) {
        return leasedPremisesService.getLeasedPremisesBySearch(search);
    }

    @GetMapping(value = "/history")
    public List<LeasedPremisesHistory> getAllLPHistory() {
        return leasedPremisesService.getAllLPHistory();
    }

    @GetMapping(value = "/history/search")
    public List<LeasedPremisesHistory> getLeasedPremisesHistoriesBySearch(@RequestParam String search){
        return leasedPremisesService.getLeasedPremisesHistoriesBySearch(search);
    }

    @GetMapping(value = "/history/byDate")
    public List<LeasedPremisesHistory> getLeasedPremisesHistoriesByDate(@RequestParam Date from_date, @RequestParam Date to_date , @RequestParam(required = false) String value) {
        return leasedPremisesService.getLeasedPremisesHistoriesByDate(from_date,to_date, value);
    }
    @PostMapping(value = "/history")
    public void insert(@RequestBody LeasedPremisesHistory leasedPremisesHistory) {
        leasedPremisesService.insert(leasedPremisesHistory);
    }

    @GetMapping(value = "/status")
    List<Leased_Premises> getLPByStatus() {
        return leasedPremisesService.getLPByStatus();
    }

}
