package com.duongtuananh.buildingmanagement.repository.auth;

import com.duongtuananh.buildingmanagement.entity.auth.User;
import com.duongtuananh.buildingmanagement.entity.view.CustomerView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
@EnableJpaRepositories
public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByUsername(String username);

    Boolean existsByUsername(String username);

    Boolean existsByEmail(String email);

    @Query(
            value = "SELECT * \n" +
                    "FROM users \n" +
                    "WHERE CONVERT (id USING utf8mb4) LIKE %?1%  OR \n" +
                    "          CONVERT(email USING utf8mb4) LIKE %?1% \n" +
                    "          OR \n" +
                    "          CONVERT(username USING utf8mb4) LIKE %?1%\n",
            nativeQuery = true
    )
    List<User> getUsersBySearch(String search);

    @Modifying
    @Transactional
    @Query(
            value = "update user_roles set role_id  = ?2 where user_id = ?1",
            nativeQuery = true
    )
    void updateRoles(Long userID, Integer role_id);

    User findUserByEmail(String email);
}
