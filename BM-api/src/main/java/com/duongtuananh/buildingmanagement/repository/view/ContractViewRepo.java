package com.duongtuananh.buildingmanagement.repository.view;

import com.duongtuananh.buildingmanagement.entity.view.ContractView;
import com.duongtuananh.buildingmanagement.entity.view.CustomerView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@EnableJpaRepositories
public interface ContractViewRepo extends JpaRepository<ContractView, Integer> {
    @Query(
            value = "SELECT * \n" +
                    "FROM contractview \n" +
                    "WHERE CONVERT (id USING utf8mb4) LIKE %?1%  OR \n" +
                    "          CONVERT(code USING utf8mb4) LIKE %?1% \n" +
                    "          OR \n" +
                    "          CONVERT(name_LD USING utf8mb4) LIKE %?1% \n" +
                    "          OR \n" +
                    "          CONVERT(name_Cpn USING utf8mb4) LIKE %?1% \n" +
                    "          OR\n" +
                    "          CONVERT(name_Cus USING utf8mb4) LIKE %?1% \n" +
                    "          OR\n" +
                    "          CONVERT(name_Emp USING utf8mb4) LIKE %?1% \n" +
                    "          OR\n" +
                    "          CONVERT(created_at USING utf8mb4) LIKE %?1% \n" +
                    "          OR\n" +
                    "          CONVERT(created_by USING utf8mb4) LIKE %?1% \n" +
                    "          OR\n" +
//                    "          CONVERT(update_at USING utf8mb4) LIKE %?1% \n" +
//                    "          OR\n" +
//                    "          CONVERT(update_by USING utf8mb4) LIKE %?1% \n" +
//                    "          OR\n" +
                    "          CONVERT(start_Date USING utf8mb4) LIKE %?1% \n" +
                    "          OR\n" +
                    "          CONVERT(end_Date USING utf8mb4) LIKE %?1% \n" +
                    "          OR\n" +
                    "          CONVERT(deposit USING utf8mb4) LIKE %?1%\n",
            nativeQuery = true
    )
    List<ContractView> getContractsBySearch(String search);

    @Query(
            value = "SELECT * FROM contractview WHERE id_Cpn like ?1",
            nativeQuery = true
    )
    List<ContractView> getContractViewByCompany(Integer idCpn);
}
