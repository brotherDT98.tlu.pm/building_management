package com.duongtuananh.buildingmanagement.repository.view;

import com.duongtuananh.buildingmanagement.entity.view.ContractView;
import com.duongtuananh.buildingmanagement.entity.view.InvoiceTotalView;
import com.duongtuananh.buildingmanagement.entity.view.RegisterServiceView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface InvoiceTotalViewRepo extends JpaRepository<InvoiceTotalView, Integer> {
    @Query(
            value = "SELECT * FROM invoicetotal WHERE id like ?1",
            nativeQuery = true
    )
    Optional<InvoiceTotalView> getIvoiceTotal(Integer id);

    @Query(
            value = "SELECT * \n" +
                    "FROM invoicetotal \n" +
                    "WHERE CONVERT (id USING utf8mb4) LIKE %?1%  OR \n" +
                    "          CONVERT(code USING utf8mb4) LIKE %?1% \n" +
                    "          OR \n" +
                    "          CONVERT(name_cus USING utf8mb4) LIKE %?1% \n" +
                    "          OR \n" +
                    "          CONVERT(name_emp USING utf8mb4) LIKE %?1% \n" +
                    "          OR\n" +
                    "          CONVERT(name_cpn USING utf8mb4) LIKE %?1% \n" +
                    "          OR\n" +
                    "          CONVERT(type_Pay USING utf8mb4) LIKE %?1% \n" +
                    "          OR\n" +
                    "          CONVERT(created_by USING utf8mb4) LIKE %?1% \n" +
                    "          OR\n" +
//                    "          CONVERT(update_at USING utf8mb4) LIKE %?1% \n" +
//                    "          OR\n" +
//                    "          CONVERT(update_by USING utf8mb4) LIKE %?1% \n" +
//                    "          OR\n" +
                    "          CONVERT(created_at USING utf8mb4) LIKE %?1%\n",
            nativeQuery = true
    )
    List<InvoiceTotalView> getInvoicesTotalBySearch(String search);

    @Query(
            value = "SELECT * FROM buildingmanagement.invoicetotal\n" +
                    "where day_Create between :fromDate and :toDate ",
            nativeQuery = true
    )
    List<InvoiceTotalView> getInvoicesTotalByDate(Date fromDate, Date toDate);
}
