package com.duongtuananh.buildingmanagement.repository;

import com.duongtuananh.buildingmanagement.entity.Emp_Building;
import com.duongtuananh.buildingmanagement.entity.Emp_Company;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface Emp_BuildingRepository extends JpaRepository<Emp_Building,Integer> {
    @Query(
            value = "SELECT * \n" +
                    "FROM Emp_Building \n" +
                    "WHERE CONVERT (ID_Emp USING utf8mb4) LIKE %?1%  OR \n" +
                    "          CONVERT(Emp_Code USING utf8mb4) LIKE %?1% \n" +
                    "          OR \n" +
                    "          CONVERT(Name_Emp USING utf8mb4) LIKE %?1% \n" +
                    "          OR\n" +
                    "          CONVERT(Gender USING utf8mb4) LIKE %?1% \n" +
                    "          OR\n" +
                    "          CONVERT(ID_Card USING utf8mb4) LIKE %?1% \n" +
                    "          OR\n" +
                    "          CONVERT(DofB USING utf8mb4) LIKE %?1% \n" +
                    "          OR\n" +
                    "          CONVERT(Address USING utf8mb4) LIKE %?1% \n" +
                    "          OR\n" +
                    "          CONVERT(Phone_Number USING utf8mb4) LIKE %?1% \n" +
                    "          OR\n" +
                    "          CONVERT(Job_Position USING utf8mb4) LIKE %?1% \n" +
                    "          OR\n" +
                    "          CONVERT(ID_Emp_Manager USING utf8mb4) LIKE %?1%\n",
            nativeQuery = true
    )
    List<Emp_Building> getEmployeesBuildingBySearch(String search);
}
