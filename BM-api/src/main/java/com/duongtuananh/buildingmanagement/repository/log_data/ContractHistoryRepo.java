package com.duongtuananh.buildingmanagement.repository.log_data;
import com.duongtuananh.buildingmanagement.entity.log_data.ContractHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;

public interface ContractHistoryRepo extends JpaRepository<ContractHistory, Long> {
    @Query(
            value = "SELECT * \n" +
                    "FROM contract_history \n" +
                    "WHERE CONVERT (code USING utf8mb4) LIKE %?1%  OR \n" +
                    "          CONVERT(id_LD USING utf8mb4) LIKE %?1% \n" +
                    "          OR \n" +
                    "          CONVERT(id_Cpn USING utf8mb4) LIKE %?1% \n" +
                    "          OR \n" +
                    "          CONVERT(id_Cus USING utf8mb4) LIKE %?1% \n" +
                    "          OR\n" +
                    "          CONVERT(id_Emp USING utf8mb4) LIKE %?1%\n" +
                    "           OR\n" +
//                    "          CONVERT(date_Of_Delivery USING utf8mb4) LIKE %?1%\n" +
//                    "           OR\n" +
                    "          CONVERT(start_Date USING utf8mb4) LIKE %?1%\n" +
                    "           OR\n" +
                    "          CONVERT(end_Date USING utf8mb4) LIKE %?1%\n" +
                    "           OR\n" +
                    "          CONVERT(status USING utf8mb4) LIKE %?1%\n" +
                    "           OR\n" +
                    "          CONVERT(deposit USING utf8mb4) LIKE %?1%\n" +
                    "           OR\n" +
                    "          CONVERT(type_his USING utf8mb4) LIKE %?1%\n" +
                    "           OR\n" +
                    "          CONVERT(created_by USING utf8mb4) LIKE %?1%\n" +
                    "           OR\n" +
                    "          CONVERT(created_at USING utf8mb4) LIKE %?1% ",
            nativeQuery = true
    )
    List<ContractHistory> getContractsHistoriesBySearch(String search);

    @Query(
            value = "SELECT * FROM contract_history \n" +
                    "where ((created_at between ?1 and ?2) and type_his like %?3%) or " +
                    "((created_at between ?1 and ?2) and code like %?3%) " +
                    "or ((created_at between ?1 and ?2) and id_LD like %?3%) " +
                    "or ((created_at between ?1 and ?2) and created_by like %?3%) " +
                    "or ((created_at between ?1 and ?2) and id_Cpn like %?3%) " +
                    "or ((created_at between ?1 and ?2) and id_Cus like %?3%) " +
                    "or ((created_at between ?1 and ?2) and id_Emp like %?3%) " +
                    "or ((created_at between ?1 and ?2) and date_Of_Delivery like %?3%) " +
                    "or ((created_at between ?1 and ?2) and start_Date like %?3%) " +
                    "or ((created_at between ?1 and ?2) and end_Date like %?3%) " +
                    "or ((created_at between ?1 and ?2) and status like %?3%) " +
                    "or ((created_at between ?1 and ?2) and deposit like %?3%)",
            nativeQuery = true
    )
    List<ContractHistory> getContractsHistoriesByDate(Date fromDate, Date toDate, String value);
}
