package com.duongtuananh.buildingmanagement.repository.view;

import com.duongtuananh.buildingmanagement.entity.view.CustomerView;
import com.duongtuananh.buildingmanagement.entity.view.RegisterServiceView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface RegisServiceViewRepo extends JpaRepository<RegisterServiceView,Integer> {
    @Transactional
    @Modifying
    @Query(
            value = "select * from registerserviceview where name_Cpn like ?1",
            nativeQuery = true
    )
    List<RegisterServiceView> getListServiceByCompany(String code);

    @Query(
            value = "SELECT * \n" +
                    "FROM registerserviceview \n" +
                    "WHERE CONVERT (id_Register USING utf8mb4) LIKE %?1%  OR \n" +
                    "          CONVERT(cpn_Code USING utf8mb4) LIKE %?1% \n" +
                    "          OR \n" +
                    "          CONVERT(name_Cpn USING utf8mb4) LIKE %?1% \n" +
                    "          OR \n" +
                    "          CONVERT(service_Code USING utf8mb4) LIKE %?1% \n" +
                    "          OR\n" +
                    "          CONVERT(name_Service USING utf8mb4) LIKE %?1% \n" +
                    "          OR\n" +
                    "          CONVERT(created_at USING utf8mb4) LIKE %?1% \n" +
                    "          OR\n" +
                    "          CONVERT(created_by USING utf8mb4) LIKE %?1% \n" +
                    "          OR\n" +
//                    "          CONVERT(update_at USING utf8mb4) LIKE %?1% \n" +
//                    "          OR\n" +
//                    "          CONVERT(update_by USING utf8mb4) LIKE %?1% \n" +
 //                   "          OR\n" +
                    "          CONVERT(end_Date USING utf8mb4) LIKE %?1%\n",
            nativeQuery = true
    )
    List<RegisterServiceView> getRegisterServicesBySearch(String search);
}
