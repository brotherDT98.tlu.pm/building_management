package com.duongtuananh.buildingmanagement.repository.log_data;
import com.duongtuananh.buildingmanagement.entity.log_data.EmployeeCompanyHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;

public interface EmpCompanyHistoryRepo extends JpaRepository<EmployeeCompanyHistory,Long> {
    @Query(
            value = "SELECT * \n" +
                    "FROM emp_company_history \n" +
                    "WHERE CONVERT (Emp_Code USING utf8mb4) LIKE %?1%  OR \n" +
                    "          CONVERT(Name_Emp USING utf8mb4) LIKE %?1% \n" +
                    "          OR \n" +
                    "          CONVERT(Gender USING utf8mb4) LIKE %?1% \n" +
                    "          OR \n" +
                    "          CONVERT(ID_Card USING utf8mb4) LIKE %?1% \n" +
                    "          OR\n" +
                    "          CONVERT(DofB USING utf8mb4) LIKE %?1%\n" +
                    "           OR\n" +
                    "          CONVERT(Address USING utf8mb4) LIKE %?1%\n" +
                    "           OR\n" +
                    "          CONVERT(Phone_Number USING utf8mb4) LIKE %?1%\n" +
                    "           OR\n" +
                    "          CONVERT(ID_Cpn USING utf8mb4) LIKE %?1%\n" +
                    "           OR\n" +
                    "          CONVERT(type_his USING utf8mb4) LIKE %?1%\n" +
                    "           OR\n" +
                    "          CONVERT(created_by USING utf8mb4) LIKE %?1%\n" +
                    "           OR\n" +
                    "          CONVERT(created_at USING utf8mb4) LIKE %?1% ",
            nativeQuery = true
    )
    List<EmployeeCompanyHistory> getEmployeesCompanyHistoriesBySearch(String search);

    @Query(
            value = "SELECT * FROM emp_company_history \n" +
                    "where ((created_at between ?1 and ?2) and type_his like %?3%) or " +
                    "((created_at between ?1 and ?2) and Emp_Code like %?3%) " +
                    "or ((created_at between ?1 and ?2) and Name_Emp like %?3%) " +
                    "or ((created_at between ?1 and ?2) and created_by like %?3%) " +
                    "or ((created_at between ?1 and ?2) and Gender like %?3%) " +
                    "or ((created_at between ?1 and ?2) and ID_Card like %?3%) " +
                    "or ((created_at between ?1 and ?2) and DofB like %?3%) " +
                    "or ((created_at between ?1 and ?2) and Address like %?3%) " +
                    "or ((created_at between ?1 and ?2) and Phone_Number like %?3%) " +
                    "or ((created_at between ?1 and ?2) and ID_Cpn like %?3%)",
            nativeQuery = true
    )
    List<EmployeeCompanyHistory> getEmployeesCompanyHistoriesByDate(Date fromDate, Date toDate, String value);
}
