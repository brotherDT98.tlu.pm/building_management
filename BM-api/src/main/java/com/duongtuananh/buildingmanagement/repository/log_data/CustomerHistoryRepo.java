package com.duongtuananh.buildingmanagement.repository.log_data;
import com.duongtuananh.buildingmanagement.entity.log_data.CustomerHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;

public interface CustomerHistoryRepo extends JpaRepository<CustomerHistory,Long> {
    @Query(
            value = "SELECT * \n" +
                    "FROM customer_history \n" +
                    "WHERE CONVERT (code USING utf8mb4) LIKE %?1%  OR \n" +
                    "          CONVERT(id_Cus USING utf8mb4) LIKE %?1% \n" +
                    "          OR \n" +
                    "          CONVERT(email USING utf8mb4) LIKE %?1% \n" +
                    "          OR \n" +
                    "          CONVERT(name_Bank USING utf8mb4) LIKE %?1% \n" +
                    "          OR\n" +
                    "          CONVERT(bank_Account_Number USING utf8mb4) LIKE %?1%\n" +
                    "           OR\n" +
                    "          CONVERT(type_his USING utf8mb4) LIKE %?1%\n" +
                    "           OR\n" +
                    "          CONVERT(created_by USING utf8mb4) LIKE %?1%\n" +
                    "           OR\n" +
                    "          CONVERT(created_at USING utf8mb4) LIKE %?1% ",
            nativeQuery = true
    )
    List<CustomerHistory> getCustomersHistoriesBySearch(String search);

    @Query(
            value = "SELECT * FROM customer_history \n" +
                    "where ((created_at between ?1 and ?2) and type_his like %?3%) or " +
                    "((created_at between ?1 and ?2) and id_Cus like %?3%) " +
                    "or ((created_at between ?1 and ?2) and email like %?3%) " +
                    "or ((created_at between ?1 and ?2) and created_by like %?3%) " +
                    "or ((created_at between ?1 and ?2) and name_Bank like %?3%) " +
                    "or ((created_at between ?1 and ?2) and bank_Account_Number like %?3%) " +
                    "or ((created_at between ?1 and ?2) and code like %?3%)",
            nativeQuery = true
    )
    List<CustomerHistory> getCustomersHistoriesByDate(Date fromDate, Date toDate, String value);
}
