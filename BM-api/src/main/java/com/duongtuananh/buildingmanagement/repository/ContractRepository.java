package com.duongtuananh.buildingmanagement.repository;

import com.duongtuananh.buildingmanagement.entity.Contract;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface ContractRepository extends JpaRepository<Contract,Integer> {
    @Transactional
    @Modifying
    @Query(
            value = "\tupdate contract c, company cpn , leased_premises lp, emp_building emp_b, emp_company emp_c  \n" +
                    "\t\tset  c.name_LD = lp.name, c.name_Cpn = cpn.Name_Cpn, c.name_Cus = emp_c.Name_Emp, c.name_Emp = emp_b.Name_Emp\n" +
                    "        where c.id>0 and c.id_Cpn = cpn.ID_Cpn and c.id_LD = lp.id and c.id_Cus = emp_c.ID_Emp and c.id_Emp = emp_b.ID_Emp;",
            nativeQuery = true)
    void updateAllContract();

    @Query(value = "select id, code, name_LD, name_Cpn, name_Cus, name_Emp, date_Of_Delivery,start_Date,end_Date,status,deposit\n" +
            "from contract ",
        nativeQuery = true
    )
    List<Contract> getAllContract();
}
