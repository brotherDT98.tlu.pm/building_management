package com.duongtuananh.buildingmanagement.repository.log_data;
import com.duongtuananh.buildingmanagement.entity.log_data.CompanyHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface CompanyHistoryRepo extends JpaRepository<CompanyHistory,Long> {
    @Query(
            value =  "SELECT * \n" +
                    "FROM company_history \n" +
                    "WHERE CONVERT (cpn_code USING utf8mb4) LIKE %?1%  OR \n" +
                    "          CONVERT(name_cpn USING utf8mb4) LIKE %?1% \n" +
                    "          OR \n" +
                    "          CONVERT(tax_code USING utf8mb4) LIKE %?1% \n" +
                    "          OR \n" +
                    "          CONVERT(capital USING utf8mb4) LIKE %?1% \n" +
                    "          OR\n" +
                    "          CONVERT(num_of_emp USING utf8mb4) LIKE %?1%\n" +
                    "           OR\n" +
                    "          CONVERT(address USING utf8mb4) LIKE %?1%\n" +
                    "           OR\n" +
                    "          CONVERT(phone_number USING utf8mb4) LIKE %?1%\n" +
                    "           OR\n" +
                    "          CONVERT(type_his USING utf8mb4) LIKE %?1%\n" +
                    "           OR\n" +
                    "          CONVERT(created_by USING utf8mb4) LIKE %?1%\n" +
                    "           OR\n" +
                    "          CONVERT(created_at USING utf8mb4) LIKE %?1% ",
            nativeQuery = true
    )
    List<CompanyHistory> getCompanyHistoriesBySearch(String search);

    @Query(
            value = "SELECT * FROM company_history \n" +
                    "where ((created_at between ?1 and ?2) and type_his like %?3%) or " +
                    "((created_at between ?1 and ?2) and cpn_code like %?3%) " +
                    "or ((created_at between ?1 and ?2) and name_cpn like %?3%) " +
                    "or ((created_at between ?1 and ?2) and created_by like %?3%) " +
                    "or ((created_at between ?1 and ?2) and tax_code like %?3%) " +
                    "or ((created_at between ?1 and ?2) and num_of_emp like %?3%) " +
                    "or ((created_at between ?1 and ?2) and address like %?3%) " +
                    "or ((created_at between ?1 and ?2) and phone_number like %?3%) " +
                    "or ((created_at between ?1 and ?2) and capital like %?3%)",
            nativeQuery = true
    )
    List<CompanyHistory> getCompanyHistoriesByDate(Date fromDate, Date toDate, String value);

}
