package com.duongtuananh.buildingmanagement.repository.log_data;
import com.duongtuananh.buildingmanagement.entity.log_data.LeasedPremisesHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import java.util.Date;
import java.util.List;

public interface LeasedPremisesHistoryRepo extends JpaRepository<LeasedPremisesHistory,Long> {
    @Query(
            value = "SELECT * \n" +
                    "FROM leased_premises_history \n" +
                    "WHERE CONVERT (code USING utf8mb4) LIKE %?1%  OR \n" +
                    "          CONVERT(name USING utf8mb4) LIKE %?1% \n" +
                    "          OR \n" +
                    "          CONVERT(floors USING utf8mb4) LIKE %?1% \n" +
                    "          OR \n" +
                    "          CONVERT(acreage USING utf8mb4) LIKE %?1% \n" +
                    "          OR\n" +
                    "          CONVERT(status USING utf8mb4) LIKE %?1%\n" +
                    "           OR\n" +
                    "          CONVERT(type_his USING utf8mb4) LIKE %?1%\n" +
                    "           OR\n" +
                    "          CONVERT(created_by USING utf8mb4) LIKE %?1%\n" +
                    "           OR\n" +
                    "          CONVERT(created_at USING utf8mb4) LIKE %?1% ",
            nativeQuery = true
    )
    List<LeasedPremisesHistory> getLeasedPremisesHistoriesBySearch(String search);

    @Query(
            value = "SELECT * FROM leased_premises_history \n" +
                    "where ((created_at between ?1 and ?2) and type_his like %?3%) or " +
                    "((created_at between ?1 and ?2) and floors like %?3%) " +
                    "or ((created_at between ?1 and ?2) and acreage like %?3%) " +
                    "or ((created_at between ?1 and ?2) and created_by like %?3%) " +
                    "or ((created_at between ?1 and ?2) and status like %?3%) " +
                    "or ((created_at between ?1 and ?2) and code like %?3%)",
            nativeQuery = true
    )
    List<LeasedPremisesHistory> getLeasedPremisesHistoriesByDate(Date fromDate, Date toDate, String value);
}
