package com.duongtuananh.buildingmanagement.repository;

import com.duongtuananh.buildingmanagement.entity.Company;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CompanyRepository extends JpaRepository<Company,Integer> {
    @Query(
            value = "SELECT * \n" +
                    "FROM company \n" +
                    "WHERE CONVERT (name_Cpn USING utf8mb4) LIKE %?1%  OR \n" +
                    "          CONVERT(ID_Cpn USING utf8mb4) LIKE %?1% \n" +
                    "          OR \n" +
                    "          CONVERT(Cpn_Code USING utf8mb4) LIKE %?1% \n" +
                    "          OR \n" +
                    "          CONVERT(Tax_Code USING utf8mb4) LIKE %?1% \n" +
                    "          OR\n" +
                    "          CONVERT(capital USING utf8mb4) LIKE %?1%\n" +
                    "           OR\n" +
                    "          CONVERT(num_Of_Emp USING utf8mb4) LIKE %?1%\n" +
                    "           OR\n" +
                    "          CONVERT(address USING utf8mb4) LIKE %?1%\n" +
                    "           OR\n" +
                    "          CONVERT(phone_Number USING utf8mb4) LIKE %?1% ",
            nativeQuery = true
    )
    List<Company> getCompaniesBySearch(String search);

}
