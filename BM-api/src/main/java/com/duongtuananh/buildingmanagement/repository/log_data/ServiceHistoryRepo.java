package com.duongtuananh.buildingmanagement.repository.log_data;
import com.duongtuananh.buildingmanagement.entity.log_data.ServiceHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;

public interface ServiceHistoryRepo extends JpaRepository<ServiceHistory,Long> {
    @Query(
            value = "SELECT * \n" +
                    "FROM service_history \n" +
                    "WHERE CONVERT (Service_Code USING utf8mb4) LIKE %?1%  OR \n" +
                    "          CONVERT(Name_Service USING utf8mb4) LIKE %?1% \n" +
                    "          OR \n" +
                    "          CONVERT(Unit_Price USING utf8mb4) LIKE %?1% \n" +
                    "          OR \n" +
                    "          CONVERT(type_his USING utf8mb4) LIKE %?1%\n" +
                    "           OR\n" +
                    "          CONVERT(created_by USING utf8mb4) LIKE %?1%\n" +
                    "           OR\n" +
                    "          CONVERT(created_at USING utf8mb4) LIKE %?1% ",
            nativeQuery = true
    )
    List<ServiceHistory> getServicesHistoriesBySearch(String search);

    @Query(
            value = "SELECT * FROM service_history \n" +
                    "where ((created_at between ?1 and ?2) and type_his like %?3%) or " +
                    "((created_at between ?1 and ?2) and Service_Code like %?3%) " +
                    "or ((created_at between ?1 and ?2) and created_by like %?3%) " +
                    "or ((created_at between ?1 and ?2) and Name_Service like %?3%) " +
                    "or ((created_at between ?1 and ?2) and Unit_Price like %?3%)",
            nativeQuery = true
    )
    List<ServiceHistory> getServicesHistoriesByDate(Date fromDate, Date toDate, String value);
}
