package com.duongtuananh.buildingmanagement.repository.log_data;
import com.duongtuananh.buildingmanagement.entity.log_data.RegisterServiceHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import java.util.Date;
import java.util.List;

public interface RegisterServiceHistoryRepo extends JpaRepository<RegisterServiceHistory,Long> {
    @Query(
            value = "SELECT * \n" +
                    "FROM register_service_history \n" +
                    "WHERE CONVERT (ID_Cpn USING utf8mb4) LIKE %?1%  OR \n" +
                    "          CONVERT(ID_Service USING utf8mb4) LIKE %?1% \n" +
                    "          OR \n" +
                    "          CONVERT(End_Date USING utf8mb4) LIKE %?1%\n" +
                    "           OR\n" +
                    "          CONVERT(type_his USING utf8mb4) LIKE %?1%\n" +
                    "           OR\n" +
                    "          CONVERT(created_by USING utf8mb4) LIKE %?1%\n" +
                    "           OR\n" +
                    "          CONVERT(updated_by USING utf8mb4) LIKE %?1%\n" +
                    "           OR\n" +
                    "          CONVERT(updated_at USING utf8mb4) LIKE %?1%\n" +
                    "           OR\n" +
                    "          CONVERT(created_at USING utf8mb4) LIKE %?1% ",
            nativeQuery = true
    )
    List<RegisterServiceHistory> getRegisterServiceHistoriesBySearch(String search);

    @Query(
            value = "SELECT * FROM register_service_history \n" +
                    "where ((created_at between ?1 and ?2) and type_his like %?3%) or " +
                    "((created_at between ?1 and ?2) and ID_Cpn like %?3%) " +
                    "or ((created_at between ?1 and ?2) and created_by like %?3%) " +
                    "or ((created_at between ?1 and ?2) and ID_Service like %?3%) " +
                    "or ((created_at between ?1 and ?2) and End_Date like %?3%) "
                    ,
            nativeQuery = true
    )
    List<RegisterServiceHistory> getRegisterServiceHistoriesByDate(Date fromDate, Date toDate, String value);
}
