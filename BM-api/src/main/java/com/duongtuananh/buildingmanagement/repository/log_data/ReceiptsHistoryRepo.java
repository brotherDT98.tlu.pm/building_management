package com.duongtuananh.buildingmanagement.repository.log_data;
import com.duongtuananh.buildingmanagement.entity.log_data.ReceiptsHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;

public interface ReceiptsHistoryRepo extends JpaRepository<ReceiptsHistory,Long> {
    @Query(
            value = "SELECT * \n" +
                    "FROM receipts_history \n" +
                    "WHERE CONVERT (code USING utf8mb4) LIKE %?1%  OR \n" +
//                    "          CONVERT(day_Create USING utf8mb4) LIKE %?1% \n" +
//                    "          OR \n" +
                    "          CONVERT(amomunt_Of_Money USING utf8mb4) LIKE %?1% \n" +
                    "          OR \n" +
                    "          CONVERT(id_Cus USING utf8mb4) LIKE %?1% \n" +
                    "          OR\n" +
                    "          CONVERT(id_Emp USING utf8mb4) LIKE %?1%\n" +
                    "           OR\n" +
                    "          CONVERT(id_Cpn USING utf8mb4) LIKE %?1%\n" +
                    "           OR\n" +
                    "          CONVERT(type_Pay USING utf8mb4) LIKE %?1%\n" +
                    "           OR\n" +
                    "          CONVERT(note USING utf8mb4) LIKE %?1%\n" +
                    "           OR\n" +
                    "          CONVERT(status USING utf8mb4) LIKE %?1%\n" +
                    "           OR\n" +
                    "          CONVERT(type_his USING utf8mb4) LIKE %?1%\n" +
                    "           OR\n" +
                    "          CONVERT(created_by USING utf8mb4) LIKE %?1%\n" +
                    "           OR\n" +
                    "          CONVERT(created_at USING utf8mb4) LIKE %?1% ",
            nativeQuery = true
    )
    List<ReceiptsHistory> getReceiptsHistoriesBySearch(String search);

    @Query(
            value = "SELECT * FROM receipts_history \n" +
                    "where ((created_at between ?1 and ?2) and type_his like %?3%) or " +
                    "((created_at between ?1 and ?2) and code like %?3%) " +
                    "or ((created_at between ?1 and ?2) and day_Create like %?3%) " +
                    "or ((created_at between ?1 and ?2) and created_by like %?3%) " +
                    "or ((created_at between ?1 and ?2) and amomunt_Of_Money like %?3%) " +
                    "or ((created_at between ?1 and ?2) and id_Cus like %?3%) " +
                    "or ((created_at between ?1 and ?2) and id_Emp like %?3%) " +
                    "or ((created_at between ?1 and ?2) and id_Cpn like %?3%) " +
                    "or ((created_at between ?1 and ?2) and type_Pay like %?3%) " +
                    "or ((created_at between ?1 and ?2) and note like %?3%) " +
                    "or ((created_at between ?1 and ?2) and status like %?3%)",
            nativeQuery = true
    )
    List<ReceiptsHistory> getReceiptsHistoriesByDate(Date fromDate, Date toDate, String value);
}
