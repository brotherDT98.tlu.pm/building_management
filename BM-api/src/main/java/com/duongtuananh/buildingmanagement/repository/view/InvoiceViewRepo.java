package com.duongtuananh.buildingmanagement.repository.view;

import com.duongtuananh.buildingmanagement.entity.view.InvoiceView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import javax.transaction.Transactional;
import java.util.List;

public interface InvoiceViewRepo extends JpaRepository<InvoiceView,Integer> {
    @Transactional
    @Modifying
    @Query(
            value = "select distinct * from invoice where id_Cpn like ?1",
            nativeQuery = true
    )
    List<InvoiceView> getInvoicesByCompany(Integer id);
}
