package com.duongtuananh.buildingmanagement.repository.auth;

import com.duongtuananh.buildingmanagement.entity.auth.ERole;
import com.duongtuananh.buildingmanagement.entity.auth.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(ERole roleName);
}
