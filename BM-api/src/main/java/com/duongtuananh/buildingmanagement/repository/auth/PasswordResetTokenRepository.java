package com.duongtuananh.buildingmanagement.repository.auth;

import com.duongtuananh.buildingmanagement.entity.auth.PasswordResetToken;
import com.duongtuananh.buildingmanagement.entity.auth.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface PasswordResetTokenRepository extends JpaRepository<PasswordResetToken,Integer> {
    PasswordResetToken findByToken(String token);

    @Query(
            value = " SELECT * \n" +
                    " FROM  password_reset_token\n" +
                    " WHERE id IN (SELECT MAX(id) FROM password_reset_token GROUP BY user_id)",
            nativeQuery = true
    )
    PasswordResetToken findByUser(User user);
}
