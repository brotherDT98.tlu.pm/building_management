package com.duongtuananh.buildingmanagement.repository.view;

import com.duongtuananh.buildingmanagement.entity.view.ContractView;
import com.duongtuananh.buildingmanagement.entity.view.CustomerView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CustomerViewRepo extends JpaRepository<CustomerView,Integer> {
    @Query(
            value = "select * from customerview where id_Cus = ?1",
            nativeQuery = true
    )
    Optional<CustomerView> getCusByID_Cus(Integer id_Cus);

    @Query(
            value = "SELECT * \n" +
                    "FROM customerview \n" +
                    "WHERE CONVERT (id USING utf8mb4) LIKE %?1%  OR \n" +
                    "          CONVERT(code USING utf8mb4) LIKE %?1% \n" +
                    "          OR \n" +
                    "          CONVERT(name_Emp USING utf8mb4) LIKE %?1% \n" +
                    "          OR \n" +
                    "          CONVERT(gender USING utf8mb4) LIKE %?1% \n" +
                    "          OR\n" +
                    "          CONVERT(dofB USING utf8mb4) LIKE %?1% \n" +
                    "          OR\n" +
                    "          CONVERT(address USING utf8mb4) LIKE %?1% \n" +
                    "          OR\n" +
                    "          CONVERT(phone_Number USING utf8mb4) LIKE %?1% \n" +
                    "          OR\n" +
                    "          CONVERT(email USING utf8mb4) LIKE %?1% \n" +
                    "          OR\n" +
                    "          CONVERT(name_Bank USING utf8mb4) LIKE %?1% \n" +
                    "          OR\n" +
                    "          CONVERT(bank_Account_Number USING utf8mb4) LIKE %?1% \n" +
                    "          OR\n" +
                    "          CONVERT(name_Cpn USING utf8mb4) LIKE %?1%\n",
            nativeQuery = true
    )
    List<CustomerView> getCustomersBySearch(String search);

    @Query(value = "select * from customerview where name_Cpn like ?1",
    nativeQuery = true)
    List<CustomerView> getCusByCompany(String nameCompany);
}
