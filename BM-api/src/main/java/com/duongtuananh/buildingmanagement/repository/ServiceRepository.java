package com.duongtuananh.buildingmanagement.repository;

import com.duongtuananh.buildingmanagement.entity.Company;
import com.duongtuananh.buildingmanagement.entity.Service;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ServiceRepository extends JpaRepository<Service, Integer> {
    @Query(
            value = "SELECT * \n" +
                    "FROM service \n" +
                    "WHERE CONVERT (ID_Service USING utf8mb4) LIKE %?1%  OR \n" +
                    "          CONVERT(Service_Code USING utf8mb4) LIKE %?1% \n" +
                    "          OR \n" +
                    "          CONVERT(Name_Service USING utf8mb4) LIKE %?1% \n" +
                    "          OR\n" +
                    "          CONVERT(Unit_Price USING utf8mb4) LIKE %?1%\n",
            nativeQuery = true
    )
    List<Service> getServicesBySearch(String search);

}
