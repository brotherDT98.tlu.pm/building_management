package com.duongtuananh.buildingmanagement.repository;

import com.duongtuananh.buildingmanagement.entity.Leased_Premises;
import com.duongtuananh.buildingmanagement.entity.Service;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface Leased_PremisesRepository extends JpaRepository<Leased_Premises,Integer> {
    @Query(
            value = "SELECT * \n" +
                    "FROM Leased_Premises \n" +
                    "WHERE CONVERT (id USING utf8mb4) LIKE %?1%  OR \n" +
                    "          CONVERT(code USING utf8mb4) LIKE %?1% \n" +
                    "          OR \n" +
                    "          CONVERT(name USING utf8mb4) LIKE %?1% \n" +
                    "          OR\n" +
                    "          CONVERT(acreage USING utf8mb4) LIKE %?1% \n" +
                    "          OR\n" +
                    "          CONVERT(status USING utf8mb4) LIKE %?1% \n" +
                    "          OR\n" +
                    "          CONVERT(floors USING utf8mb4) LIKE %?1%\n",
            nativeQuery = true
    )
    List<Leased_Premises> getLeasedPremisesBySearch(String search);


    @Query(value = "select case when count(*)> 0 then true else false end  from leased_premises lp  where lp.code like :code", nativeQuery = true)
    Integer existsByCodeLP(String code);

    @Query(value = "select case when count(*)> 0 then true else false end  from leased_premises lp  where lp.name like :name", nativeQuery = true)
    Integer existsByNameLP(String name);

    @Query(value = "select * from Leased_Premises where status = 0 or status = null ",
    nativeQuery = true)
    List<Leased_Premises> getLPByStatus();
}
