import React, { Component } from "react";
import {Switch, Route, Link, Redirect} from "react-router-dom";
import Admin from "./layouts/Admin";
import Customer from "./layouts/Customer";
import RTL from "./layouts/RTL";
import AuthService from "./authentication/auth.service";
//
// const user = AuthService.getCurrentUser();
// const PrivateRoute = ({ component: Component, ...rest }) => (
//     <Route {...rest} render={(props) => (
//         // if (user) {
//         //     return (
//         //         <Component {...props} />
//         //     )
//         // } else return <Redirect to="/customer/login" />
//             user
//                 ? <div><Component {...props} /> <Redirect from="/" to="/admin/dashboard" /> </div>
//             : <Redirect to='/customer/login' />
//     )} />
//)

class App extends React.Component {
    state = {
        currentUser: undefined,
        roles:[]
    }
    componentDidMount() {
        let user1 = AuthService.getCurrentUser();
        if (user1){
            this.setState({
            currentUser: user1,
                roles: user1.roles
        })}
    }
    render() {
        let {currentUser,roles} = this.state;
        console.log(roles[1])
        return (
            <div>
                { roles.length === 2
                    &&
                        <Switch>
                            <Route path="/admin" component={Admin} />
                            <Redirect from="/" to="/admin/dashboard" />
                        </Switch>
                }
                { roles.length ===1
                    &&
                    <Switch>
                        <Route path="/rtl" component={RTL} />
                        <Redirect from="/" to="/rtl/test" />
                    </Switch>
                }
                { roles.length === 0 &&
                <Switch>
                    <Route path="/customer" component={Customer} />
                    <a href="/customer/login"><h1>Please login here !</h1></a>
                    {/*<Redirect from="/" to="/customer/login" />*/}
                </Switch>
                }
            </div>
        )
    }
}
export default App;