
import Dashboard from "@material-ui/icons/Dashboard";
import Person from "@material-ui/icons/Person";
// core components/views for Admin layout
import DashboardPage from "views/Dashboard/Dashboard.js";
import UserProfile from "views/UserProfile/UserProfile.js";
import TableList from "views/TableList/TableList.js";
import Account from "./views/Account/Account";
import EmployeeBuilding  from "./views/EmployeeBuilding/EmployeeBuilding"
import Contract from "./views/Contract/Contract";
import Invoice from "./views/Invoice/Invoice";
import Test from "./views/Test/Test";
import SignInSide from "./views/Sections/SignInSide";
import Register from "./views/Sections/Register";
import Login from "./views/Sections/SignInSide";
import Company from "./views/Company/Company";
import EmployeeCompany from "./views/EmployeeCompany/EmployeeCompany";
import Customer from "./views/Customer/Customer"
import LeasedPremises from "./views/Leased_Premises/LeasedPremises";
import ServiceBuilding from "./views/ServiceBuilding/ServiceBuilding";
import RegisterService from "./views/RegisterService/RegisterService";

const dashboardRoutes = [
  {
    path: "/dashboard",
    name: "Dashboard",
    rtlName: "لوحة القيادة",
    icon: Dashboard,
    component: DashboardPage,
    layout: "/admin"
  },
  {
    path: "/account",
    name: "Account Table",
    rtlName: "لوحة القيادة",
    icon: "content_paste",
    component: Account,
    layout: "/admin"
  },
  {
    path: "/leased-premises",
    name: "Leased Premises",
    rtlName: "لوحة القيادة",
    icon: "content_paste",
    component: LeasedPremises,
    layout: "/admin"
  },
  {
    path: "/emp-building",
    name: "Employee Building ",
    rtlName: "لوحة القيادة",
    icon: "content_paste",
    component: EmployeeBuilding,
    layout: "/admin"
  },
  {
    path: "/services",
    name: "Service Building",
    rtlName: "لوحة القيادة",
    icon: "content_paste",
    component: ServiceBuilding,
    layout: "/admin"
  },
  {
    path: "/company",
    name: "Company ",
    rtlName: "لوحة القيادة",
    icon: "content_paste",
    component: Company,
    layout: "/admin"
  },
  {
    path: "/emp-company",
    name: "Employee Company ",
    rtlName: "لوحة القيادة",
    icon: "content_paste",
    component: EmployeeCompany,
    layout: "/admin"
  },
  {
    path: "/customers",
    name: "Table Customer ",
    rtlName: "لوحة القيادة",
    icon: "content_paste",
    component: Customer,
    layout: "/admin"
  },
  {
    path: "/user",
    name: "User Profile",
    rtlName: "ملف تعريفي للمستخدم",
    icon: Person,
    component: UserProfile,
    layout: "/admin"
  },
  // {
  //   path: "/table",
  //   name: "Table List",
  //   rtlName: "قائمة الجدول",
  //   icon: "content_paste",
  //   component: TableList,
  //   layout: "/admin"
  // },
  {
    path: "/contract",
    name: "Table Contract",
    rtlName: "قائمة الجدول",
    icon: "content_paste",
    component: Contract,
    layout: "/admin"
  },
  {
    path: "/register-service",
    name: "Register Service",
    rtlName: "قائمة الجدول",
    icon: "content_paste",
    component: RegisterService,
    layout: "/admin"
  },
  {
    path: "/invoice",
    name: "Table Invoice",
    rtlName: "قائمة الجدول",
    icon: "content_paste",
    component: Invoice,
    layout: "/admin"
  },
  // {
  //   path: "/test",
  //   name: "Test Table",
  //   rtlName: "قائمة الجدول",
  //   icon: "content_paste",
  //   component: Test,
  //   layout: "/rtl"
  // },


];

export default dashboardRoutes;
