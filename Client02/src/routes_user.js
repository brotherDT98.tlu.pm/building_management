import Login from "./views/Sections/SignInSide";
import Register from "./views/Sections/Register";

const dashboardRoutes = [
    {
        path: "/login",
        name: "Sign In",
        rtlName: "قائمة الجدول",
        icon: "content_paste",
        component: Login,
        layout: "/customer"
    },
    {
        path: "/register",
        name: "Register",
        rtlName: "قائمة الجدول",
        icon: "content_paste",
        component: Register,
        layout: "/customer"
    },
]
export default dashboardRoutes;