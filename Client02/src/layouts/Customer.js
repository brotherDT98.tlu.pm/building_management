import React from "react";
import {Redirect, Route, Switch} from "react-router-dom";
import routes from "../routes_user";

const switchRoutes = (
    <Switch>
        {routes.map((prop, key) => {
            if (prop.layout === "/customer") {
                return (
                    <Route
                        path={prop.layout + prop.path}
                        component={prop.component}
                        key={key}
                    />
                );
            }
            return null;
        })}
    </Switch>
)
export default function Customer(){
    return (
        <div>
            {switchRoutes}
        </div>
    )
}