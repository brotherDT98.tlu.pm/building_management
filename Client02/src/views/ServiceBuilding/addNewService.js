import React, { Component } from "react";
import {
    Button,
    withStyles
} from "@material-ui/core";
import {ValidatorForm, TextValidator, SelectValidator} from "react-material-ui-form-validator";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import SaveIcon from '@material-ui/icons/Save';
import styles from "./styles"
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import axios from "axios";
import {generateRandomId} from "../../utils"
class AddNewServiceDialog extends Component {
    state = {
        id_Service: "",
        service_Code: "",
        name_Service:"",
        unit_Price:"",
    }

    handleChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    handleFormSubmit = () => {
        axios.post('http://localhost:8082/service',{id_Service: generateRandomId(), ...this.state }).then(() => {
            this.props.handleClose();
        }).catch(err => {
            console.log(err);
        })
    };
    handleChangeValue = (event) => {

    }
    handleChangeService = (e) => {
        this.setState({
            name_Service: e.target.value
        })
    }
    render() {
        let {service_Code,name_Service,unit_Price} = this.state;
        let { open, handleClose } = this.props;
        const {classes} = this.props;
        return (
            <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title" className={classes.bgTitleDialog}>Add/Update</DialogTitle>
                <ValidatorForm ref="form" onSubmit={this.handleFormSubmit} className={classes.formControl}>
                    <DialogContent>
                        <TextValidator className = {classes.textField}
                                       label="Code"
                                       onChange={this.handleChange}
                                       type="text"
                                       name="service_Code"
                                       value={service_Code}
                            // validators={["required","matchRegexp:^(?=.{4,10}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9]+(?<![_.])$"]}
                            // errorMessages={["this field is required", "User name max 10 characters and min 4 character and not special character"]}

                        />

                        <TextValidator className = {classes.textField}
                                       label="Name"
                                       onChange={this.handleChange}
                                       type="text"
                                       name="name_Service"
                                       value={name_Service}
                            // validators={["required","matchRegexp:^(?=.*?[#?!@$%^&*-]).{4,10}$","matchRegexp:^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])"]}
                            //errorMessages={["this field is required","Pass word max 10 character and min 4 character and have least one special character","Must have least one upper case and one lower case"]}
                        />

                        <TextValidator className = {classes.textField}
                                       label="Unit Price"
                                       onChange={this.handleChange}
                                       type="text"
                                       name="unit_Price"
                                       value={unit_Price}
                            // validators={["required","matchRegexp:^(?=.*?[#?!@$%^&*-]).{4,10}$","matchRegexp:^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])"]}
                            //errorMessages={["this field is required","Pass word max 10 character and min 4 character and have least one special character","Must have least one upper case and one lower case"]}
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button variant="contained" color="secondary" type="submit" startIcon={<SaveIcon />}>
                            Save
                        </Button>
                        <Button onClick={() => handleClose()}>Cancel</Button>
                    </DialogActions>
                </ValidatorForm>
            </Dialog>
        );
    }
}

export default withStyles(styles) (AddNewServiceDialog);
