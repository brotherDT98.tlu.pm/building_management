import IconButton from "@material-ui/core/IconButton";
import Icon from "@material-ui/core/Icon";
import React from "react";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import TableHead from "@material-ui/core/TableHead";
import Table from "@material-ui/core/Table";
import Card from "../../components/Card/Card";
import TableBody from "@material-ui/core/TableBody";
import axios from "axios";
import GridItem from "../../components/Grid/GridItem";
import CardHeader from "../../components/Card/CardHeader";
import CardBody from "../../components/Card/CardBody";
import GridContainer from "../../components/Grid/GridContainer";
import {withStyles} from "@material-ui/styles";
import styles from "./styles";
import TablePagination from "@material-ui/core/TablePagination";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogActions from "@material-ui/core/DialogActions";
import TableContainer from "@material-ui/core/TableContainer";
import Paper from "@material-ui/core/Paper";
import AddNewCompanyDiaglog from "./addNewCompanyDiaglog";

const user = JSON.parse(localStorage.getItem('user'));
class Company extends React.Component {
    state = {
        companies: [],
        rowsPerPage:10,
        page:0,
        shouldOpenEditorDialog: false,
        shouldOpenUpdateDialog: false,
        open: false,
        setOpen: false,
    }
    setPage = page => {
        this.setState({page});
    }
    setRowsPerPage = event => {
        this.setState({rowsPerPage: event.target.value});
    }
    handleChangePage = (event,newPage) => {
        this.setPage(newPage);
    }
    componentDidMount() {
        this.updatePageData();
    }
    updatePageData = () => {
        axios.get("http://localhost:8082/company",{headers : {
            Authorization: 'Bearer ' + user.accessToken
        }}).then(res => {
            const companies = res.data;
            this.setState({ companies: companies });
        }).catch(error => console.log(error));
    };

    handleDialogClose = () => {
        this.setState({
            shouldOpenEditorDialog: false,
            shouldOpenUpdateDialog: false
        });
        this.updatePageData();
    };

    handleClickOpen = ID => {
        this.setState({setOpen : true, open : true, ID});
    };

    handleClickClose = () => {
        this.setState({setOpen : false, open : false});
    };

    handleDeleteAcc = (ID) => {
        this.setState({ID});
        console.log(ID);
        this.handleClickOpen(ID);
    };

    handleSubmitDelete = () => {
        // deleteAcc(this.state.ID).then(() => {
        //     this.handleClickClose();
        //     this.updatePageData();
        // });
    };
    render() {
        let  {
            companies,
            page,
            rowsPerPage,
            shouldOpenEditorDialog,
            shouldOpenUpdateDialog
        } = this.state;
        const {classes} = this.props;
        return (
            <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                    <Button
                        className="mb-16"
                        variant="contained"
                        color="primary"
                        onClick={() => this.setState({ shouldOpenEditorDialog: true })}
                    >
                        Add new
                    </Button>
                    <Card>
                        <CardHeader color="primary" >
                            <h4 className={classes.cardTitleWhite}>Simple Table</h4>
                            <p className={classes.cardCategoryWhite}>
                                Here is a subtitle for this table
                            </p>
                        </CardHeader>
                        <CardBody>
                            <div className={classes.tableResponsive}>
                                <Paper className={classes.root}>
                                    <TableContainer className={classes.container}>
                                        <Table className={classes.table}>
                                            <TableHead>
                                                <TableRow className={classes.tableHeadRow}>
                                                    <TableCell style={{textAlign: "center"}} className={classes.tableCell} >ID</TableCell>
                                                    <TableCell style={{textAlign: "center"}} className={classes.tableCell}>COMPANY NAME</TableCell>
                                                    <TableCell style={{textAlign: "center"}} className={classes.tableCell}>COMPANY CODE</TableCell>
                                                    <TableCell style={{textAlign: "center"}} className={classes.tableCell}>PHONE NUMBER</TableCell>
                                                    <TableCell style={{textAlign: "center"}} className={classes.tableCell}>TAX CODE</TableCell>
                                                    <TableCell style={{textAlign: "center"}} className={classes.tableCell}>CAPITAL</TableCell>
                                                    <TableCell style={{textAlign: "center"}} className={classes.tableCell}>AMOUNT EMPLOYEES</TableCell>
                                                    <TableCell style={{textAlign: "center"}} className={classes.tableCell}>ACTION</TableCell>
                                                </TableRow>
                                            </TableHead>
                                            <TableBody>
                                                {companies.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((cpn, index) => (
                                                    <TableRow key={index} className={classes.tableBodyRow}>
                                                        <TableCell style={{textAlign: "center"}} className={classes.tableCell} key={index}>
                                                            {cpn.id_Cpn}
                                                        </TableCell>
                                                        <TableCell style={{textAlign: "center"}} className={classes.tableCell} key={index}>
                                                            {cpn.cpn_Code}
                                                        </TableCell>
                                                        <TableCell style={{textAlign: "center"}} className={classes.tableCell} key={index}>
                                                            {cpn.name_Cpn}
                                                        </TableCell>
                                                        <TableCell style={{textAlign: "center"}} className={classes.tableCell} key={index}>
                                                            {cpn.phone_Number}
                                                        </TableCell>
                                                        <TableCell style={{textAlign: "center"}} className={classes.tableCell} key={index}>
                                                            {cpn.tax_Code}
                                                        </TableCell>
                                                        <TableCell style={{textAlign: "center"}} className={classes.tableCell} key={index}>
                                                            {cpn.capital}
                                                        </TableCell>
                                                        <TableCell style={{textAlign: "center"}} className={classes.tableCell} key={index} >
                                                            {cpn.num_Of_Emp}
                                                        </TableCell>
                                                        <TableCell style={{textAlign: "center"}}  className={classes.tableCell} key={index}>
                                                            <IconButton
                                                                // onClick={() =>
                                                                //     this.setState({
                                                                //         uid: acc.id_Account,
                                                                //         shouldOpenUpdateDialog: true
                                                                //     })
                                                                // }
                                                            >
                                                                <Icon color="primary">edit</Icon>
                                                            </IconButton>
                                                            <IconButton>
                                                                <Icon color="error">delete</Icon>
                                                            </IconButton>
                                                        </TableCell>
                                                    </TableRow>
                                                ))}
                                            </TableBody>
                                        </Table>
                                    </TableContainer>
                                </Paper>
                                <TablePagination
                                    className="px-16"
                                    rowsPerPageOptions={[3, 6, 9]}
                                    component="div"
                                    count={companies.length}
                                    rowsPerPage={rowsPerPage}
                                    page={page}
                                    backIconButtonProps={{
                                        "aria-label": "Previous Page"
                                    }}
                                    nextIconButtonProps={{
                                        "aria-label": "Next Page"
                                    }}
                                    onChangePage={this.handleChangePage}
                                    onChangeRowsPerPage={this.setRowsPerPage}
                                />
                            </div>
                        </CardBody>
                    </Card>
                    {shouldOpenEditorDialog && (
                        <AddNewCompanyDiaglog
                            handleClose={this.handleDialogClose}
                            open={shouldOpenEditorDialog}
                            uid={this.state.uid}
                        />
                    )}
                    {/*{shouldOpenUpdateDialog && (*/}
                    {/*    <UpdateDialog*/}
                    {/*        handleClose={this.handleDialogClose}*/}
                    {/*        open={shouldOpenUpdateDialog}*/}
                    {/*        uid={this.state.uid}*/}
                    {/*    />*/}
                    {/*)}*/}

                    <Dialog
                        open = {this.state.open}
                        onClose={this.handleClickClose}
                        aria-labelledby="alert-dialog-title"
                        aria-describedby="alert-dialog-description"
                    >
                        <DialogTitle id="alert-dialog-title">{"Có chắc chắn xoá ?"}</DialogTitle>
                        <DialogContent>
                            <DialogContentText id="alert-dialog-description">
                                Hãy chắc chắn !.
                            </DialogContentText>
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={this.handleClickClose} color="primary">
                                Huỷ
                            </Button>
                            <Button  color="primary" autoFocus onClick={this.handleSubmitDelete}>
                                Chấp nhận
                            </Button>
                        </DialogActions>
                    </Dialog>
                </GridItem>
            </GridContainer>
        )
    }
}
export default withStyles(styles) (Company);