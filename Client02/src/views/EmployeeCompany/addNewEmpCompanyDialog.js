import React, { Component } from "react";
import {
    Button,
    Grid,
    withStyles
} from "@material-ui/core";
import {ValidatorForm, TextValidator, SelectValidator} from "react-material-ui-form-validator";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import SaveIcon from '@material-ui/icons/Save';
import styles from "./styles";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import axios from "axios";
import {KeyboardDatePicker,MuiPickersUtilsProvider} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import {generateRandomId} from "../../utils";
class AddNewEmpCompanyDialog extends Component {
    state = {
        id_Emp: "",
        emp_Code:"",
        name_Emp:"",
        gender:"",
        id_Card:"",
        dofB: new Date('2020-10-01T21:11:54'),
        address:"",
        phone_Number:"",
        id_Cpn:"",
        cpns:[]
    }
    handleChange = (event, source) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    handleFormSubmit = () => {
        axios.post('http://localhost:8082/employeecompany',{id_Cpn: generateRandomId(), ...this.state }).then(() => {
            this.props.handleClose();
        }).catch(err => {
            console.log(err);
        })
    };
    handleChangeValue = (event) => {
        this.setState({
            id_Cpn: event.target.value,
        });
    }
    handleChangeValueGender = (event) => {
        this.setState({
            gender: event.target.value,
        });
    }
    handleChangeDate = (date) => {
        this.setState({
            dofB:date
        });
    }
    handleChangeValuePosition = (event) => {
        this.setState({
            job_Position: event.target.value
        })
    }
    componentDidMount() {
        axios.get("http://localhost:8082/company").then(res => {
            this.setState({ cpns: res.data });
        }).catch(error => console.log(error));
    }
    render() {
        let {emp_Code,name_Emp,id_Cpn,id_Card,gender,address,dofB,phone_Number,cpns} = this.state;
        let { open, handleClose } = this.props;
        const {classes} = this.props;
        return (
            <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title" className={classes.bgTitleDialog}>Add/Update</DialogTitle>
                <ValidatorForm ref="form" onSubmit={this.handleFormSubmit} className={classes.formControl}>
                    <DialogContent>
                        <TextValidator className = {classes.textField}
                                       label="Full Name"
                                       onChange={this.handleChange}
                                       type="text"
                                       name="name_Emp"
                                       value={name_Emp}
                                       validators={["required","maxStringLength:30"]}
                                       errorMessages={["this field is required","Name maximum 30 character"]}
                        />
                        <TextValidator className = {classes.textField}
                                       label="Employee Code"
                                       onChange={this.handleChange}
                                       type="text"
                                       name="emp_Code"
                                       value={emp_Code}
                                       validators={["required","matchRegexp:^(NV[0-9]{0,8})$"]}
                                       errorMessages={["this field is required", "Employee code must 'NV' characters and number repeat <= 8 times"]}

                        />
                        <br></br>
                        <InputLabel className={classes.textField} label="Standard" shrink={true}>
                            Choose gender
                        </InputLabel>
                        <SelectValidator
                            required
                            value={gender}
                            displayEmpty
                            onChange={this.handleChangeValueGender}
                            validators={["required"]}
                            errorMessages={["this field is required", "email is not valid"]}
                            className= {classes.textField}
                        >
                            <MenuItem value="Dif">
                                <em>Difference</em>
                            </MenuItem>
                            <MenuItem value="Mal">
                                Male
                            </MenuItem>
                            <MenuItem value="Fel">
                                Female
                            </MenuItem>
                        </SelectValidator>
                        <TextValidator className = {classes.textField}
                                       label="ID Identify"
                                       onChange={this.handleChange}
                                       type="text"
                                       name="id_Card"
                                       value={id_Card}
                                       validators={["required","matchRegexp:^([0-9]{9,11})$"]}
                                       errorMessages={["this field is required","Must length min 9 number and max length 11 number"]}
                        />
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <KeyboardDatePicker
                                className = {classes.textField}
                                margin="normal"
                                id="date-picker-dialog"
                                label="Choose birthday"
                                format="yyyy/MM/dd"
                                value={dofB}
                                onChange={this.handleChangeDate}
                                KeyboardButtonProps={{
                                    'aria-label': 'change date',
                                }}
                            />
                        </MuiPickersUtilsProvider>
                        <TextValidator className = {classes.textField}
                                       label="Address"
                                       onChange={this.handleChange}
                                       type="text"
                                       name="address"
                                       value={address}
                                       validators={["required","maxStringLength:50"]}
                                       errorMessages={["this field is required","Max length 50 character"]}
                        />
                        <TextValidator className = {classes.textField}
                                       label="Phone Number"
                                       onChange={this.handleChange}
                                       type="text"
                                       name="phone_Number"
                                       value={phone_Number}
                                       validators={["required","matchRegexp:^[0-9]{10}$"]}
                                       errorMessages={["this field is required","Must 10 numbers"]}
                        />
                        <br></br>
                        <InputLabel className={classes.textField} label="Standard" shrink={true}>
                            Choose Company
                        </InputLabel>
                        <SelectValidator
                            required
                            value={id_Cpn}
                            displayEmpty
                            onChange={this.handleChangeValue}
                            validators={["required"]}
                            errorMessages={["this field is required", "email is not valid"]}
                            className= {classes.textField}
                        >
                            <MenuItem value="">
                                <em>None</em>
                            </MenuItem>
                            {cpns.map((cpn,index) => (
                                <MenuItem value={cpn.id_Cpn}>{cpn.cpn_Code} - {cpn.name_Cpn}</MenuItem>
                            ))};
                        </SelectValidator>
                    </DialogContent>
                    <DialogActions>
                        <Button variant="contained" color="secondary" type="submit" startIcon={<SaveIcon />}>
                            Save
                        </Button>
                        <Button onClick={() => this.props.handleClose()}>Cancel</Button>
                    </DialogActions>
                </ValidatorForm>
            </Dialog>
        );
    }
}

export default withStyles(styles) (AddNewEmpCompanyDialog);
