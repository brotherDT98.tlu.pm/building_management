import React from "react";

import axios from "axios";
import GridContainer from "../../components/Grid/GridContainer";
import GridItem from "../../components/Grid/GridItem";
import Button from "@material-ui/core/Button";
import Card from "../../components/Card/Card";
import CardHeader from "../../components/Card/CardHeader";
import CardBody from "../../components/Card/CardBody";
import Paper from "@material-ui/core/Paper";
import TableContainer from "@material-ui/core/TableContainer";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import IconButton from "@material-ui/core/IconButton";
import Icon from "@material-ui/core/Icon";
import TablePagination from "@material-ui/core/TablePagination";
import AddNewEmpBD from "../EmployeeBuilding/AddNewEmpBD";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogActions from "@material-ui/core/DialogActions";
import styles from "./styles";
import {withStyles} from "@material-ui/styles";
const DATE_OPTIONS = {year: 'numeric', month: 'short', day: 'numeric' };
class RegisterService extends React.Component {

    state = {
        rgs:[],
        rowsPerPage:10,
        page:0,
        shouldOpenEditorDialog: false,
        shouldOpenUpdateDialog: false,
        open: false,
        setOpen: false,
    }
    setPage = page => {
        this.setState({page});
    }
    setRowsPerPage = event => {
        this.setState({rowsPerPage: event.target.value});
    }
    handleChangePage = (event,newPage) => {
        this.setPage(newPage);
    }
    handleDialogClose = () => {
        this.setState({
            shouldOpenEditorDialog: false,
            shouldOpenUpdateDialog: false
        });
        this.updatePageData();
    };

    handleClickOpen = ID => {
        this.setState({setOpen : true, open : true, ID});
    };

    handleClickClose = () => {
        this.setState({setOpen : false, open : false});
    };

    handleDeleteAcc = (ID) => {
        this.setState({ID});
        console.log(ID);
        this.handleClickOpen(ID);
    };

    handleSubmitDelete = () => {
        // deleteEmpBuilding(this.state.ID).then(() => {
        //     this.handleClickClose();
        //     this.updatePageData();
        // });
    };
    componentDidMount() {
        this.updatePageData();
    }
    updatePageData = () => {
        axios.get("http://localhost:8082/registerservice/details").then(res => {
            const rgs = res.data;
            this.setState({ rgs:rgs });
        }).catch(error => console.log(error));
    }
    render() {
        let  {
            rgs,
            page,
            rowsPerPage,
            shouldOpenEditorDialog,
            shouldOpenUpdateDialog
        } = this.state;
        const {classes} = this.props;
        return (
            <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                    <Button
                        className="mb-16"
                        variant="contained"
                        color="primary"
                        onClick={() => this.setState({ shouldOpenEditorDialog: true })}
                    >
                        Add new
                    </Button>
                    <Card>
                        <CardHeader color="primary" >
                            <h4 className={classes.cardTitleWhite}>Simple Table</h4>
                            <p className={classes.cardCategoryWhite}>
                                Here is a subtitle for this table
                            </p>
                        </CardHeader>
                        <CardBody>
                            <div className={classes.tableResponsive}>
                                <Paper className={classes.roott}>
                                    <TableContainer className={classes.containerr}>
                                        <Table className={classes.table} style={{ whiteSpace: "pre", minWidth: "500px" }}>
                                            <TableHead>
                                                <TableRow className={classes.tableHeadRow}>
                                                    <TableCell style={{textAlign: "center"}}  className={classes.tableCell}>ID</TableCell>
                                                    <TableCell style={{textAlign: "center"}}  className={classes.tableCell}>CODE COMPANY</TableCell>
                                                    <TableCell style={{textAlign: "center"}}  className={classes.tableCell}>NAME COMPANY</TableCell>
                                                    <TableCell style={{textAlign: "center"}}  className={classes.tableCell}>CODE SERVICE</TableCell>
                                                    <TableCell style={{textAlign: "center"}}  className={classes.tableCell}>NAME SERVICE</TableCell>
                                                    <TableCell style={{textAlign: "center"}}  className={classes.tableCell}>START DATE</TableCell>
                                                    <TableCell style={{textAlign: "center"}}  className={classes.tableCell}>END DATE</TableCell>
                                                    <TableCell style={{textAlign: "center"}}  className={classes.tableCell}>PRICE</TableCell>
                                                    <TableCell style={{textAlign: "center"}}  className={classes.tableCell}>Action</TableCell>
                                                </TableRow>
                                            </TableHead>
                                            <TableBody>
                                                {rgs.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((emp, index) => (
                                                    <TableRow  key={index} className={classes.tableBodyRow}>
                                                        <TableCell style={{textAlign: "center"}}  className={classes.tableCell} key={index}>
                                                            {emp.id_Register}
                                                        </TableCell>
                                                        <TableCell style={{textAlign: "center"}} className={classes.tableCell} key={index}>
                                                            {emp.cpn_Code}
                                                        </TableCell>
                                                        <TableCell style={{textAlign: "center"}} className={classes.tableCell} key={index}>
                                                            {emp.name_Cpn}
                                                        </TableCell>
                                                        <TableCell style={{textAlign: "center"}} className={classes.tableCell} key={index}>
                                                            {emp.service_Code}
                                                        </TableCell>
                                                        <TableCell style={{textAlign: "center"}} className={classes.tableCell} key={index}>
                                                            {emp.name_Service}
                                                        </TableCell>
                                                        <TableCell style={{textAlign: "center"}} className={classes.tableCell} key={index}>
                                                            {(new Date(emp.start_Date)).toLocaleDateString('en-US',DATE_OPTIONS)}
                                                        </TableCell>
                                                        <TableCell style={{textAlign: "center"}} className={classes.tableCell} key={index}>
                                                            {(new Date(emp.end_Date)).toLocaleDateString('en-US',DATE_OPTIONS)}
                                                        </TableCell>
                                                        <TableCell style={{textAlign: "center"}} className={classes.tableCell} key={index}>
                                                            {emp.price}
                                                        </TableCell>
                                                        <TableCell style={{textAlign: "center"}} className={classes.tableCell} key={index}>
                                                            <IconButton
                                                                // onClick={() =>
                                                                    // this.setState({
                                                                    //     uid: emp.id_Emp,
                                                                    //     shouldOpenUpdateDialog: true
                                                                    // })}

                                                            >
                                                                <Icon color="primary">edit</Icon>
                                                            </IconButton>
                                                            <IconButton onClick={() => this.handleDeleteAcc(emp.id_Emp)}>
                                                                <Icon color="error">delete</Icon>
                                                            </IconButton>
                                                        </TableCell>
                                                    </TableRow>
                                                ))}
                                            </TableBody>
                                        </Table>
                                    </TableContainer>
                                </Paper>
                                <TablePagination
                                    className="px-16"
                                    rowsPerPageOptions={[3, 6, 9]}
                                    component="div"
                                    count={rgs.length}
                                    rowsPerPage={rowsPerPage}
                                    page={page}
                                    backIconButtonProps={{
                                        "aria-label": "Previous Page"
                                    }}
                                    nextIconButtonProps={{
                                        "aria-label": "Next Page"
                                    }}
                                    onChangePage={this.handleChangePage}
                                    onChangeRowsPerPage={this.setRowsPerPage}
                                />
                            </div>
                        </CardBody>
                    </Card>
                    {/*{shouldOpenEditorDialog && (*/}
                    {/*    <AddNewEmpBD*/}
                    {/*        handleClose={this.handleDialogClose}*/}
                    {/*        open={shouldOpenEditorDialog}*/}
                    {/*        uid={this.state.uid}*/}
                    {/*    />*/}

                    {/*{shouldOpenUpdateDialog && (*/}
                    {/*    <UpdateDialog*/}
                    {/*        handleClose={this.handleDialogClose}*/}
                    {/*        open={shouldOpenUpdateDialog}*/}
                    {/*        uid={this.state.uid}*/}
                    {/*    />*/}
                    {/*)}*/}

                    <Dialog
                        open = {this.state.open}
                        onClose={this.handleClickClose}
                        aria-labelledby="alert-dialog-title"
                        aria-describedby="alert-dialog-description"
                    >
                        <DialogTitle id="alert-dialog-title">{"Có chắc chắn xoá ?"}</DialogTitle>
                        <DialogContent>
                            <DialogContentText id="alert-dialog-description">
                                Hãy chắc chắn !.
                            </DialogContentText>
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={this.handleClickClose} color="primary">
                                Huỷ
                            </Button>
                            <Button  color="primary" autoFocus onClick={this.handleSubmitDelete}>
                                Chấp nhận
                            </Button>
                        </DialogActions>
                    </Dialog>
                </GridItem>
            </GridContainer>
        )
    }
}
export default withStyles(styles) (RegisterService);