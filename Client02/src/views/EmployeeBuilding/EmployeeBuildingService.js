import axios from "axios";

export const getEmpBuildingByID = ID => {
    return axios.get(`http://localhost:8082/emp-building/${ID}`);
}
export const addNewEmpBuilding = (EmployeeBuilding) => {
    return axios.post(`http://localhost:8082/emp-building`,EmployeeBuilding);
}
export const deleteEmpBuilding = ID => {
    return axios.delete(`http://localhost:8082/emp-building/${ID}`);
}