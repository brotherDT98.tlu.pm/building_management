import IconButton from "@material-ui/core/IconButton";
import Icon from "@material-ui/core/Icon";
import React from "react";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import TableHead from "@material-ui/core/TableHead";
import Table from "@material-ui/core/Table";
import Card from "../../components/Card/Card";
import TableBody from "@material-ui/core/TableBody";
import axios from "axios";
import GridItem from "../../components/Grid/GridItem";
import CardHeader from "../../components/Card/CardHeader";
import CardBody from "../../components/Card/CardBody";
import GridContainer from "../../components/Grid/GridContainer";
import {withStyles} from "@material-ui/styles";
import styles from "./styles";
import TablePagination from "@material-ui/core/TablePagination";
import Button from "@material-ui/core/Button";
import AddNewDialog from "./AddNewDialog";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogActions from "@material-ui/core/DialogActions";
import UpdateDialog from "./UpdateDialog";
import {deleteAcc} from "./AccountService";
import TableContainer from "@material-ui/core/TableContainer";
import Paper from "@material-ui/core/Paper";
const user = JSON.parse(localStorage.getItem('user'));

class Account extends React.Component {
    state = {
        accounts: [],
        rowsPerPage:10,
        page:0,
        shouldOpenEditorDialog: false,
        shouldOpenUpdateDialog: false,
        open: false,
        setOpen: false,
    }
    setPage = page => {
        this.setState({page});
    }
    setRowsPerPage = event => {
        this.setState({rowsPerPage: event.target.value});
    }
    handleChangePage = (event,newPage) => {
        this.setPage(newPage);
    }
    componentDidMount() {
        this.updatePageData();
    }
    updatePageData = () => {
        axios.get("http://localhost:8082/allUsers",{headers: {
            Authorization: 'Bearer ' + user.accessToken,
            'Content-Type': 'application/x-www-form-urlencoded' 
        }}).then(res => {
            const accounts = res.data;
            this.setState({ accounts });
            console.log("------------------"+res)
        }).catch(error => console.log(error));
    };

    handleDialogClose = () => {
        this.setState({
            shouldOpenEditorDialog: false,
            shouldOpenUpdateDialog: false
        });
        this.updatePageData();
    };

    handleClickOpen = ID => {
        this.setState({setOpen : true, open : true, ID});
    };

    handleClickClose = () => {
        this.setState({setOpen : false, open : false});
    };

    handleDeleteAcc = (ID) => {
        this.setState({ID});
        console.log(ID);
        this.handleClickOpen(ID);
    };

    handleSubmitDelete = () => {
        deleteAcc(this.state.ID).then(() => {
            this.handleClickClose();
            this.updatePageData();
        });
    };
    render() {
        let  {
            accounts,
            page,
            rowsPerPage,
            shouldOpenEditorDialog,
            shouldOpenUpdateDialog
        } = this.state;
        const {classes} = this.props;
        console.log(accounts)
        return (
            <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                    <Button
                        className="mb-16"
                        variant="contained"
                        color="primary"
                        onClick={() => this.setState({ shouldOpenEditorDialog: true })}
                    >
                        Add new
                    </Button>
                    <Card>
                        <CardHeader color="primary" >
                            <h4 className={classes.cardTitleWhite}>Simple Table</h4>
                            <p className={classes.cardCategoryWhite}>
                                Here is a subtitle for this table
                            </p>
                        </CardHeader>
                        <CardBody>
                            <div className={classes.tableResponsive}>
                                <Paper className={classes.root}>
                                    <TableContainer className={classes.container}>
                                        <Table className={classes.table}>
                                            <TableHead>
                                                <TableRow className={classes.tableHeadRow}>
                                                    <TableCell style={{textAlign: "center"}}  className={classes.tableCell}>ID</TableCell>
                                                    <TableCell style={{textAlign: "center"}}  className={classes.tableCell}>User Name</TableCell>
                                                    <TableCell style={{textAlign: "center"}}  className={classes.tableCell}>Email</TableCell>
                                                    <TableCell style={{textAlign: "center"}}  className={classes.tableCell}>ROLES</TableCell>
                                                    <TableCell style={{textAlign: "center"}}  className={classes.tableCell}>Action</TableCell>
                                                </TableRow>
                                            </TableHead>
                                            <TableBody>
                                                {accounts.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((acc, index) => (
                                                    <TableRow key={index} className={classes.tableBodyRow}>
                                                        <TableCell style={{textAlign: "center"}} className={classes.tableCell} key={index}>
                                                            {acc.id}
                                                        </TableCell>
                                                        <TableCell style={{textAlign: "center"}} className={classes.tableCell} key={index}>
                                                            {acc.username}
                                                        </TableCell>
                                                        <TableCell style={{textAlign: "center"}} className={classes.tableCell} key={index}>
                                                            {acc.email}
                                                        </TableCell>
                                                        <TableCell style={{textAlign: "center"}} className={classes.tableCell} key={index}>
                                                            {
                                                            acc.roles.map((role,i) => (<p>{role.name}</p>))
                                                            }
                                                        </TableCell>
                                            
                                                        <TableCell style={{textAlign: "center"}} className={classes.tableCell} key={index}>
                                                            <IconButton
                                                                onClick={() =>
                                                                    this.setState({
                                                                        uid: acc.id_Account,
                                                                        shouldOpenUpdateDialog: true
                                                                    })
                                                                }
                                                            >
                                                                <Icon color="primary">edit</Icon>
                                                            </IconButton>
                                                            <IconButton onClick={() => this.handleDeleteAcc(acc.id_Account)}>
                                                                <Icon color="error">delete</Icon>
                                                            </IconButton>
                                                        </TableCell>
                                                    </TableRow>
                                                ))}
                                            </TableBody>
                                        </Table>
                                    </TableContainer>
                                </Paper>
                                <TablePagination
                                    className="px-16"
                                    rowsPerPageOptions={[3, 6, 9]}
                                    component="div"
                                    count={accounts.length}
                                    rowsPerPage={rowsPerPage}
                                    page={page}
                                    backIconButtonProps={{
                                        "aria-label": "Previous Page"
                                    }}
                                    nextIconButtonProps={{
                                        "aria-label": "Next Page"
                                    }}
                                    onChangePage={this.handleChangePage}
                                    onChangeRowsPerPage={this.setRowsPerPage}
                                />
                            </div>
                        </CardBody>
                    </Card>
                    {shouldOpenEditorDialog && (
                        <AddNewDialog
                            handleClose={this.handleDialogClose}
                            open={shouldOpenEditorDialog}
                            uid={this.state.uid}
                        />
                    )}
                    {shouldOpenUpdateDialog && (
                        <UpdateDialog
                            handleClose={this.handleDialogClose}
                            open={shouldOpenUpdateDialog}
                            uid={this.state.uid}
                        />
                    )}

                    <Dialog
                        open = {this.state.open}
                        onClose={this.handleClickClose}
                        aria-labelledby="alert-dialog-title"
                        aria-describedby="alert-dialog-description"
                    >
                        <DialogTitle id="alert-dialog-title">{"Có chắc chắn xoá ?"}</DialogTitle>
                        <DialogContent>
                            <DialogContentText id="alert-dialog-description">
                                Hãy chắc chắn !.
                            </DialogContentText>
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={this.handleClickClose} color="primary">
                                Huỷ
                            </Button>
                            <Button  color="primary" autoFocus onClick={this.handleSubmitDelete}>
                                Chấp nhận
                            </Button>
                        </DialogActions>
                    </Dialog>
                </GridItem>
            </GridContainer>
        )
    }
}
export default withStyles(styles) (Account);