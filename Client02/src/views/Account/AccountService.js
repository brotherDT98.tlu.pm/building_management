import axios from "axios";

export const getAccountByID = ID => {
    return axios.get(`http://localhost:8082/account/${ID}`);
}
export const addNewAcc = (Accounts) => {
    return axios.post(`http://localhost:8082/account`,Accounts);
}
export const updateAcc = (Accounts) => {
    return axios.post(`http://localhost:8082/account/update`,Accounts);
}
export const deleteAcc = ID => {
    return axios.delete(`http://localhost:8082/account/${ID}`);
}