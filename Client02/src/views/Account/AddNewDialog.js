import React, { Component } from "react";
import {
    Button,
    Grid,
    withStyles
} from "@material-ui/core";
import {ValidatorForm, TextValidator, SelectValidator} from "react-material-ui-form-validator";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import SaveIcon from '@material-ui/icons/Save';
import styles from "./styles"
import {addNewAcc, getAccountByID, updateAcc} from "./AccountService";
import Select from "@material-ui/core/Select";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import axios from "axios";
import FormControl from "@material-ui/core/FormControl";
import FormHelperText from "@material-ui/core/FormHelperText";
import Input from "@material-ui/core/Input";
class AddNewDialog extends Component {
    state = {
        id_Account: "",
        user_Name:"",
        pass_Word:"",
        id_Emp:"",
        status_Acc:"",
        employees:[],
    }

    handleChange = (event, source) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    handleFormSubmit = () => {
        addNewAcc({...this.state}).then(() => {
            this.props.handleClose();
        })
    };
    handleChangeValue = (event) => {
        this.setState({
            id_Emp: event.target.value,
        });
    }
    handleChangeValueStatus = (event) => {
        this.setState({
            status_Acc: event.target.value,
        });
    }
    componentDidMount() {
        axios.get("http://localhost:8082/emp-building").then(res => {
            const employees = res.data;
            this.setState({ employees });
        }).catch(error => console.log(error));
    }
    render() {
        let {id_Account,user_Name,pass_Word,id_Emp,status_Acc,employees} = this.state;
        let { open, handleClose } = this.props;
        const {classes} = this.props;
        console.log(employees)
        return (
            <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title" className={classes.bgTitleDialog}>Add/Update</DialogTitle>
                <ValidatorForm ref="form" onSubmit={this.handleFormSubmit} className={classes.formControl}>
                    <DialogContent>
                        <TextValidator className = {classes.textField}
                                       label="User Name"
                                       onChange={this.handleChange}
                                       type="text"
                                       name="user_Name"
                                       value={user_Name}
                                       validators={["required","matchRegexp:^(?=.{4,10}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9]+(?<![_.])$"]}
                                       errorMessages={["this field is required", "User name max 10 characters and min 4 character and not special character"]}

                        />

                        <TextValidator className = {classes.textField}
                                       label="Pass Word"
                                       onChange={this.handleChange}
                                       type="password"
                                       name="pass_Word"
                                       value={pass_Word}
                                       validators={["required","matchRegexp:^(?=.*?[#?!@$%^&*-]).{4,10}$","matchRegexp:^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])"]}
                                       errorMessages={["this field is required","Pass word max 10 character and min 4 character and have least one special character","Must have least one upper case and one lower case"]}
                        />
                        <br></br>
                        <InputLabel className={classes.textField} label="Standard" shrink={true}>
                            Choose Employee
                        </InputLabel>
                        <SelectValidator
                            required
                            value={id_Emp}
                            displayEmpty
                            onChange={this.handleChangeValue}
                            validators={["required"]}
                            errorMessages={["this field is required", "email is not valid"]}
                            className= {classes.textField}
                        >
                            <MenuItem value="">
                                <em>None</em>
                            </MenuItem>
                            {employees.map((emp,index) => (
                                <MenuItem value={emp.id_Emp}>{emp.id_Emp}-{emp.name_Emp}</MenuItem>
                            ))};
                        </SelectValidator>
                        <br></br>
                        <InputLabel className={classes.textField} label="Standard" shrink={true}>
                            Choose status
                        </InputLabel>
                        <SelectValidator
                            required
                            value={status_Acc}
                            displayEmpty
                            onChange={this.handleChangeValueStatus}
                            validators={["required"]}
                            errorMessages={["this field is required", "email is not valid"]}
                            className= {classes.textField}
                        >
                            <MenuItem value="">
                                <em>None</em>
                            </MenuItem>
                            <MenuItem value="1">
                                Normal
                            </MenuItem>
                            <MenuItem value="0">
                                Admin-Vip
                            </MenuItem>
                        </SelectValidator>
                    </DialogContent>
                    <DialogActions>
                        <Button variant="contained" color="secondary" type="submit" startIcon={<SaveIcon />}>
                            Save
                        </Button>
                        <Button onClick={() => this.props.handleClose()}>Cancel</Button>
                    </DialogActions>
                </ValidatorForm>
            </Dialog>
        );
    }
}

export default withStyles(styles) (AddNewDialog);
