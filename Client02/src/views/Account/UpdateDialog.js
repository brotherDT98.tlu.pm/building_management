import React, { Component } from "react";
import {
    Button,
    Grid,
    withStyles
} from "@material-ui/core";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import SaveIcon from '@material-ui/icons/Save';
import styles from "./styles"
import {addNewAcc, getAccountByID, updateAcc} from "./AccountService";
import {generateRandomId} from "../../utils";
class UpdateDialog extends Component {
    state = {
        id_Account: "",
        user_Name:"",
        pass_Word:"",
        id_Emp:"",
        status_Acc:""
    }

    handleChange = (event, source) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    handleFormSubmit = () => {
        addNewAcc({id_Account:generateRandomId(),...this.state}).then(() => {
            this.props.handleClose();
        })
    };

    componentWillMount() {
        getAccountByID(this.props.uid).then(result => this.setState({...result.data}));
    }
    render() {
        let {id_Account,user_Name,pass_Word,id_Emp,status_Acc} = this.state;
        let { open, handleClose } = this.props;
        const {classes} = this.props;
        return (
            <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title" className={classes.bgTitleDialog}>Add/Update</DialogTitle>
                <ValidatorForm ref="form" onSubmit={this.handleFormSubmit} className={classes.formControl}>
                    <DialogContent>
                        <TextValidator className = {classes.textField}
                                       label="User Name"
                                       onChange={this.handleChange}
                                       type="text"
                                       name="user_Name"
                                       value={user_Name}
                                       validators={["required"]}
                                       errorMessages={["this field is required"]}
                        />
                        <TextValidator className = {classes.textField}
                                       label="Pass Word"
                                       onChange={this.handleChange}
                                       type="text"
                                       name="pass_Word"
                                       value={pass_Word}
                                       validators={["required"]}
                                       errorMessages={["this field is required"]}
                        />
                        <TextValidator className = {classes.textField}
                                       label="ID Employee"
                                       onChange={this.handleChange}
                                       type="text"
                                       name="id_Emp"
                                       value={id_Emp}
                                       validators={["required"]}
                                       errorMessages={["this field is required"]}
                        />
                        <TextValidator className = {classes.textField}
                                       label="Status"
                                       onChange={this.handleChange}
                                       type="text"
                                       name="status_Acc"
                                       value={status_Acc}
                                       validators={["required"]}
                                       errorMessages={["this field is required"]}
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button variant="contained" color="secondary" type="submit" startIcon={<SaveIcon />}>
                            Save
                        </Button>
                        <Button onClick={() => this.props.handleClose()}>Cancel</Button>
                    </DialogActions>
                </ValidatorForm>
            </Dialog>
        );
    }
}

export default withStyles(styles) (UpdateDialog);
