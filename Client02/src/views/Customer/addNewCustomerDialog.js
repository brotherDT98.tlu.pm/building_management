import React, { Component } from "react";
import {
    Button,
    withStyles
} from "@material-ui/core";
import {ValidatorForm, TextValidator, SelectValidator} from "react-material-ui-form-validator";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import SaveIcon from '@material-ui/icons/Save';
import styles from "./styles"
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import axios from "axios";
import {generateRandomId} from "../../utils";
class AddNewCustomerDialog extends Component {
    state = {
        id: "",
        code:"",
        id_Cus:"",
        email:"",
        name_Bank:"",
        bank_Account_Number:"",
        employees:[],
    }

    handleChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    handleFormSubmit = () => {
        axios.post('http://localhost:8082/customer',{id: generateRandomId(), ...this.state }).then(() => {
            this.props.handleClose();
        }).catch(err => {
            console.log(err);
        })
    };
    handleChangeValue = (event) => {
        this.setState({
            id_Cus: event.target.value,
        });
    }
    componentDidMount() {
        axios.get("http://localhost:8082/employeecompany").then(res => {
            const employees = res.data;
            this.setState({ employees });
        }).catch(error => console.log(error));
    }
    render() {
        let {id_Cus,name_Bank,email,bank_Account_Number,code,employees} = this.state;
        let { open, handleClose } = this.props;
        const {classes} = this.props;
        return (
            <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title" className={classes.bgTitleDialog}>Add/Update</DialogTitle>
                <ValidatorForm ref="form" onSubmit={this.handleFormSubmit} className={classes.formControl}>
                    <DialogContent>
                        <TextValidator className = {classes.textField}
                                       label="Code"
                                       onChange={this.handleChange}
                                       type="text"
                                       name="code"
                                       value={code}
                                       validators={["required","matchRegexp:^(?=.{4,10}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9]+(?<![_.])$"]}
                                       errorMessages={["this field is required", "User name max 10 characters and min 4 character and not special character"]}

                        />

                        <TextValidator className = {classes.textField}
                                       label="Email"
                                       onChange={this.handleChange}
                                       type="email"
                                       name="email"
                                       value={email}
                                       validators={["required","matchRegexp:^(?=.*?[#?!@$%^&*-]).{4,10}$","matchRegexp:^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])"]}
                                       errorMessages={["this field is required","Pass word max 10 character and min 4 character and have least one special character","Must have least one upper case and one lower case"]}
                        />
                        <TextValidator className = {classes.textField}
                                       label="Name Bank"
                                       onChange={this.handleChange}
                                       type="text"
                                       name="nam_Bank"
                                       value={name_Bank}
                                       validators={["required","matchRegexp:^(?=.*?[#?!@$%^&*-]).{4,10}$","matchRegexp:^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])"]}
                                       errorMessages={["this field is required","Pass word max 10 character and min 4 character and have least one special character","Must have least one upper case and one lower case"]}
                        />
                        <TextValidator className = {classes.textField}
                                           label="Number Account"
                                           onChange={this.handleChange}
                                           type="text"
                                           name="bank_Account_Number"
                                           value={bank_Account_Number}
                                           validators={["required","matchRegexp:^(?=.*?[#?!@$%^&*-]).{4,10}$","matchRegexp:^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])"]}
                                           errorMessages={["this field is required","Pass word max 10 character and min 4 character and have least one special character","Must have least one upper case and one lower case"]}
                        />
                        <br></br>
                        <InputLabel className={classes.textField} label="Standard" shrink={true}>
                            Choose Customer
                        </InputLabel>
                        <SelectValidator
                            required
                            value={id_Cus}
                            displayEmpty
                            onChange={this.handleChangeValue}
                            validators={["required"]}
                            errorMessages={["this field is required", "email is not valid"]}
                            className= {classes.textField}
                        >
                            <MenuItem value="">
                                <em>None</em>
                            </MenuItem>
                            {employees.map((emp,index) => (
                                <MenuItem value={emp.id_Emp}>{emp.id_Emp}-{emp.name_Emp}</MenuItem>
                            ))};
                        </SelectValidator>
                    </DialogContent>
                    <DialogActions>
                        <Button variant="contained" color="secondary" type="submit" startIcon={<SaveIcon />}>
                            Save
                        </Button>
                        <Button onClick={() => this.props.handleClose()}>Cancel</Button>
                    </DialogActions>
                </ValidatorForm>
            </Dialog>
        );
    }
}

export default withStyles(styles) (AddNewCustomerDialog);
