import React, { Component } from "react";
import {
    Button,
    Grid,
    withStyles
} from "@material-ui/core";
import {ValidatorForm, TextValidator, SelectValidator} from "react-material-ui-form-validator";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import SaveIcon from '@material-ui/icons/Save';
import styles from "./styles";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import axios from "axios";
import {generateRandomId} from "../../utils";
import Paper from "@material-ui/core/Paper";
import Card from "../../components/Card/Card";
import CardHeader from "../../components/Card/CardHeader";
import CardBody from "../../components/Card/CardBody";
import TableContainer from "@material-ui/core/TableContainer";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import Link from "@material-ui/core/Link";
import IconButton from "@material-ui/core/IconButton";
import Icon from "@material-ui/core/Icon";
import TablePagination from "@material-ui/core/TablePagination";
import GridItem from "../../components/Grid/GridItem";
class ViewInvoice extends Component {

    handleChange = (event, source) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    render() {
        let { open, handleClose } = this.props;
        const {classes} = this.props;
        return (
            <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title" maxWidth={true}>
                <DialogTitle id="form-dialog-title" className={classes.bgTitleDialog}>Details Contract</DialogTitle>
                <DialogContent>
                    <Grid container spacing={3}>
                        <Grid item xs={12} sm={6} md={6}>
                            <Card>
                                <CardHeader color="primary" >
                                    <h4 className={classes.cardTitleWhite}>THÔNG TIN MẶT BẰNG</h4>
                                </CardHeader>
                                <CardBody>
                                    <div className={classes.tableResponsive}>
                                        <Paper className={classes.roott}>
                                            <TableContainer className={classes.containerr}>
                                                <Table className={classes.table} style={{ whiteSpace: "pre", minWidth: "500px" }}>
                                                    <TableBody>
                                                        <TableRow  className={classes.tableBodyRow}>
                                                            <TableCell className={classes.tableCell}>
                                                                Mặt bằng
                                                            </TableCell>
                                                            <TableCell className={classes.tableCell} >
                                                                <Link href="#">
                                                                    HD20201029
                                                                </Link>
                                                            </TableCell>
                                                        </TableRow>
                                                        <TableRow  className={classes.tableBodyRow}>
                                                            <TableCell className={classes.tableCell}>
                                                                Loại mặt bằng
                                                            </TableCell>
                                                            <TableCell className={classes.tableCell} >
                                                                <Link href="#">
                                                                    HD20201029
                                                                </Link>
                                                            </TableCell>
                                                        </TableRow>
                                                        <TableRow  className={classes.tableBodyRow}>
                                                            <TableCell className={classes.tableCell}>
                                                                Tổng diện tích
                                                            </TableCell>
                                                            <TableCell className={classes.tableCell} >
                                                                <Link href="#">
                                                                    HD20201029
                                                                </Link>
                                                            </TableCell>
                                                        </TableRow>
                                                        <TableRow  className={classes.tableBodyRow}>
                                                            <TableCell className={classes.tableCell}>
                                                                Địa điểm
                                                            </TableCell>
                                                            <TableCell className={classes.tableCell} >
                                                                <Link href="#">
                                                                    HD20201029
                                                                </Link>
                                                            </TableCell>
                                                        </TableRow>
                                                    </TableBody>
                                                </Table>
                                            </TableContainer>
                                        </Paper>
                                    </div>
                                </CardBody>
                            </Card>
                        </Grid>
                        <Grid item xs={12} sm={6} md={6}>
                            <Card>
                                <CardHeader color="primary" >
                                    <h4 className={classes.cardTitleWhite}>THÔNG TIN KHÁCH HÀNG</h4>
                                </CardHeader>
                                <CardBody>
                                    <div className={classes.tableResponsive}>
                                        <Paper className={classes.roott}>
                                            <TableContainer className={classes.containerr}>
                                                <Table className={classes.table} style={{ whiteSpace: "pre", minWidth: "500px" }}>
                                                    <TableBody>
                                                        <TableRow  className={classes.tableBodyRow}>
                                                            <TableCell className={classes.tableCell}>
                                                                Mã khách hàng
                                                            </TableCell>
                                                            <TableCell className={classes.tableCell} >
                                                                P.001
                                                            </TableCell>
                                                        </TableRow>
                                                        <TableRow  className={classes.tableBodyRow}>
                                                            <TableCell className={classes.tableCell}>
                                                                Tên khách hàng
                                                            </TableCell>
                                                            <TableCell className={classes.tableCell} >
                                                                P.001
                                                            </TableCell>
                                                        </TableRow>
                                                        <TableRow  className={classes.tableBodyRow}>
                                                            <TableCell className={classes.tableCell}>
                                                                Số điện thoại
                                                            </TableCell>
                                                            <TableCell className={classes.tableCell} >
                                                                P.001
                                                            </TableCell>
                                                        </TableRow>
                                                        <TableRow  className={classes.tableBodyRow}>
                                                            <TableCell className={classes.tableCell}>
                                                                Email
                                                            </TableCell>
                                                            <TableCell className={classes.tableCell} >
                                                                P.001
                                                            </TableCell>
                                                        </TableRow>
                                                    </TableBody>
                                                </Table>
                                            </TableContainer>
                                        </Paper>
                                    </div>
                                </CardBody>
                            </Card>
                        </Grid>
                    </Grid>
                    <Grid container spacing={3}>
                        <Grid item xs={12} sm={6} md={6}>
                            <Card>
                                <CardHeader color="primary" >
                                    <h4 className={classes.cardTitleWhite}>THÔNG TIN HỢP ĐỒNG</h4>
                                </CardHeader>
                                <CardBody>
                                    <div className={classes.tableResponsive}>
                                        <Paper className={classes.roott}>
                                            <TableContainer className={classes.containerr}>
                                                <Table className={classes.table} style={{ whiteSpace: "pre", minWidth: "500px" }}>
                                                    <TableBody>
                                                        <TableRow  className={classes.tableBodyRow}>
                                                            <TableCell className={classes.tableCell}>
                                                                Mã hợp đồng
                                                            </TableCell>
                                                            <TableCell className={classes.tableCell} >
                                                                P.001
                                                            </TableCell>
                                                        </TableRow>
                                                        <TableRow  className={classes.tableBodyRow}>
                                                            <TableCell className={classes.tableCell}>
                                                                Trạng thái
                                                            </TableCell>
                                                            <TableCell className={classes.tableCell} >
                                                                P.001
                                                            </TableCell>
                                                        </TableRow>
                                                        <TableRow  className={classes.tableBodyRow}>
                                                            <TableCell className={classes.tableCell}>
                                                                Ngày bàn giao
                                                            </TableCell>
                                                            <TableCell className={classes.tableCell} >
                                                                P.001
                                                            </TableCell>
                                                        </TableRow>
                                                        <TableRow  className={classes.tableBodyRow}>
                                                            <TableCell className={classes.tableCell}>
                                                                Ngày bắt đầu
                                                            </TableCell>
                                                            <TableCell className={classes.tableCell} >
                                                                P.001
                                                            </TableCell>
                                                        </TableRow>
                                                        <TableRow  className={classes.tableBodyRow}>
                                                            <TableCell className={classes.tableCell}>
                                                                Ngày chấm dứt
                                                            </TableCell>
                                                            <TableCell className={classes.tableCell} >
                                                                P.001
                                                            </TableCell>
                                                        </TableRow>
                                                        <TableRow  className={classes.tableBodyRow}>
                                                            <TableCell className={classes.tableCell}>
                                                                Tiền cọc
                                                            </TableCell>
                                                            <TableCell className={classes.tableCell} >
                                                                P.001
                                                            </TableCell>
                                                        </TableRow>
                                                        <TableRow  className={classes.tableBodyRow}>
                                                            <TableCell className={classes.tableCell}>
                                                                Ghi chú
                                                            </TableCell>
                                                            <TableCell className={classes.tableCell} >
                                                                P.001
                                                            </TableCell>
                                                        </TableRow>
                                                    </TableBody>
                                                </Table>
                                            </TableContainer>
                                        </Paper>
                                    </div>
                                </CardBody>
                            </Card>
                        </Grid>
                        <Grid item xs={12} sm={6} md={6}>
                            <Card>
                                <CardHeader color="primary" >
                                    <h4 className={classes.cardTitleWhite}>CÁC DỊCH VỤ ĐÃ ĐĂNG KÝ</h4>
                                </CardHeader>
                                <CardBody>
                                    <div className={classes.tableResponsive}>
                                        <Paper className={classes.roott}>
                                            <TableContainer className={classes.containerr}>
                                                <Table className={classes.table} style={{ whiteSpace: "pre", minWidth: "500px" }}>
                                                    <TableHead>
                                                        <TableRow className={classes.tableHeadRow}>
                                                            <TableCell>MÃ DỊCH VỤ</TableCell>
                                                            <TableCell>TÊN DỊCH VỤ</TableCell>
                                                            <TableCell>NGÀY ĐĂNG KÝ</TableCell>
                                                            <TableCell>NGÀY BẮT ĐẦU</TableCell>
                                                            <TableCell>NGÀY KẾT THÚC</TableCell>
                                                            <TableCell>ĐƠN GIÁ</TableCell>
                                                        </TableRow>
                                                    </TableHead>
                                                    <TableBody>
                                                        <TableRow  className={classes.tableBodyRow}>
                                                            <TableCell className={classes.tableCell}>
                                                                1
                                                            </TableCell>
                                                            <TableCell className={classes.tableCell} >
                                                                P.001
                                                            </TableCell>
                                                            <TableCell className={classes.tableCell} >
                                                                P.001
                                                            </TableCell>
                                                            <TableCell className={classes.tableCell} >
                                                                <Link href="#">
                                                                    HD20201029
                                                                </Link>
                                                            </TableCell>
                                                        </TableRow>
                                                    </TableBody>
                                                </Table>
                                            </TableContainer>
                                        </Paper>
                                    </div>
                                </CardBody>
                            </Card>
                        </Grid>
                    </Grid>
                </DialogContent>
                <DialogActions>
                    <Button variant="contained" color="secondary" type="submit" startIcon={<SaveIcon />}>
                        Print
                    </Button>
                    <Button onClick={() => this.props.handleClose()}>Cancel</Button>
                </DialogActions>
            </Dialog>
        );
    }
}

export default withStyles(styles) (ViewInvoice);
