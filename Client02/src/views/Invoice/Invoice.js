import React from "react";
import axios from "axios";
import GridContainer from "../../components/Grid/GridContainer";
import GridItem from "../../components/Grid/GridItem";
import Button from "@material-ui/core/Button";
import Card from "../../components/Card/Card";
import CardHeader from "../../components/Card/CardHeader";
import CardBody from "../../components/Card/CardBody";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import IconButton from "@material-ui/core/IconButton";
import Icon from "@material-ui/core/Icon";
import TablePagination from "@material-ui/core/TablePagination";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogActions from "@material-ui/core/DialogActions";
import {withStyles} from "@material-ui/styles";
import styles  from "./styles";
import Paper from "@material-ui/core/Paper";
import TableContainer from "@material-ui/core/TableContainer";
import Link from "@material-ui/core/Link";
import ViewInvoice from "./ViewInvoice";
const DATE_OPTIONS = {year: 'numeric', month: 'short', day: 'numeric' };
class Invoice extends React.Component {
    state = {
        rowsPerPage:10,
        page:0,
        shouldOpenEditorDialog: false,
        shouldOpenUpdateDialog: false,
        open: false,
        setOpen: false,
    }
    setPage = page => {
        this.setState({page});
    }
    setRowsPerPage = event => {
        this.setState({rowsPerPage: event.target.value});
    }
    handleChangePage = (event,newPage) => {
        this.setPage(newPage);
    }
    handleDialogClose = () => {
        this.setState({
            shouldOpenEditorDialog: false,
            shouldOpenUpdateDialog: false
        });
        this.updatePageData();
    };

    handleClickOpen = ID => {
        this.setState({setOpen : true, open : true, ID});
    };

    handleClickClose = () => {
        this.setState({setOpen : false, open : false});
    };

    handleDeleteAcc = (ID) => {
        this.setState({ID});
        console.log(ID);
        this.handleClickOpen(ID);
    };

    handleSubmitDelete = () => {
        // deleteEmpBuilding(this.state.ID).then(() => {
        //     this.handleClickClose();
        //     this.updatePageData();
        // });
    };
    componentDidMount() {
        this.updatePageData();
    }
    updatePageData = () => {
        // axios.get("http://localhost:8082/emp-building").then(res => {
        //     const employees = res.data;
        //     this.setState({ employees });
        //     console.log(employees)
        // }).catch(error => console.log(error));
    }
    render() {
        let  {
            page,
            rowsPerPage,
            shouldOpenEditorDialog,
            shouldOpenUpdateDialog
        } = this.state;
        const {classes} = this.props;
        return (
            <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                    <Button
                        className="mb-16"
                        variant="contained"
                        color="primary"
                        onClick={() => this.setState({ shouldOpenEditorDialog: true })}
                    >
                        Add new
                    </Button>
                    <Card>
                        <CardHeader color="primary" >
                            <h4 className={classes.cardTitleWhite}>Simple Table</h4>
                            <p className={classes.cardCategoryWhite}>
                                Here is a subtitle for this table
                            </p>
                        </CardHeader>
                        <CardBody>
                            <div className={classes.tableResponsive}>
                                <Paper className={classes.roott}>
                                    <TableContainer className={classes.containerr}>
                                        <Table className={classes.table} style={{ whiteSpace: "pre", minWidth: "500px" }}>
                                            <TableHead>
                                                <TableRow className={classes.tableHeadRow}>
                                                    <TableCell>ID</TableCell>
                                                    <TableCell>NAME</TableCell>
                                                    <TableCell>TYPE</TableCell>
                                                    <TableCell>CONTRACT CODE</TableCell>
                                                    <TableCell>CUSTOMER</TableCell>
                                                    <TableCell>START DATE</TableCell>
                                                    <TableCell>END DATE</TableCell>
                                                    <TableCell>STATUS</TableCell>
                                                    <TableCell>Action</TableCell>
                                                </TableRow>
                                            </TableHead>
                                            <TableBody>
                                                <TableRow  className={classes.tableBodyRow}>
                                                    <TableCell className={classes.tableCell}>
                                                        1
                                                    </TableCell>
                                                    <TableCell className={classes.tableCell} >
                                                        P.001
                                                    </TableCell>
                                                    <TableCell className={classes.tableCell} >
                                                        P.001
                                                    </TableCell>
                                                    <TableCell className={classes.tableCell} >
                                                        <Link href="#" onClick={() => this.setState({ shouldOpenEditorDialog: true })}>
                                                            HD20201029
                                                        </Link>
                                                    </TableCell>
                                                    <TableCell className={classes.tableCell} >
                                                        P.001
                                                    </TableCell>
                                                    <TableCell className={classes.tableCell} >
                                                        P.001
                                                    </TableCell>
                                                    <TableCell className={classes.tableCell} >
                                                        P.001
                                                    </TableCell>
                                                    <TableCell className={classes.tableCell} >
                                                        P.001
                                                    </TableCell>
                                                    <TableCell className={classes.tableCell}>
                                                        <IconButton

                                                        >
                                                            <Icon color="primary">edit</Icon>
                                                        </IconButton>
                                                        <IconButton>
                                                            <Icon color="error">delete</Icon>
                                                        </IconButton>
                                                    </TableCell>
                                                </TableRow>
                                            </TableBody>
                                        </Table>
                                    </TableContainer>
                                </Paper>
                                <TablePagination
                                    className="px-16"
                                    rowsPerPageOptions={[3, 6, 9]}
                                    component="div"
                                    // count={employees.length}
                                    rowsPerPage={rowsPerPage}
                                    page={page}
                                    backIconButtonProps={{
                                        "aria-label": "Previous Page"
                                    }}
                                    nextIconButtonProps={{
                                        "aria-label": "Next Page"
                                    }}
                                    onChangePage={this.handleChangePage}
                                    onChangeRowsPerPage={this.setRowsPerPage}
                                />
                            </div>
                        </CardBody>
                    </Card>
                    {shouldOpenEditorDialog && (
                        <ViewInvoice
                            handleClose={this.handleDialogClose}
                            open={shouldOpenEditorDialog}
                            // uid={this.state.uid}
                        />
                    )}
                    {/*{shouldOpenUpdateDialog && (*/}
                    {/*    <UpdateDialog*/}
                    {/*        handleClose={this.handleDialogClose}*/}
                    {/*        open={shouldOpenUpdateDialog}*/}
                    {/*        uid={this.state.uid}*/}
                    {/*    />*/}
                    {/*)}*/}

                    <Dialog
                        open = {this.state.open}
                        onClose={this.handleClickClose}
                        aria-labelledby="alert-dialog-title"
                        aria-describedby="alert-dialog-description"
                    >
                        <DialogTitle id="alert-dialog-title">{"Có chắc chắn xoá ?"}</DialogTitle>
                        <DialogContent>
                            <DialogContentText id="alert-dialog-description">
                                Hãy chắc chắn !.
                            </DialogContentText>
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={this.handleClickClose} color="primary">
                                Huỷ
                            </Button>
                            <Button  color="primary" autoFocus onClick={this.handleSubmitDelete}>
                                Chấp nhận
                            </Button>
                        </DialogActions>
                    </Dialog>
                </GridItem>
            </GridContainer>
        )
    }
}
export default withStyles(styles)(Invoice);