import React from "react";
import axios from "axios";
import GridContainer from "../../components/Grid/GridContainer";
import GridItem from "../../components/Grid/GridItem";
import Button from "@material-ui/core/Button";
import Card from "../../components/Card/Card";
import CardHeader from "../../components/Card/CardHeader";
import CardBody from "../../components/Card/CardBody";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import IconButton from "@material-ui/core/IconButton";
import Icon from "@material-ui/core/Icon";
import TablePagination from "@material-ui/core/TablePagination";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogActions from "@material-ui/core/DialogActions";
import {withStyles} from "@material-ui/styles";
import styles  from "./styles";
import Paper from "@material-ui/core/Paper";
import TableContainer from "@material-ui/core/TableContainer";
import Link from "@material-ui/core/Link";
import ViewContract from "./ViewContract";
import AddNewContractDialog from "./addNewContractDialog";

const DATE_OPTIONS = {year: 'numeric', month: 'short', day: 'numeric' };
const user = JSON.parse(localStorage.getItem('user'));

class Contract extends React.Component {
    state = {
        contracts:[],
        rowsPerPage:10,
        page:0,
        shouldOpenEditorDialog: false,
        shouldOpenUpdateDialog: false,
        openViewDetailsContract:false,
        id_LP:"",
        id_Cpn:"",
        id_Cus:"",
        id_Emp:"",
        id_Contract:"",
        open: false,
        setOpen: false,
    }
    setPage = page => {
        this.setState({page});
    }
    setRowsPerPage = event => {
        this.setState({rowsPerPage: event.target.value});
    }
    handleChangePage = (event,newPage) => {
        this.setPage(newPage);
    }
    handleDialogClose = () => {
        this.setState({
            shouldOpenEditorDialog: false,
            shouldOpenUpdateDialog: false,
            openViewDetailsContract: false
        });
        this.updatePageData();
    };

    handleClickOpen = ID => {
        this.setState({setOpen : true, open : true, ID});
    };

    handleClickClose = () => {
        this.setState({setOpen : false, open : false});
    };

    handleDeleteAcc = (ID) => {
        this.setState({ID});
        console.log(ID);
        this.handleClickOpen(ID);
    };

    handleSubmitDelete = () => {
        // deleteEmpBuilding(this.state.ID).then(() => {
        //     this.handleClickClose();
        //     this.updatePageData();
        // });
    };
    componentDidMount() {
        this.updatePageData();
    }
    updatePageData = () => {
        axios.get("http://localhost:8082/contract",{headers: {
            Authorization: 'Bearer ' + user.accessToken
        }}).then(res => {
            const contracts = res.data;
            this.setState({ contracts: contracts });
        }).catch(error => console.log(error));
    }
    render() {
        let  {
            contracts,
            page,
            rowsPerPage,
            shouldOpenEditorDialog,
            shouldOpenUpdateDialog,
            openViewDetailsContract,
            shouldOpenEditorDialogialog,
            id_LP,
            id_Cpn,
            id_Cus,
            id_Emp,
            id_Contract
        } = this.state;
        const {classes} = this.props;
        return (
            <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                    <Button
                        className="mb-16"
                        variant="contained"
                        color="primary"
                        onClick={() => this.setState({ shouldOpenEditorDialog: true })}
                    >
                        Add new
                    </Button>
                    <Card>
                        <CardHeader color="primary" >
                            <h4 className={classes.cardTitleWhite}>Simple Table</h4>
                            <p className={classes.cardCategoryWhite}>
                                Here is a subtitle for this table
                            </p>
                        </CardHeader>
                        <CardBody>
                            <div className={classes.tableResponsive}>
                                <Paper className={classes.roott}>
                                    <TableContainer className={classes.containerr}>
                                        <Table className={classes.table} style={{ whiteSpace: "pre", minWidth: "500px" }}>
                                            <TableHead>
                                                <TableRow className={classes.tableHeadRow}>
                                                    <TableCell style={{textAlign: "center"}} className={classes.tableCell}>ID</TableCell>
                                                    <TableCell style={{textAlign: "center"}} className={classes.tableCell}>CONTRACT CODE</TableCell>
                                                    <TableCell style={{textAlign: "center"}} className={classes.tableCell}>LEASED PREMISES</TableCell>
                                                    <TableCell style={{textAlign: "center"}} className={classes.tableCell}>NAME COMPANY</TableCell>
                                                    <TableCell style={{textAlign: "center"}} className={classes.tableCell}>NAME CUSTOMER</TableCell>
                                                    <TableCell style={{textAlign: "center"}} className={classes.tableCell}>NAME EMPLOYEE</TableCell>
                                                    <TableCell style={{textAlign: "center"}} className={classes.tableCell}>DATE OF DELIVERY</TableCell>
                                                    <TableCell style={{textAlign: "center"}} className={classes.tableCell}>START DATE</TableCell>
                                                    <TableCell style={{textAlign: "center"}} className={classes.tableCell}>END DATE</TableCell>
                                                    <TableCell style={{textAlign: "center"}} className={classes.tableCell}>DEPOSIT</TableCell>
                                                    <TableCell style={{textAlign: "center"}} className={classes.tableCell}>ACTION</TableCell>
                                                </TableRow>
                                            </TableHead>
                                            <TableBody>
                                                {
                                                    contracts.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((con, index) => (
                                                        <TableRow className={classes.tableBodyRow}>
                                                            <TableCell style={{textAlign: "center"}} className={classes.tableCell}>
                                                                {con.id}
                                                            </TableCell>
                                                            <TableCell style={{textAlign: "center"}} className={classes.tableCell}>
                                                                <a href="#" onClick={
                                                                    () => this.setState({
                                                                        openViewDetailsContract: true,
                                                                        id_LP: con.id_LD,
                                                                        id_Cpn: con.id_Cpn,
                                                                        id_Cus: con.id_Cus,
                                                                        id_Emp: con.id_Emp,
                                                                        id_Contract: con.id
                                                                })}>{con.code}</a>
                                                            </TableCell>
                                                            <TableCell style={{textAlign: "center"}} className={classes.tableCell}>
                                                                {con.name_LD}
                                                            </TableCell>
                                                            <TableCell style={{textAlign: "center"}} className={classes.tableCell}>
                                                                {con.name_Cpn}
                                                            </TableCell>
                                                            <TableCell style={{textAlign: "center"}} className={classes.tableCell}>
                                                                {con.name_Cus}
                                                            </TableCell>
                                                            <TableCell style={{textAlign: "center"}} className={classes.tableCell}>
                                                                {con.name_Emp}
                                                            </TableCell>
                                                            <TableCell style={{textAlign: "center"}} className={classes.tableCell}>
                                                                {(new Date(con.date_Of_Delivery)).toLocaleDateString('en-US',DATE_OPTIONS)}
                                                            </TableCell>
                                                            <TableCell style={{textAlign: "center"}} className={classes.tableCell}>
                                                                {(new Date(con.start_Date)).toLocaleDateString('en-US',DATE_OPTIONS)}
                                                            </TableCell>
                                                            <TableCell style={{textAlign: "center"}} className={classes.tableCell}>
                                                                {(new Date(con.end_Date)).toLocaleDateString('en-US',DATE_OPTIONS)}
                                                            </TableCell>
                                                            <TableCell style={{textAlign: "center"}} className={classes.tableCell}>
                                                                {con.deposit}
                                                            </TableCell>
                                                            <TableCell style={{textAlign: "center"}} className={classes.tableCell}>
                                                                <IconButton
                                                                    // onClick={() =>
                                                                    //     this.setState({
                                                                    //         uid: acc.id_Account,
                                                                    //         shouldOpenUpdateDialog: true
                                                                    //     })
                                                                    // }
                                                                >
                                                                    <Icon color="primary">edit</Icon>
                                                                </IconButton>
                                                                <IconButton
                                                                    //onClick={() => this.handleDeleteAcc(acc.id_Account)}
                                                                >
                                                                    <Icon color="error">delete</Icon>
                                                                </IconButton>
                                                            </TableCell>
                                                        </TableRow>
                                                    ))
                                                        }
                                            </TableBody>
                                        </Table>
                                    </TableContainer>
                                </Paper>
                                <TablePagination
                                    className="px-16"
                                    rowsPerPageOptions={[3, 6, 9]}
                                    component="div"
                                    count={contracts.length}
                                    rowsPerPage={rowsPerPage}
                                    page={page}
                                    backIconButtonProps={{
                                        "aria-label": "Previous Page"
                                    }}
                                    nextIconButtonProps={{
                                        "aria-label": "Next Page"
                                    }}
                                    onChangePage={this.handleChangePage}
                                    onChangeRowsPerPage={this.setRowsPerPage}
                                />
                            </div>
                        </CardBody>
                    </Card>
                    {openViewDetailsContract && (
                        <ViewContract
                            handleClose={this.handleDialogClose}
                            open={openViewDetailsContract}
                            // uid={this.state.uid}
                            id_LP = {this.state.id_LP}
                            id_Cpn = {this.state.id_Cpn}
                            id_Cus = {this.state.id_Cus}
                            id_Emp = {this.state.id_Emp}
                            id_Contract = {this.state.id_Contract}
                        />
                    )}
                    {shouldOpenEditorDialog && (
                        <AddNewContractDialog
                            handleClose={this.handleDialogClose}
                            open={shouldOpenEditorDialog}
                            uid={this.state.uid}
                        />
                    )}

                    <Dialog
                        open = {this.state.open}
                        onClose={this.handleClickClose}
                        aria-labelledby="alert-dialog-title"
                        aria-describedby="alert-dialog-description"
                    >
                        <DialogTitle id="alert-dialog-title">{"Có chắc chắn xoá ?"}</DialogTitle>
                        <DialogContent>
                            <DialogContentText id="alert-dialog-description">
                                Hãy chắc chắn !.
                            </DialogContentText>
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={this.handleClickClose} color="primary">
                                Huỷ
                            </Button>
                            <Button  color="primary" autoFocus onClick={this.handleSubmitDelete}>
                                Chấp nhận
                            </Button>
                        </DialogActions>
                    </Dialog>
                </GridItem>
            </GridContainer>
        )
    }
}
export default withStyles(styles)(Contract);