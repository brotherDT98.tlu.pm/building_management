import React, { Component } from "react";
import {
    Button,
    withStyles
} from "@material-ui/core";
import {ValidatorForm, TextValidator, SelectValidator} from "react-material-ui-form-validator";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import SaveIcon from '@material-ui/icons/Save';
import styles from "./styles"
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import axios from "axios";
import {generateRandomId} from "../../utils";
import {KeyboardDatePicker, MuiPickersUtilsProvider} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
class AddNewContractDialog extends Component {
    state = {
        id: "",
        code:"",
        id_LD:"",
        name_LD:"",
        id_Cpn:"",
        name_Cpn: "",
        id_Cus:"",
        name_Cus:"",
        id_Emp:"",
        name_Emp: "",
        date_Of_Delivery:new Date('2020-10-01T21:11:54'),
        start_Date:new Date('2020-10-01T21:11:54'),
        end_Date:new Date('2020-10-01T21:11:54'),
        status:"",
        deposit:"",
        lps:[],
        cpns:[],
        cuss:[],
        empBs:[]
    }

    handleChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    handleFormSubmit = () => {
        axios.post('http://localhost:8082/contract',{id: generateRandomId(), ...this.state }).then(() => {
            this.props.handleClose();
        }).catch(err => {
            console.log(err);
        })
    };
    handleChangeValueLP = (event) => {
        this.setState({
            id_LD: event.target.value,
        });
    }
    handleChangeValueCompany = (event) => {
        this.setState({
            id_Cpn: event.target.value,
        });
    }
    handleChangeValueEmployee = (event) => {
        this.setState({
            id_Emp: event.target.value,
        });
    }
    handleChangeValueCustomer = (event) => {
        this.setState({
            id_Cus: event.target.value,
        });
    }
    handleChangeDate = (date) => {
        this.setState({
            date_Of_Delivery:date,
            start_Date:date,
            end_Date:date,
        });
    }
    componentDidMount() {
        axios.get("http://localhost:8082/customer/details").then(res => {
            this.setState({ cuss:res.data });
        }).catch(error => console.log(error));
        axios.get("http://localhost:8082/leased-premises").then(res => {
            this.setState({ lps:res.data });
        }).catch(error => console.log(error));
        axios.get("http://localhost:8082/company").then(res => {
            this.setState({ cpns:res.data });
        }).catch(error => console.log(error));
        axios.get("http://localhost:8082/emp-building").then(res => {
            this.setState({ empBs:res.data });
        }).catch(error => console.log(error));
    }
    render() {
        let {code,id_LD,id_Cpn,id_Cus,id_Emp,date_Of_Delivery,start_Date,end_Date,status,deposit,lps,cpns,empBs,cuss} = this.state;
        let { open, handleClose } = this.props;
        const {classes} = this.props;
        return (
            <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title" className={classes.bgTitleDialog}>Add/Update</DialogTitle>
                <ValidatorForm ref="form" onSubmit={this.handleFormSubmit} className={classes.formControl}>
                    <DialogContent>
                        <TextValidator className = {classes.textField}
                                       label="Code"
                                       onChange={this.handleChange}
                                       type="text"
                                       name="code"
                                       value={code}
                                       validators={["required","matchRegexp:^(?=.{4,10}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9]+(?<![_.])$"]}
                                       errorMessages={["this field is required", "User name max 10 characters and min 4 character and not special character"]}
                        />
                        <br></br>
                        <InputLabel className={classes.textField} label="Standard" shrink={true}>
                            Choose Leased Premises
                        </InputLabel>
                        <SelectValidator
                            required
                            value={id_LD}
                            displayEmpty
                            onChange={this.handleChangeValueLP}
                            validators={["required"]}
                            errorMessages={["this field is required", "email is not valid"]}
                            className= {classes.textField}
                        >
                            <MenuItem value="">
                                <em>None</em>
                            </MenuItem>
                            {lps.map((lp,index) => (
                                <MenuItem value={lp.id}>{lp.code} - {lp.name}</MenuItem>
                            ))};
                        </SelectValidator>
                        <br></br>
                        <InputLabel className={classes.textField} label="Standard" shrink={true}>
                            Choose Company
                        </InputLabel>
                        <SelectValidator
                            required
                            value={id_Cpn}
                            displayEmpty
                            onChange={this.handleChangeValueCompany}
                            validators={["required"]}
                            errorMessages={["this field is required", "email is not valid"]}
                            className= {classes.textField}
                        >
                            <MenuItem value="">
                                <em>None</em>
                            </MenuItem>
                            {cpns.map((cpn,index) => (
                                <MenuItem value={cpn.id_Cpn}>{cpn.cpn_Code} - {cpn.name_Cpn}</MenuItem>
                            ))};
                        </SelectValidator>
                        <br></br>
                        <InputLabel className={classes.textField} label="Standard" shrink={true}>
                            Choose Customer
                        </InputLabel>
                        <SelectValidator
                            required
                            value={id_Cus}
                            displayEmpty
                            onChange={this.handleChangeValueCustomer}
                            validators={["required"]}
                            errorMessages={["this field is required", "email is not valid"]}
                            className= {classes.textField}
                        >
                            <MenuItem value="">
                                <em>None</em>
                            </MenuItem>
                            {cuss.map((cus,index) => (
                                <MenuItem value={cus.id_Cus}>{cus.code} - {cus.name_Emp}</MenuItem>
                            ))};
                        </SelectValidator>
                        <br></br>
                        <InputLabel className={classes.textField} label="Standard" shrink={true}>
                            Choose Employee
                        </InputLabel>
                        <SelectValidator
                            required
                            value={id_Emp}
                            displayEmpty
                            onChange={this.handleChangeValueEmployee}
                            validators={["required"]}
                            errorMessages={["this field is required", "email is not valid"]}
                            className= {classes.textField}
                        >
                            <MenuItem value="">
                                <em>None</em>
                            </MenuItem>
                            {empBs.map((emp,index) => (
                                <MenuItem value={emp.id_Emp}>{emp.emp_Code} - {emp.name_Emp}</MenuItem>
                            ))};
                        </SelectValidator>
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <KeyboardDatePicker
                                className = {classes.textField}
                                margin="normal"
                                id="date-picker-dialog"
                                label="Choose Date Of Delivery"
                                format="yyyy/MM/dd"
                                value={date_Of_Delivery}
                                onChange={this.handleChangeDate}
                                KeyboardButtonProps={{
                                    'aria-label': 'change date',
                                }}
                            />
                        </MuiPickersUtilsProvider>
                        <br/>
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <KeyboardDatePicker
                                className = {classes.textField}
                                margin="normal"
                                id="date-picker-dialog"
                                label="Choose Start Date"
                                format="yyyy/MM/dd"
                                value={start_Date}
                                onChange={this.handleChangeDate}
                                KeyboardButtonProps={{
                                    'aria-label': 'change date',
                                }}
                            />
                        </MuiPickersUtilsProvider>
                        <br/>
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <KeyboardDatePicker
                                className = {classes.textField}
                                margin="normal"
                                id="date-picker-dialog"
                                label="Choose End Date"
                                format="yyyy/MM/dd"
                                value={end_Date}
                                onChange={this.handleChangeDate}
                                KeyboardButtonProps={{
                                    'aria-label': 'change date',
                                }}
                            />
                        </MuiPickersUtilsProvider>
                        <TextValidator className = {classes.textField}
                                       label="Deposit"
                                       onChange={this.handleChange}
                                       type="text"
                                       name="deposit"
                                       value={deposit}
                                       validators={["required","matchRegexp:^(?=.{4,10}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9]+(?<![_.])$"]}
                                       errorMessages={["this field is required", "User name max 10 characters and min 4 character and not special character"]}
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button variant="contained" color="secondary" type="submit" startIcon={<SaveIcon />}>
                            Save
                        </Button>
                        <Button onClick={() => this.props.handleClose()}>Cancel</Button>
                    </DialogActions>
                </ValidatorForm>
            </Dialog>
        );
    }
}

export default withStyles(styles) (AddNewContractDialog);
