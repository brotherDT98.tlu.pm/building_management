
// import React, { Component } from "react";
// import Form from "react-validation/build/form";
// import Input from "react-validation/build/input";
// import CheckButton from "react-validation/build/button";

// import AuthService from "../../authentication/auth.service";

// const required = value => {
//     if (!value) {
//         return (
//             <div className="alert alert-danger" role="alert">
//                 This field is required!
//             </div>
//         );
//     }
// };

// export default class Login extends Component {
//     constructor(props) {
//         super(props);
//         this.handleLogin = this.handleLogin.bind(this);
//         this.onChangeUsername = this.onChangeUsername.bind(this);
//         this.onChangePassword = this.onChangePassword.bind(this);

//         this.state = {
//             username: "",
//             password: "",
//             loading: false,
//             message: ""
//         };
//     }

//     onChangeUsername(e) {
//         this.setState({
//             username: e.target.value
//         });
//     }

//     onChangePassword(e) {
//         this.setState({
//             password: e.target.value
//         });
//     }

//     handleLogin(e) {
//         e.preventDefault();

//         this.setState({
//             message: "",
//             loading: true
//         });

//         this.form.validateAll();

//         if (this.checkBtn.context._errors.length === 0) {
//             AuthService.login(this.state.username, this.state.password).then(
//                 () => {
//                     this.props.history.push("/");
//                     window.location.reload();
//                 },
//                 error => {
//                     const resMessage =
//                         (error.response &&
//                             error.response.data &&
//                             error.response.data.message) ||
//                         error.message ||
//                         error.toString();

//                     this.setState({
//                         loading: false,
//                         message: resMessage
//                     });
//                 }
//             );
//         } else {
//             this.setState({
//                 loading: false
//             });
//         }
//     }

//     render() {
//         return (
//             <div className="col-md-12">
//                 <div className="card card-container">
//                     <img
//                         src="//ssl.gstatic.com/accounts/ui/avatar_2x.png"
//                         alt="profile-img"
//                         className="profile-img-card"
//                     />

//                     <Form
//                         onSubmit={this.handleLogin}
//                         ref={c => {
//                             this.form = c;
//                         }}
//                     >
//                         <div className="form-group">
//                             <label htmlFor="username">Username</label>
//                             <Input
//                                 type="text"
//                                 className="form-control"
//                                 name="username"
//                                 value={this.state.username}
//                                 onChange={this.onChangeUsername}
//                                 validations={[required]}
//                             />
//                         </div>

//                         <div className="form-group">
//                             <label htmlFor="password">Password</label>
//                             <Input
//                                 type="password"
//                                 className="form-control"
//                                 name="password"
//                                 value={this.state.password}
//                                 onChange={this.onChangePassword}
//                                 validations={[required]}
//                             />
//                         </div>

//                         <div className="form-group">
//                             <button
//                                 className="btn btn-primary btn-block"
//                                 disabled={this.state.loading}
//                             >
//                                 {this.state.loading && (
//                                     <span className="spinner-border spinner-border-sm"></span>
//                                 )}
//                                 <span>Login</span>
//                             </button>
//                         </div>
//                         <a href="/customer/register">Link</a>

//                         {this.state.message && (
//                             <div className="form-group">
//                                 <div className="alert alert-danger" role="alert">
//                                     {this.state.message}
//                                 </div>
//                             </div>
//                         )}
//                         <CheckButton
//                             style={{ display: "none" }}
//                             ref={c => {
//                                 this.checkBtn = c;
//                             }}
//                         />
//                     </Form>
//                 </div>
//             </div>
//         );
//     }
// }



import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { useHistory } from 'react-router-dom';

import AuthService from "../../authentication/auth.service";

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://material-ui.com/">
        Your Website
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export default function SignIn() {
    const history = useHistory();

    function handleChangeUser(e) {
        setUsername(e.target.value);
    }
    function handleChangePass(e) {
        setPassword(e.target.value);
    }

    function  handleLogin(e) {
        e.preventDefault();

        setMessage("");
        setLoading(true);
        if(username.length == 0 || password.length == 0){
            setMessage('Please complete all information !')

        }else {
            AuthService.login(username,password).then(
                () => {
                    history.push("/");
                    window.location.reload();
                },
                error => {
                    const resMessage =
                        (error.response &&
                            error.response.data &&
                            error.response.data.message) ||
                        error.message ||
                        error.toString();
                    setLoading(false);
                    setMessage(resMessage);
                }
            );
        }
    }
    
    const classes = useStyles();

    const [username, setUsername] = React.useState("");
    const [password, setPassword] = React.useState("");
    const [loading, setLoading] = React.useState(false);
    const [message, setMessage] = React.useState("");

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign in
        </Typography>
        <form className={classes.form} noValidate  onSubmit={handleLogin}>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="username"
            label="Username"
            name="username"
            autoComplete="username"
            autoFocus
            onChange ={handleChangeUser}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            autoComplete="current-password"
            onChange = {handleChangePass}
          />
          <FormControlLabel
            control={<Checkbox value="remember" color="primary" />}
            label="Remember me"
          />
          {message && (
                            <div className="form-group">
                                <div className="alert alert-danger" role="alert">
                                    {message}
                                </div>
                            </div>
                        )}
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Sign In
          </Button>
          <Grid container>
            <Grid item xs>
              <Link href="#" variant="body2">
                Forgot password?
              </Link>
            </Grid>
            <Grid item>
              <Link href="/customer/register" variant="body2">
                {"Don't have an account? Sign Up"}
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
      <Box mt={8}>
        <Copyright />
      </Box>
    </Container>
  );
}