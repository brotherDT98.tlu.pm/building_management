import React, {useEffect, useState} from "react";
import axios from "axios";
// import TextField from "@material-ui/core/TextField";
// import {Button} from "@material-ui/core";
// import styles from "./styles.css";
// import data from "./data";
// class Test extends React.Component {
//     constructor(props) {
//         super(props);
//         //Khởi tạo state chứa giá trị của input
//         this.state = {
//             email: "",
//             password: ""
//         };
//     }
//     //  state = {
//     //      chieuDai: "",
//     //      chieuRong:"",
//     //      ketQua: 0,
//     //      soNguyen:"",
//     //      ketQuaGT: 0,
//     //  }
//     // handleChange = (e) => {
//     //     this.setState({
//     //         [e.target.name]: e.target.value
//     //     });
//     // };
//     // handleOnSubmit = () => {
//     //     this.setState({
//     //         ketQua: parseInt(this.state.chieuRong) * parseInt(this.state.chieuDai)
//     //     })
//     // }
//     // clearValue = () => {
//     //     this.setState({
//     //         chieuDai:"",
//     //         chieuRong:"",
//     //         soNguyen:"",
//     //         ketQua:0,
//     //         ketQuaGT:0
//     //     })
//     // }
//     // handleGiaiThua = () => {
//     //     var kq = 1;
//     //     var soNguyen = parseInt(this.state.soNguyen);
//     //     for (let i = 1; i <= soNguyen; i++) {
//     //         kq *=i;
//     //     }
//     //     this.setState({
//     //         ketQuaGT: kq
//     //     })
//     // }
//     // fibo(num) {
//     //     if(num <=1 ) return 1;
//     //     return this.fibo(num - 1) + this.fibo(num -2);
//     // }
//     // componentWillMount() {
//     //     console.log('Component will mount!')
//     // }
//     // componentDidMount() {
//     //     console.log('Component did mount!')
//     //     this.getList();
//     // }
//     // getList=()=>{
//     //     /*** Gọi API, update state,vv...***/
//     // }
//     submitForm(e) {
//         //Stop event default of form
//         e.preventDefault();
//         const valid = this.validationForm();
//         if(valid.error){
//             alert(valid.msg)
//         }else {
//             alert("Submit form success !")
//         }
//
//     }
//     changeInputValue(e) {
//         this.setState({
//             [e.target.name] : e.target.value
//         })
//     }
//     validationForm() {
//         let returnData = {
//             error : false,
//             msg : ""
//         }
//         const {email,password} = this.state;
//         const regx = /\S+@\S+\.\S+/;
//         // Validation Email
//         if (!regx.test(email)) {
//             returnData = {
//                 error: true,
//                 msg: "Email In Correct !"
//             }
//         }
//         //Validation Password
//         if (password.length <= 6) {
//             returnData = {
//                 error: true,
//                 msg: "Password In Correct !"
//             }
//         }
//         return returnData;
//     }
//     render() {
//        // let {chieuDai,chieuRong,ketQua,soNguyen,ketQuaGT} = this.state;
//        // console.log(this.fibo(6))
//         return (
//             <div className="container" style={{ paddingTop: "5%" }}>
//                 <form
//                     onSubmit={e => {
//                         this.submitForm(e);
//                     }}
//                 >
//                     <div className="form-group">
//                         <label htmlFor="text">Email:</label>
//                         <input
//                             type="text"
//                             className="form-control"
//                             name="email"
//                             placeholder="Enter email"
//                             onChange={e => this.changeInputValue(e)}
//                         />
//                     </div>
//                     <div className="form-group">
//                         <label htmlFor="pwd">Password:</label>
//                         <input
//                             type="password"
//                             className="form-control"
//                             name="password"
//                             placeholder="Enter password"
//                             onChange={e => this.changeInputValue(e)}
//                         />
//                     </div>
//                     <button type="submit" className="btn btn-primary">
//                         Submit
//                     </button>
//                 </form>
//
//             </div>
//             // <div className="wrap">
//             //     <h1>Animals</h1>
//             //     {
//             //         data.map( (item) => (
//             //                 <AnimalCard key={item.name} name={item.name} additional={item.additional}
//             //                     diet={item.diet} scientificName={item.scientificName} size={item.size}
//             //                             showAdditional={showAdditional}
//             //                 />
//             //             )
//             //         )
//             //     }
//             //     {/*<form noValidate autoComplete="off">*/}
//             //     {/*    <h1>Tính Diện Tích Hình Chữ Nhật</h1>*/}
//             //     {/*    <TextField id="standard-basic" label="Chieu Dai" name="chieuDai" value={chieuDai} onChange={this.handleChange} />*/}
//             //     {/*    <br></br>*/}
//             //     {/*    <TextField id="standard-basic" label="Chieu Rong" name="chieuRong" value={chieuRong} onChange={this.handleChange} />*/}
//             //     {/*    <br></br>*/}
//             //     {/*    <br></br>*/}
//             //     {/*    <Button variant="contained" color="primary" onClick={this.handleOnSubmit}>Tinh</Button>*/}
//             //     {/*    <Button variant="contained" onClick={this.clearValue}>Clear</Button>*/}
//             //     {/*    <h1>Result : {ketQua}</h1>*/}
//             //     {/*</form>*/}
//             //     {/*<hr></hr>*/}
//             //     {/*<form noValidate autoComplete="off">*/}
//             //     {/*    <h1>Tính Giai Thừa</h1>*/}
//             //     {/*    <TextField id="standard-basic" label="Nhập số" name="soNguyen" value={soNguyen} onChange={this.handleChange} />*/}
//             //     {/*    <br></br>*/}
//             //     {/*    <br></br>*/}
//             //     {/*    <Button variant="contained" color="primary" onClick={this.handleGiaiThua}>Tinh</Button>*/}
//             //     {/*    <Button variant="contained" onClick={this.clearValue}>Clear</Button>*/}
//             //     {/*    <h1>Result : {ketQuaGT}</h1>*/}
//             //     {/*</form>*/}
//             // </div>
//         )
//     }
// }
// export default Test;
//
// // {/*const Welcome = (props) => {*/}
// // {/*    console.log(props);*/}
// // {/*    return (*/}
// // {/*        <div>*/}
// // {/*            <h1>{props.children}</h1>*/}
// // {/*            <ul>*/}
// // {/*                <li>{props.name}</li>*/}
// // {/*                <li>{props.age}</li>*/}
// // {/*                <li>{props.address}</li>*/}
// // {/*            </ul>*/}
// // {/*        </div>*/}
// // {/*    )*/}
// // }
// // const AnimalCard = (props,additional) => {
// //     return (
// //         <div>
// //             <h2>{props.name}</h2>
// //             <h3>{props.scientificName}</h3>
// //             <h4>{props.size}kg</h4>
// //             <div>{props.diet.join(",")}</div>
// //             <button onClick={() => showAdditional(additional)}>More Info</button>
// //         </div>
// //     )
// // }
//
// function AnimalCard({
//     additional,
//     diet,
//     name,
//     scientificName,
//     showAdditional,
//     size
// }) {
//     return (
//         <div>
//             <h2>{name}</h2>
//             <h3>{scientificName}</h3>
//             <h4>{size}kg</h4>
//             <div>{diet.join(', ')}.</div>
//             <button onClick={() => showAdditional(additional)}>More Info</button>
//         </div>
//     );
// }
//
// function showAdditional(additional) {
//     const alertInformation = Object.entries(additional)
//         .map(information => `${information[0]}: ${information[1]}`)
//         .join('\n');
//     alert(alertInformation)
// };
//--------------------------------------------------------------------------------
//Hook useState()
//Create component ShowUser
// const ShowUser = (props) => {
//     //Get value of userList props
//     const {userList} = props;
//     // Render userList
//     //React.Fragment wrapper JSX
//     //Lists key : chỉ định key, loại bỏ cảnh báo
//     return (
//         <div>
//             {userList.map((user, index) => {
//                 return (
//                     <React.Fragment key={user.id}>
//                         <ul>
//                             <li>{user.name}</li>
//                             <li>{user.email}</li>
//                         </ul>
//                         <hr/>
//                     </React.Fragment>
//                 )
//             })}
//         </div>
//     )
// }
// export default function Test(props) {
//     //Khai báo state, sủ dụng hook useState()
//     const [listUser, setListUser] = useState([]);
//     const [isLoading,setLoading] = useState(false);
//
//     //API chua du leu
//     const getAPI = "https://5df8a4c6e9f79e0014b6a587.mockapi.io/freetuts/users";
//     //Ham fecth API lay du lieu
//     const getUser = () => {
//         //Cap nhat lai gia tri isLoading
//         setLoading(true);
//         //Thuc hien goi API
//         axios.get(getAPI).then(result => {
//             setListUser(result.data)
//         }).catch((err) => {
//             alert("Error !")
//         }).finally(() => {
//             setLoading(false);
//         })
//     }
//     return (
//        <React.Fragment>
//            {
//                isLoading ? "Loading..." : <button onClick={getUser}>Get User</button>
//            }
//            <ShowUser userList={listUser} />
//        </React.Fragment>
//     )
// }
//--------------------------------------------------------------------------------------------------------
//Hook useEffect() là của 3 phương thức componentDidMount, componentDidUpdate, componentWillUnMount

//Sử dụng useEffect như componentDidMount. Chúng ta có thể bắt sự kiện componentDidMount trong một functional component bằng cách sử dụng useEffect và chỉ định arrayDependencies là một mảng rỗng:
// useEffect(effectFunction, [])
// const ShowUser = (props) => {
//     const {userList} = props;
//     return (
//         <div>
//             {userList.map((user,index) => {
//                 return (
//                     <React.Fragment key={user.id}>
//                         <ul>
//                             <li>{user.name}</li>
//                             <li>{user.email}</li>
//                         </ul>
//                         <hr/>
//                     </React.Fragment>
//                 )
//             })}
//         </div>
//     )
// }
// export default function Test(props) {
//     const [listUser,setListUser] = useState([]);
//     useEffect(() => {
//         const getAPI = 'https://5df8a4c6e9f79e0014b6a587.mockapi.io/freetuts/users';
//         axios.get(getAPI).then(result => {
//             setListUser(result.data);
//         }).catch(err => {
//             alert("Error !");
//         })
//     },[]);
//     return (
//         <ShowUser userList={listUser} />
//     );
// }

//Sử dụng useEffect như componentDidUpdate. Để sử dụng componentDidUpdate trong functional component chúng ta sẽ chỉ định giả trị của tham số arrayDependencies là null.
//useEffect(effectFunction)

// export default function Test() {
//     const [count,setCount] = useState(0);
//     useEffect(() => {
//         document.title = `(${count}) lan click !`
//     });
//     return (
//         <React.Fragment>
//             <h1>Ban da click {count} lan</h1>
//             <button onClick={() => {setCount(count+1)}}>Click me</button>
//         </React.Fragment>
//     );
// }

//Sử dụng useEffect như componentUnWillMount.  Để sử dụng nó bằng useEffect chúng ta chỉ cần return về 1 function trong effectFunction, function được return đó sẽ đóng vai trò như là componentUnWillMount

function LifecycleDemo() {

    useEffect(() => {
        //Được gọi khi component render
        console.log('render!');
        // useEffect trả về một hàm ,
        // hàm trả về đó là đóng vai trò như
        // là componentWillUnmount
        return () => console.log('unmounting...');
    })

    return (<code>freetuts.net</code>);
}

export default function App() {
    const [mounted, setMounted] = useState(true);
    const toggle = () => setMounted(!mounted);
    return (
        <>
            {mounted && <LifecycleDemo/>} <hr />
            <button onClick={toggle}>Show/Hide LifecycleDemo</button>
        </>
    );
}