import React, { Component } from "react";
import {
    Button,
    withStyles
} from "@material-ui/core";
import {ValidatorForm, TextValidator, SelectValidator} from "react-material-ui-form-validator";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import SaveIcon from '@material-ui/icons/Save';
import styles from "./styles"
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import axios from "axios";
import {generateRandomId} from "../../utils"
class AddNewLPDialog extends Component {
    state = {
        id: "",
        code: "",
        name:"",
        acreage:"",
        status:"",
        floors:"",
    }

    handleChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    handleFormSubmit = () => {
        axios.post('http://localhost:8082/leased-premises',{id: generateRandomId(), ...this.state }).then(() => {
            this.props.handleClose();
        }).catch(err => {
            console.log(err);
        })
    };
    handleChangeValue = (event) => {

    }
    handleChangeValueStatus = (event) => {

    }
    handleChangeFloor = (e) => {
        this.setState({
            floors: e.target.value
        })
    }
    render() {
        let {code,name,acreage,status,floors} = this.state;
        let { open, handleClose } = this.props;
        const {classes} = this.props;
        return (
            <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title" className={classes.bgTitleDialog}>Add/Update</DialogTitle>
                <ValidatorForm ref="form" onSubmit={this.handleFormSubmit} className={classes.formControl}>
                    <DialogContent>
                        <TextValidator className = {classes.textField}
                                       label="Code"
                                       onChange={this.handleChange}
                                       type="text"
                                       name="code"
                                       value={code}
                                      // validators={["required","matchRegexp:^(?=.{4,10}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9]+(?<![_.])$"]}
                                      // errorMessages={["this field is required", "User name max 10 characters and min 4 character and not special character"]}

                        />

                        <TextValidator className = {classes.textField}
                                       label="Name"
                                       onChange={this.handleChange}
                                       type="text"
                                       name="name"
                                       value={name}
                                      // validators={["required","matchRegexp:^(?=.*?[#?!@$%^&*-]).{4,10}$","matchRegexp:^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])"]}
                                       //errorMessages={["this field is required","Pass word max 10 character and min 4 character and have least one special character","Must have least one upper case and one lower case"]}
                        />

                        <TextValidator className = {classes.textField}
                                       label="Acreage"
                                       onChange={this.handleChange}
                                       type="text"
                                       name="acreage"
                                       value={acreage}
                            // validators={["required","matchRegexp:^(?=.*?[#?!@$%^&*-]).{4,10}$","matchRegexp:^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])"]}
                            //errorMessages={["this field is required","Pass word max 10 character and min 4 character and have least one special character","Must have least one upper case and one lower case"]}
                        />
                        <br></br>
                        <InputLabel className={classes.textField} label="Standard" shrink={true}>
                            Choose Floors
                        </InputLabel>
                        <SelectValidator
                            required
                            value={floors}
                            displayEmpty
                            onChange={this.handleChangeFloor}
                            validators={["required"]}
                            errorMessages={["this field is required", "email is not valid"]}
                            className= {classes.textField}
                        >
                            <MenuItem value="">
                                <em>None</em>
                            </MenuItem>
                            <MenuItem value="Tang 1">
                                Tang 1
                            </MenuItem>
                            <MenuItem value="Tang 2">
                                Tang 2
                            </MenuItem>
                            <MenuItem value="Tang 3">
                                Tang 3
                            </MenuItem>
                            <MenuItem value="Tang 4">
                                Tang 4
                            </MenuItem>
                            <MenuItem value="Tang 5">
                                Tang 5
                            </MenuItem>
                            <MenuItem value="Tang 6">
                                Tang 6
                            </MenuItem>
                            <MenuItem value="Tang 7">
                                Tang 7
                            </MenuItem>
                            <MenuItem value="Tang 8">
                                Tang 8
                            </MenuItem>
                            <MenuItem value="Tang 9">
                                Tang 9
                            </MenuItem>
                            <MenuItem value="Tang 10">
                                Tang 10
                            </MenuItem>
                        </SelectValidator>
                    </DialogContent>
                    <DialogActions>
                        <Button variant="contained" color="secondary" type="submit" startIcon={<SaveIcon />}>
                            Save
                        </Button>
                        <Button onClick={() => handleClose()}>Cancel</Button>
                    </DialogActions>
                </ValidatorForm>
            </Dialog>
        );
    }
}

export default withStyles(styles) (AddNewLPDialog);
